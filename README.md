# PREMS: The Plant Ressource Management System

PREMS is a web interface for the ELVIS project developped at IRHS (Institut de
Recherche en Horticulture et Semences) Beaucouzé, France.
Together they provide an environment to manage plant in field, greenhouse and
laboratory and gather notations on them.

## Install

This is a quick and dirty minimal system insallation procedure.
At the end you will have ELVIS and PREMS running on your server but no data in
ELVIS. You will need to create some user by hand and add some material (i.e.
some accessions and other) to be able to use the application.

### System and environment setup

Fresh Debian install with only the two options `SSH server` and `standard system
utilities`.

* Enable fr_FR.UTF-8 locale by default or if you choose to install the system in
another locale
```
sudo dpkg-reconfigure locales
```

* Install apache, postgresql, git and unzip
```
sudo apt install apache2 postgresql git unzip python-psycopg2
```

* Enable cgi-bin in apache2
```
sudo a2enmod cgi
```

* Modify the `/etc/apache2/sites-enabled/000-default.conf` to allow cgi-bin to
follow symlinks by adding this bloc:
```
  ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
  <Directory "/usr/lib/cgi-bin">
    AllowOverride None
    Options ExecCGI FollowSymLinks
    Order allow,deny
    Allow from all
  </Directory>
```

* Restart apache2
```
sudo systemctl restart apache2.service
```

* Set the user to group `staff` to make file manipulations as user not as root
```
sudo usermod -a -G staff <username>
```

### Get the sources

ELVIS, PREMS and their dependancies will be installed in `/var/local`
```
cd /var/local
sudo git clone https://git.renater.fr/python-jsonrpc.git
sudo git clone https://git.renater.fr/qxfileio.git
sudo git clone https://git.renater.fr/elvis.git
sudo git clone https://git.renater.fr/prems.git
sudo git clone https://git.renater.fr/glams.git
```

### Install ELVIS

* Creation of group role `dev_team` and connexion role `elvis` with password
`Elv;S2016`
```
echo "CREATE ROLE dev_team NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;" | sudo -u postgres psql
echo "CREATE ROLE elvis LOGIN PASSWORD 'Elv;S2016' NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION; GRANT dev_team TO elvis;" | sudo -u postgres psql
```

* Initialize the ELVIS database
```
sudo -u postgres psql < /var/local/elvis/database/dbstructure.sql
```

* Populate the ELVIS database with minimal data
```
sudo -u postgres psql < /var/local/elvis/database/refdata.sql
```

* Configure the ELVIS credentials: create the `/etc/elvis.json` file containing
```
{
  "database" : {
    "name" : "elvis",
    "host" : "localhost",    
    "port" : "5432",
    "user" : "elvis",
    "password" : "Elv;S2016"
  }
}
```

* Enable python packages
```
sudo ln -s /var/local/python-jsonrpc/jsonrpc /usr/local/lib/python2.7/dist-packages/
sudo ln -s /var/local/elvis/lib/elvis /usr/local/lib/python2.7/dist-packages/
sudo ln -s /var/local/elvis/cgi-bin/ /usr/lib/cgi-bin/elvis
```

### Install PREMS

* Install NodeJS version 8 using nvm for exemple.
```
nvm install 8
nvm use 8
```

If you don't have nvm you can install it with this command:
```
wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
```
then logout and back again.

* Install Qooxdoo compiler

```
npm install -g qxcompiler
```

* Edit the application configuration file to point to your ELVIS installation
(default should be ok if done like in this documentation).
```
cd /var/local/prems
cp source/class/prems/Config.js.sample source/class/prems/Config.js
nano source/class/prems/Config.js
```

* Build the PREMS project
```
cd /var/local/prems
qx compile --library ../qxelvis --library ../qxfileio --library ../glams
qx compile --library ../qxelvis --library ../qxfileio --library ../glams --target build
```

* Make links to serve application in apache
```
sudo ln -s /var/local/prems/compiled/build /var/www/html/prems
```

* Point your navigator to your PREMS application:

http://localhost/prems
