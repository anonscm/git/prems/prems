/**
* Présentation des information d'une variete le formulaire de recherche
* fabrice février 2019
*/
qx.Class.define("prems.PresentationClone", {
  extend : qx.ui.core.Widget,
  properties : {
    id : {
      init : null,
      nullable : true,
      check : "Integer",
      event : "changeId"
    },
    cultivated : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeCultivated"
    },
    name : {
      init : null,
      check : "new qx.ui.basic.Label()",
      nullable : true,
      event : "changeName"
    },
    informations : {
      init : null,
      check : "new qx.ui.basic.Label()",
      nullable : true,
      event : "changeInformations"
    },
    accessionOrigine : {
      init : null,
      check : "new qx.ui.basic.Label()",
      nullable : true,
      event : "changeAccessionOrigine"
    },
    dateIntroduction : {
      init : null,
      check : "new qx.ui.basic.Label()",
      nullable : true,
      event : "changeDateIntroduction"
    },
    siteCollecte : {
      init : null,
      check : "new qx.ui.basic.Label()",
      nullable : true,
      event : "changeSiteCollecte"
    },
    dateCollecte : {
      init : null,
      check : "new qx.ui.basic.Label()",
      nullable : true,
      event : "changeDateCollecte"
    },
    fournisseur : {
      init : null,
      check : "new qx.ui.basic.Label()",
      nullable : true,
      event : "changeFournisseur"
    }



  },

  construct : function() {
    this.base(arguments);

    //service
    this.__service = prems.service.Service.getInstance();
    this.__servicePlant = prems.ServicePlant.getInstance();
    this.__serviceNot = prems.ServiceNotation.getInstance();

    /* -- Data Management --*/

    this.setInformations(new qx.ui.basic.Label());
    this.setName(new qx.ui.basic.Label());
    this.setDateIntroduction(new qx.ui.basic.Label());
    this.setAccessionOrigine(new qx.ui.basic.Label());
    this.setSiteCollecte(new qx.ui.basic.Label());
    this.setDateCollecte(new qx.ui.basic.Label());
    this.setFournisseur(new qx.ui.basic.Label());

    /*-- UI drawing --*/
    this._setLayout(new qx.ui.layout.VBox());
    var container = new qx.ui.container.Composite(new qx.ui.layout.VBox(10));
    container.setAllowStretchX(true);
    container.setAllowStretchY(true);
    this._add(container, {flex : 1});
    container.add(this.getName());
    container.add(this.getName());
    container.add(this.getInformations());
    container.add(this.getDateIntroduction());
    container.add(this.getAccessionOrigine());
    container.add(this.getDateCollecte());
    container.add(this.getSiteCollecte());
    container.add(this.getFournisseur());
    this.getName().setVisibility("excluded");
    this.getInformations().setVisibility("excluded");
    this.getDateIntroduction().setVisibility("excluded");
    this.getAccessionOrigine().setVisibility("excluded");
    this.getDateCollecte().setVisibility("excluded");
    this.getSiteCollecte().setVisibility("excluded");
    this.getFournisseur().setVisibility("excluded");

    /* -- Listeners --*/
    this.addListener("changeId", function() {
      var userGroups = [];
      var session = qxelvis.session.service.Session.getInstance();
      var sessionId = session.getSessionId();
      if (session.getUserGroupsInfo() !== null) {
        userGroups = session.getUserGroupsInfo().getReadGroupIds();
      }
      var servicePlant = this.__servicePlant.query("getInformationsByIdAccession", [sessionId, userGroups, this.getId()]);
      servicePlant.addListener("changeResponseModel", function(e) {
        let r = e.getData()["result"];
        if (r[0]["name"] !== null) {
          this.getName().setValue(" Non accession : "+ r[0]["name"]);
          this.getName().setVisibility("visible");
        }
        if (r[0]["informations"] !== null) {
          this.getInformations().setValue(" Informations : "+ r[0]["informations"]);
          this.getInformations().setVisibility("visible");
        }
        if (r[0]["date"] !== null) {
          this.getDateIntroduction().setValue(" Date introduction : "+ r[0]["date"]);
          this.getDateIntroduction().setVisibility("visible");
        }
        if (r[0]["date_collecte"] !== null) {
          this.getDateCollecte().setValue("Date collecte : "+ r[0]["date_collecte"]);
          this.getDateCollecte().setVisibility("visible");
        }
        if (r[0]["site_collecte"] !== null) {
          this.getSiteCollecte().setValue("Site collecte  : "+ r[0]["site_collecte"]);
          this.getSiteCollecte().setVisibility("visible");
        }
        if (r[0]["origine_introduction"] !== null) {
          this.getAccessionOrigine().setValue("Accession d'origine  : "+ r[0]["origine_introduction"]);
          this.getAccessionOrigine().setVisibility("visible");
        }
        if (r[0]["provider_firstname"] !== null || r[0]["provider_lastname"] !== null) {
          let info = "Fournisseur  : ";
          if (r[0]["provider_firstname"] !== null) {
            info = info + r[0]["provider_firstname"]+ " ";
          }
          if (r[0]["provider_lastname"] !== null) {
            info += r[0]["provider_lastname"];
          }
          this.getFournisseur().setValue(info);
          this.getFournisseur().setVisibility("visible");
        }
      }, this);
    }, this);
  }
});
