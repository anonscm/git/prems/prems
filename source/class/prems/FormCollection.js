/**
 * Collection form
*/
qx.Class.define("prems.FormCollection",
{
  extend : qx.ui.core.Widget,
  events : {
    /**
       * Fired when the collection is saved.
       */
    "saved" : "qx.event.type.Event"
  },
  construct : function(idCollection) {
    this.base(arguments);
    this.__idCollection = idCollection;

    //Layouts
    this.__container = new qx.ui.container.Composite(new qx.ui.layout.VBox(20));
    this._setLayout(new qx.ui.layout.Grow());
    this._add(this.__container);

    //Service
    this.__service = prems.service.Service.getInstance();
    var form = new qx.ui.form.Form();
    this.__collection = new qx.ui.form.TextField();
    this.__collection.setValue("");
    form.add(this.__collection, this.tr("Collection"), null, "collection");
    var saveButton = new qx.ui.form.Button(this.tr("Enregistrer"));
    form.addButton(saveButton);
    saveButton.addListener("execute", function() {
      if (this.__validate()) {
        this.__save();
      }
    }, this);
    this.__container.add(new qx.ui.form.renderer.Single(form));
  },
  members :
  {
    /**
       * Check if a field is empty.
       *@param str {String} string to check.
       *@return {Boolean} whether the string is empty or not.
       */
    isEmpty : function(str) {
      return str.length == 0;
    },

    /**
       * Validate.
       *@return {Boolean} wether the form is valid or not.
       */
    __validate : function() {
      var returnValue = true;
      this.__collection.setValid(true);
      if (this.isEmpty(this.__collection.getValue())) {
        this.__collection.setValid(false);
        returnValue = false;
      }
      return returnValue;
    },

    /**
       * Save.
       */
    __save : function() {
      let collection = this.__collection.getValue();
      let service = null;
      if (this.__idCollection) {
        service = this.__service.query("updateCollection", [this.__idCollection, collection]);
      } else {
        service = this.__service.query("createCollection", [collection]);
      }
      service.addListener("changeResponseModel", function() {
        if (service.noError()) {
          this.fireEvent("saved");
          this.__service.fireEvent("dbChange");
        } else {
          new prems.Dialog(this.tr("Erreur. Collection existante.")); // TODO To be replaced by event
        }
      }, this);
    },

    /**
       * Set id collection.
       *@param idCollection {Integer} id collection.
       */
    setId : function(idCollection) {
      this.__idCollection = idCollection;
      if (this.__idCollection !== null) {
        var service = this.__service.query("getCollectionById", [this.__idCollection]);
        service.addListener("changeResponseModel", function() {
          this.__collection.setValue(service.getResult()[0]["nom"]);
        }, this);
      }
    }
  }
});
