/**
 * Variete form
*/
qx.Class.define("prems.VarieteForm",
{
  extend : qx.ui.core.Widget,

  /**
   *@param variete {prems.Variete} variete
   */
  construct : function(variete) {
    this.base(arguments);

    //Layouts
    var container = new qx.ui.container.Composite(new qx.ui.layout.VBox(20));
    this._setLayout(new qx.ui.layout.VBox(20));
    this._add(container);

    //Service
    this.__service = prems.service.Service.getInstance();
    var boxNoms = new qx.ui.groupbox.GroupBox(this.tr("Noms"));
    boxNoms.setLayout(new qx.ui.layout.VBox(5));
    container.add(boxNoms);

    // Customize the table column model.  We want one that automatically

    // resizes columns.
    var custom = {
      tableColumnModel : function(obj) {
        return new qx.ui.table.columnmodel.Resize(obj);
      }
    };

    //Tableaux noms
    var tableModel = new qx.ui.table.model.Simple();
    tableModel.setColumnIds(["id", "type", "nom", "date", "hash"]);
    tableModel.setColumnNamesByIndex(["Id", "Type", "Nom", "Date", "Hash"]);
    var tableNoms = new qx.ui.table.Table(tableModel, custom);
    tableNoms.setHeight(100);
    tableNoms.setStatusBarVisible(false);
    tableNoms.getTableColumnModel().setColumnVisible(0, false);
    tableNoms.getTableColumnModel().setColumnVisible(4, false);
    tableModel.setColumnEditable(1, true);
    tableModel.setColumnEditable(2, true);
    tableModel.setColumnEditable(3, true);
    tableModel.addListener("dataChanged", function(e) {
      this.__updateModelTableNoms(variete, tableNoms);
    }, this);

    //Table celle date renderer
    var tcm = tableNoms.getTableColumnModel();
    var cellRenderer = new qx.ui.table.cellrenderer.Date();
    var dateFormat = new qx.util.format.DateFormat("dd/MM/yyyy");
    cellRenderer.setDateFormat(dateFormat);
    tcm.setDataCellRenderer(3, cellRenderer);
    var autoCompleteEditorType = new prems.AutoCompleteCellEditor("typeNom");
    var autoCompleteEditorNom = new prems.AutoCompleteCellEditor("nom");
    tcm.setCellEditorFactory(1, autoCompleteEditorType);
    tcm.setCellEditorFactory(2, autoCompleteEditorNom);
    tcm.setCellEditorFactory(3, new prems.DateCellEditor());
    boxNoms.add(tableNoms);

    // Resize the table columns
    var resizeBehavior = tcm.getBehavior();
    resizeBehavior.set(1, {
      width : 175
    });
    resizeBehavior.set(2, {
      width : 300
    });
    resizeBehavior.set(3, {
      width : 100
    });
    var layoutButtons = new qx.ui.container.Composite(new qx.ui.layout.HBox(10));


    //Bouton ajouter nom
    var buttonAddName = new qx.ui.form.Button(this.tr("+"));
    layoutButtons.add(buttonAddName);
    buttonAddName.setAllowStretchX(false);
    buttonAddName.setAlignX("right");
    buttonAddName.addListener("execute", function() {
      variete.addNom(new prems.NomVariete());
    }, this);
    var command = new qx.ui.command.Command("Delete");
    var buttonRemoveName = new qx.ui.form.Button(this.tr("-"), null, command);
    command.addListener("execute", function() {
      try {
        var line = tableNoms.getSelectionModel().getSelectedRanges()[0]["maxIndex"];
        var hash = tableNoms.getTableModel().getDataAsMapArray()[line]["hash"];
        var nom = qx.core.ObjectRegistry.fromHashCode(hash);
        nom.setToDelete(true);
        variete.fireDataEvent("changeNoms", null);
      } catch (e) {
      }
    }, this);
    layoutButtons.add(buttonRemoveName);
    buttonRemoveName.setAllowStretchX(false);
    buttonRemoveName.setAlignX("right");
    boxNoms.add(layoutButtons);

    //Update de la liste des noms de la variété
    this.__updateTableNoms(variete, tableNoms);
    variete.addListener("changeNoms", function() {
      this.__updateTableNoms(variete, tableNoms);
    }, this);
    var boxInfos = new qx.ui.groupbox.GroupBox(this.tr("Informations"));
    boxInfos.setLayout(new qx.ui.layout.VBox());
    container.add(boxInfos);

    //Form Variete
    var form = new qx.ui.form.Form();

    //Genre
    var genreTf = new prems.AutoCompleteBox();
    this.__service.getAutoCompleteModel("getAllGenre", [], "genre", genreTf);
    form.add(genreTf, this.tr("Genre"), null, "genre");
    variete.bind("genre", genreTf, "value");
    genreTf.bind("value", variete, "genre");
    genreTf.setWidth(200);
    genreTf.setRequired(true);

    //Espece
    var especeTf = new prems.AutoCompleteBox();
    form.add(especeTf, this.tr("Espèce"), null, especeTf);
    variete.bind("espece", especeTf, "value");
    especeTf.bind("value", variete, "espece");
    especeTf.setRequired(true);
    genreTf.addListener("changeValue", function() {
      this.__service.getAutoCompleteModel("getAllEspeceByGenre", [genreTf.getValue()], "espece", especeTf);
    }, this);

    //Obtenteur
    var obtenteurTf = new prems.AutoCompleteBox();
    this.__service.getAutoCompleteModel("getAllPepinieriste", [], "nom", obtenteurTf);
    form.add(obtenteurTf, this.tr("Obtenteur"), null, obtenteurTf);
    variete.bind("obtenteur", obtenteurTf, "value");
    obtenteurTf.bind("value", variete, "obtenteur");

    //Editeur
    var editeurTf = new prems.AutoCompleteBox();
    this.__service.getAutoCompleteModel("getAllPepinieriste", [], "nom", editeurTf);
    form.add(editeurTf, this.tr("Editeur"), null, editeurTf);
    variete.bind("editeur", editeurTf, "value");
    editeurTf.bind("value", variete, "editeur");
    editeurTf.setWidth(200);

    //Remarque
    var remarque = new qx.ui.form.TextArea();
    form.add(remarque, this.tr("Remarque"), null, remarque);
    variete.bind("remarque", remarque, "value");
    remarque.bind("value", variete, "remarque");
    remarque.setHeight(40);
    var boxClones = new qx.ui.groupbox.GroupBox(this.tr("Accessions"));
    boxClones.setLayout(new qx.ui.layout.VBox(5));
    container.add(boxClones);

    //Tableau clones
    var tableModel2 = new qx.ui.table.model.Simple();
    tableModel2.setColumnIds(["id", "numero", "nom", "hash"]);
    tableModel2.setColumnNamesByIndex(["Id", "Numéro", "Nom", "Hash"]);
    var tableClones = new qx.ui.table.Table(tableModel2);
    tableClones.setHeight(80);
    tableClones.setStatusBarVisible(false);
    tableClones.getTableColumnModel().setColumnVisible(0, false);
    tableClones.getTableColumnModel().setColumnVisible(3, false);
    boxClones.add(tableClones);
    tableClones.addListener("cellDbltap", function() {
      try {
        var line = tableClones.getSelectionModel().getSelectedRanges()[0]["maxIndex"];
        var hash = tableClones.getTableModel().getDataAsMapArray()[line]["hash"];
        var clone = qx.core.ObjectRegistry.fromHashCode(hash);
        var form = new prems.CloneForm(clone, true);
        var win = new prems.MyWindow(this.tr("Accession"));
        win.setLayout(new qx.ui.layout.Grow());
        win.add(form);
        win.open();
        win.center();

        //
        clone.addListener("saved", function() {
          variete.fireDataEvent("changeClones", null);
          win.close();
        }, this);
      } catch (e) {
      }
    }, this);

    //Bouton ajout clone
    var buttonAddClone = new qx.ui.form.Button(this.tr("+"));
    boxClones.add(buttonAddClone);
    buttonAddClone.setAllowStretchX(false);
    buttonAddClone.setAlignX("right");
    buttonAddClone.addListener("execute", function() {
      var clone = new prems.Clone();
      var formClone = new prems.CloneForm(clone, true);
      var win = new prems.MyWindow(this.tr("Nouvelle accession"));
      win.setLayout(new qx.ui.layout.Grow());
      win.add(formClone);
      win.open();
      clone.addListenerOnce("saved", function() {
        variete.addClone(clone);
        win.close();
      }, this);
    }, this);

    //Update de la liste des noms de la variété
    this.__updateTableClones(variete, tableClones);
    variete.addListener("changeClones", function() {
      this.__updateTableClones(variete, tableClones);
    }, this);

    //Bouton enregistrer
    var buttonSave = new qx.ui.form.Button(this.tr("Enregistrer"));
    buttonSave.setAllowStretchX(false);
    buttonSave.setAlignX("right");
    var popup = new qx.ui.popup.Popup(new qx.ui.layout.Canvas());
    popup.add(new qx.ui.basic.Atom(this.tr("Variété enregistrée")));
    buttonSave.addListener("execute", function() {
      this.__disableButtonSave(buttonSave);
      if (this.__validate(tableNoms, tableClones, genreTf, especeTf, obtenteurTf, editeurTf)) {
        variete.save();
        variete.addListener("saved", function() {
          this.__enableButtonSave(buttonSave);
          popup.show();
          popup.placeToWidget(buttonSave);
        }, this);
      } else {
        this.__enableButtonSave(buttonSave);
      }
    }, this);
    boxInfos.add(new qx.ui.form.renderer.Double(form));
    container.add(buttonSave);
    if (variete.getId() !== null) {
      tableClones.exclude();
      boxClones.exclude();
    }
  },
  members : {
    /**
    * Update table noms
    * @param variete {prems.Variete} variete
    * @param table {qx.ui.table.Table} table
    */
    __updateTableNoms : function(variete, table) {
      var rows = [];
      variete.getNoms().forEach(function(nom) {
        if (!nom.getToDelete()) {
          rows.push(nom.getRow());
        }
      }, this);
      var tableModel = table.getTableModel();
      tableModel.setDataAsMapArray(rows);
    },

    /**
    * Update table model noms
    * @param variete {prems.Variete} variete
    * @param table {qx.ui.table.Table} table
    */
    __updateModelTableNoms : function(variete, table) {
      var tableModel = table.getTableModel();
      var data = tableModel.getData();
      for (let r in data) {
        var nom = qx.core.ObjectRegistry.fromHashCode(data[r][4]);
        nom.setId(data[r][0]);
        nom.setType(data[r][1]);
        nom.setNom(data[r][2]);
        nom.setDate(data[r][3]);
      }
    },

    /**
    * Update table clones
    * @param variete {prems.Variete} variete
    * @param table {qx.ui.table.Table} table
    */
    __updateTableClones : function(variete, table) {
      var rows = [];
      variete.getClones().forEach(function(clone) {
        rows.push(clone.getRow());
        clone.addListener("saved", function() {
          this.__updateTableClones(variete, table);
        }, this);
      }, this);
      var tableModel = table.getTableModel();
      tableModel.setDataAsMapArray(rows);
    },

    /**
    * Update table model clones
    * @param variete {prems.Variete} variete
    * @param table {qx.ui.table.Table} table
    */
    __updateModelTableClones : function(variete, table) {
      var tableModel = table.getTableModel();
      var data = tableModel.getData();
      for (let r in data) {
        var nom = qx.core.ObjectRegistry.fromHashCode(data[r][3]);
        nom.setId(data[r][0]);
        nom.setNumero(data[r][1]);
        nom.setNom(data[r][2]);
      }
    },

    /**
    * Validate
    * @param tableNoms {qx.ui.table.Table} table noms
    * @param tableClones {qx.ui.table.Table} table clones
    * @param genreTf {qx.ui.form.TextField} genre textfield
    * @param especeTf {qx.ui.form.TextField} espece textfield
    * @param obtenteurTf {qx.ui.form.TextField} obtenteur textfield
    * @param editeurTf {qx.ui.form.TextField} editeur textfield
    * @return {Boolean} whether the form is valid or not
    */
    __validate : function(tableNoms, tableClones, genreTf, especeTf, obtenteurTf, editeurTf) {
      tableNoms.setTextColor("#000000");
      tableClones.setTextColor("#000000");
      var returnValue = true;
      if (tableNoms.getTableModel().getDataAsMapArray().length == 0) {
        tableNoms.setTextColor("#EE3233");
        returnValue = false;
      }
      if (!tableClones.isExcluded()) {
        if (tableClones.getTableModel().getDataAsMapArray().length == 0) {
          tableClones.setTextColor("#EE3233");
          returnValue = false;
        }
      }
      if (genreTf.validate(true, this.tr("Genre non valide.")) == false) {
        returnValue = false;
      }
      if (especeTf.validate(true, this.tr("Espèce non valide.")) == false) {
        returnValue = false;
      }
      if (obtenteurTf.validate(false, this.tr("Obtenteur non valide.")) == false) {
        returnValue = false;
      }
      if (editeurTf.validate(false, this.tr("Editeur non valide.")) == false) {
        returnValue = false;
      }
      return returnValue;
    },

    /**
    * Enable button save
    * @param buttonSave {qx.ui.form.Button} save button
    */
    __enableButtonSave : function(buttonSave) {
      buttonSave.setEnabled(true);
      buttonSave.setIcon(null);
      buttonSave.setLabel(this.tr("Enregistrer"));
    },

    /**
    * Disable button save
    * @param buttonSave {qx.ui.form.Button} save button
    */
    __disableButtonSave : function(buttonSave) {
      buttonSave.setEnabled(false);
      buttonSave.setIcon("prems/loading.gif");
      buttonSave.setLabel(this.tr("Enregistrement"));
    }
  }
});
