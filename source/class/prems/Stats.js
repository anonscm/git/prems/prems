/**
 * A Stats widget displaying various counts of database objects
 */
qx.Class.define("prems.Stats", {
  extend : qx.ui.core.Widget,
  properties : {
    data : {
      nullable : true,
      event : "changeData"
    }
  },
  construct : function() {
    this.base(arguments);

    //Container + layout
    this._setLayout(new qx.ui.layout.Grow());

    //table model
    this.__rowData = [];
    this.__tableModel = new qx.ui.table.model.Simple();
    this.__tableModel.setColumns(["Elements", "Quantités"]);

    //Service
    this.__service = prems.service.Service.getInstance();
    var session = qxelvis.session.service.Session.getInstance();

    //UserGroups
    var userGroups = session.getUserGroupsInfo().getReadGroupIds();

    //nombre de variétés
    var serviceVariete = this.__service.query("getAllVariete", [userGroups]);
    serviceVariete.addListener("changeResponseModel", function() {
      var data = serviceVariete.getResult();
      var l = data.length;
      this.__rowData.push(["Variétés", String(l)]);
      this.__update();
    }, this);

    //nombre de variétés
    // var serviceVarieteCount = this.__service.query("getAllVarieteCount", [session.getSessionId(), userGroups]);
    // serviceVarieteCount.addListener("changeResponseModel", function() {
    //   var data = serviceVarieteCount.getResult();
    //   this.debug(data);
    //   this.__rowData.push(["Variétés cpt", String(data)]);
    //   this.__update();
    // }, this);

    //nombre d'accessions
    var serviceClone = this.__service.query("getAllClone", [userGroups]);
    serviceClone.addListener("changeResponseModel", function() {
      this.__rowData.push(["Accessions", String(serviceClone.getResult().length)]);
      this.__update();
    }, this);

    //nombre de lots
    var serviceLot = this.__service.query("getAllLot", [userGroups]);
    serviceLot.addListener("changeResponseModel", function() {
      this.__rowData.push(["Lots", String(serviceLot.getResult().length)]);
      this.__update();
    }, this);
    this.__tableModel.setData(this.__rowData);
    this.__table = new qx.ui.table.Table(this.__tableModel);

    this._add(this.__table);
  },
  members : {
    /**
     * Array containing data for the table
     */
    __rowData : null,

    /**
     * Update table.
    */
    __update : function() {
      this.__tableModel.setData(this.__rowData);
      //this.__table.setTableModel(this.__tableModel);
    }
  }
});
