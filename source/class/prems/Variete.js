/**
 * Variété object.
 * Stores all the informations of the "Variété" table.
*/
qx.Class.define("prems.Variete", {
  extend : qx.core.Object,
  events : {
    /**
     * Fired when the variete is saved in the database.
     */
    "saved" : "qx.event.type.Event",
    /**
     * Fired when the variete is retrieved from the database.
     */
    "getEnd" : "qx.event.type.Data"
  },
  properties : {
    /**
    * id
    */
    id : {
      init : null,
      nullable : true,
      check : "Integer",
      event : "changeId"
    },
    /**
     * obtenteur
     */
    obtenteur : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeObtenteur"
    },
    /**
     * editeur
     */
    editeur : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeEditeur"
    },
    /**
     * genre
     */
    genre : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeGenre"
    },
    /**
     * espece
     */
    espece : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeEspece"
    },
    /**
     * remarque
     */
    remarque : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeRemarque"
    },
    /**
    * noms
    */
    noms : {
      init : new qx.data.Array(),
      check : "Array",
      event : "changeNoms"
    },
    /**
     * clones
     */
    clones : {
      init : new qx.data.Array(),
      check : "Array",
      event : "changeClones"
    }
  },
  /**
   * constructor
   * @param idVariete {Integer} id of the variete if it exists in the database.
   */
  construct : function(idVariete) {
    this.base(arguments);
    this.setNoms(new qx.data.Array());
    this.setClones(new qx.data.Array());

    //Service
    this.__service = prems.service.Service.getInstance();

    //Sets the variete id
    if (idVariete) {
      this.setId(idVariete);
      this.get();
    }

    //    this.addListener("saved", function(){
    //      new prems.Dialog("Variété enregistrée");
    //      this.__service.fireEvent("dbChange");
    //    },this);
  },
  members :
  {
    /**
       * Saves (update or create)
       */
    save : function() {
      var serviceEditeur = this.__service.query("getPepinieristeByNom", [this.getEditeur()]);
      serviceEditeur.addListener("changeResponseModel", function() {
        try {
          var idEditeur = serviceEditeur.getResult()[0]["id_pepinieriste"];
        } catch (e) {
        }
        var serviceObtenteur = this.__service.query("getPepinieristeByNom", [this.getObtenteur()]);
        serviceObtenteur.addListener("changeResponseModel", function() {
          try {
            var idObtenteur = serviceObtenteur.getResult()[0]["id_pepinieriste"];
          } catch (e) {
          }
          if (this.getId() === null) {
            this.create(idObtenteur, idEditeur);
          } else {
            this.update(idObtenteur, idEditeur);
          }
        }, this);
      }, this);
    },

    /**
       * Get
       */
    get : function() {
      var serviceVariete = this.__service.query("getVarieteById", [this.getId()]);
      serviceVariete.addListener("changeResponseModel", function() {
        this.setRemarque(serviceVariete.getResult()[0]["remarque"]);
        var idObtenteur = serviceVariete.getResult()[0]["obtenteur"];
        var idEditeur = serviceVariete.getResult()[0]["editeur"];
        var idTaxinomie = serviceVariete.getResult()[0]["id_taxinomie"];
        var serviceEditeur = this.__service.query("getPepinieristeById", [idEditeur]);
        serviceEditeur.addListener("changeResponseModel", function() {
          try {
            this.setEditeur(serviceEditeur.getResult()[0]["nom"]);
          } catch (e) {
          }
          var serviceObtenteur = this.__service.query("getPepinieristeById", [idObtenteur]);
          serviceObtenteur.addListener("changeResponseModel", function() {
            try {
              this.setObtenteur(serviceObtenteur.getResult()[0]["nom"]);
            } catch (e) {
            }
            this.getTaxinomieInfos(idTaxinomie);
          }, this);
        }, this);
      }, this);
    },

    /**
     * Get taxinomie informations
     *@param idTaxinomie {Integer} id taxinomie
    */
    getTaxinomieInfos : function(idTaxinomie) {
      var service = this.__service.query("getTaxinomieById", [idTaxinomie]);
      service.addListener("changeResponseModel", function() {
        try {
            this.setGenre(service.getResult()[0]["genre"]);
        } catch (e) {
            this.setGenre(null);
        }
        try {
            this.setEspece(service.getResult()[0]["espece"]);
        } catch (e) {
            this.setEspece(null);
        }
        this.getNomsInfos();
      }, this);
    },

    /**
     * Get noms informations
    */
    getNomsInfos : function() {
      var service = this.__service.query("getNomsByVariete", [this.getId()]);
      service.addListener("changeResponseModel", function() {
        var noms = new qx.data.Array();
        var cpt = 0;
        for (var n in service.getResult()) {
          noms.push(new prems.NomVariete(service.getResult()[n]["id_nom_var"]));
          noms.getItem(n).addListener("getEnd", function() {
            cpt++;
            if (cpt == service.getResult().length) {
              this.setNoms(noms);
              this.fireDataEvent("getEnd", this);
            }
          }, this);
        }
      }, this);
    },

    /**
     * Get clone informations
    */
    getCloneInfos : function() {
      var serviceClone = this.__service.query("getCloneByIdVariete", [this.getId()]);
      serviceClone.addListener("changeResponseModel", function() {
        var clones = new qx.data.Array();
        var cpt = 0;
        for (var n in serviceClone.getResult()) {
          clones.push(new prems.Clone(serviceClone.getResult()[n]["id_clone"]));
          clones.getItem(n).addListener("getEnd", function() {
            cpt++;
            if (cpt == serviceClone.getResult().length) {
              this.setClones(clones);
              this.fireDataEvent("changeClones", null);
              this.fireDataEvent("getEnd", this);
            }
          }, this);
        }
      }, this);
    },

    /**
       * Create
       *@param idObtenteur {Integer} id obtenteur
       *@param idEditeur {Integer} id editeur
       */
    create : function(idObtenteur, idEditeur) {
      var service = this.__service.query("createVariete", [idObtenteur, this.getRemarque(), idObtenteur]);
      service.addListener("changeResponseModel", function() {
        this.setId(service.getResult()[0]["id_variete"]);
        this.setTaxinomie();
      }, this);
    },

    /**
       * Update
       *@param idObtenteur {Integer} id obtenteur
       *@param idEditeur {Integer} id editeur
       */
    update : function(idObtenteur, idEditeur) {
      var service = this.__service.query("updateVariete", [this.getId(), idObtenteur, this.getRemarque(), idEditeur]);
      service.addListener("changeResponseModel", function() {
        this.setId(service.getResult()[0]["id_variete"]);
        this.setTaxinomie();
      }, this);
    },

    /**
           * Set taxinomie
           */
    setTaxinomie : function() {
      //Attribue la taxinomie à la variété
      var serviceTaxinomie = this.__service.query("getTaxinomieByGenreAndEspece", [this.getGenre(), this.getEspece()]);
      serviceTaxinomie.addListener("changeResponseModel", function() {
        try {
          var idTaxinomie = serviceTaxinomie.getResult()[0]["id"];
          var service = this.__service.query("setTaxinomie", [this.getId(), idTaxinomie]);
          service.addListener("changeResponseModel", function() {
            this.saveNoms();
          }, this);
        } catch (e) { //Si pas de taxinomie
          this.saveNoms();
        }
      }, this);
    },

    /**
           * Set noms
           */
    saveNoms : function() {
      var cpt = 0;
      for (var n in this.getNoms().toArray()) {
        this.getNoms().getItem(n).setIdVariete(this.getId());
        this.getNoms().getItem(n).save();
        this.getNoms().getItem(n).addListener("saved", function() {
          cpt++;
          if (cpt == this.getNoms().length) {
            this.saveClones();
          }
        }, this);
      }
    },

    /**
           * Save clones
           */
    saveClones : function() {
      var cpt = 0;
      if (this.getClones().getLength() > 0) {
        for (var i in this.getClones().toArray()) {
          this.getClones().getItem(i).setIdVariete(this.getId());
          this.getClones().getItem(i).save();
          this.getClones().getItem(i).addListener("saved", function() {
            cpt++;
            if (cpt == this.getClones().length) {
              this.fireEvent("saved");
            }
          }, this);
        }
      } else {
        this.fireEvent("saved");
      }
    },

    /**
    * Add nom
    *@param nom {prems.NomVariete} nom
    */
    addNom : function(nom) {
      var noms = this.getNoms();
      noms.push(nom);
      this.setNoms(noms);
      this.fireDataEvent("changeNoms");
    },

    /**
    * Add clone
    *@param clone {prems.Clone} clone
    */
    addClone : function(clone) {
      var clones = this.getClones();
      clones.push(clone);
      this.setClones(clones);
      this.fireDataEvent("changeClones");
    },

    /**
    * get table row
    */
    getRow : function() {
    },

    /**
         * To csv
         *@return {Object} object to write in the export file
         */
    toCsv : function() {
      var data = {
      };
      data["id variété"] = this.getId();
      this.getNoms().forEach(function(nom) {
        var colName = "Nom:" + nom.getType();
        if (nom.getDate() !== null) {
          var format = new qx.util.format.DateFormat("dd/MM/yyyy");
          var date = format.format(nom.getDate());
          data[colName] = nom.getNom() + ":" + date;
        } else {
          date = null;
          data[colName] = nom.getNom();
        }
      }, this);
      data["Genre"] = this.getGenre();
      data["Espèce"] = this.getEspece();
      data["Obtenteur"] = this.getObtenteur();
      data["Editeur"] = this.getEditeur();
      data["Remarque"] = this.getRemarque();
      return data;
    }
  }
});
