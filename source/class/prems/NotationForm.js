/**
 * Variete form
*/
qx.Class.define("prems.NotationForm",
{
  extend : qx.ui.core.Widget,
  construct : function(notation) {
    this.base(arguments);

    //Layouts
    var container = new qx.ui.container.Composite(new qx.ui.layout.VBox(20));
    this._setLayout(new qx.ui.layout.Grow());
    this._add(container);

    //Service
    this.__service = prems.service.Service.getInstance();
    this.__notation = notation;
    var boxInfos = new qx.ui.groupbox.GroupBox("");
    boxInfos.setLayout(new qx.ui.layout.VBox());
    container.add(boxInfos);

    //Form
    var form = new qx.ui.form.Form();
    var session = qxelvis.session.service.Session.getInstance();
    var userGroups = session.getUserGroupsInfo().getReadGroupIds();

    //Type
    var lotTf = new prems.AutoCompleteBox();
    this.__service.getAutoCompleteModel("getAllLot", [userGroups], "numero_lot", lotTf);
    form.add(lotTf, this.tr("Numéro lot"), null, "numeroLot");
    notation.bind("lot", lotTf, "value");
    lotTf.bind("value", notation, "lot");

    //Date
    var dateTf = new qx.ui.form.DateField();
    form.add(dateTf, this.tr("Date"), null, "date");
    notation.bind("date", dateTf, "value");
    dateTf.bind("value", notation, "date");
    var formatDate = new qx.util.format.DateFormat("dd/MM/YYYY");
    dateTf.setDateFormat(formatDate);

    //Type
    var typeTf = new prems.AutoCompleteBox();
    this.__service.getAutoCompleteModel("getAllTypeNotation", [], "nom", typeTf);
    form.add(typeTf, this.tr("Type"), null, "type");
    notation.bind("typeNotation", typeTf, "value");
    typeTf.bind("value", notation, "typeNotation");
    typeTf.setWidth(300);

    //Valeur
    var valeurTf = new prems.AutoCompleteBox();
    form.add(valeurTf, this.tr("Valeur"), null, "valeur");
    notation.bind("valeur", valeurTf, "value");
    valeurTf.bind("value", notation, "valeur");
    typeTf.addListener("changeValue", function() {
      this.__service.getAutoCompleteModel("getAllValeurByNotation", [typeTf.getValue()], "valeur", valeurTf);
    }, this);

    //Site
    var siteTf = new prems.AutoCompleteBox();
    this.__service.getAutoCompleteModel("getAllSites", [], "nom_site", siteTf);
    form.add(siteTf, this.tr("Site"), null, "site");
    notation.bind("site", siteTf, "value");
    siteTf.bind("value", notation, "site");
    siteTf.setWidth(300);

    //Remarque
    var rqTf = new qx.ui.form.TextArea();
    form.add(rqTf, this.tr("Remarque"), null, "remarque");
    notation.bind("remarque", rqTf, "value");
    rqTf.bind("value", notation, "remarque");

    //Bouton enregistrer
    var buttonSave = new qx.ui.form.Button(this.tr("Enregistrer"));
    buttonSave.setAllowStretchX(false);
    buttonSave.setAlignX("right");
    form.addButton(buttonSave);
    buttonSave.addListener("execute", function() {
      if (lotTf.validate(true, "error") && typeTf.validate(true, "error") && siteTf.validate(false, "error")) {
        notation.save();
      }
    }, this);
    boxInfos.add(new qx.ui.form.renderer.Single(form));
    container.add(buttonSave);
  },
  members : {

  }
});
