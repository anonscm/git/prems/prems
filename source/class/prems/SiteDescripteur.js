/**
 * Ajout modification des sites
*/
qx.Class.define("prems.SiteDescripteur",
{
  extend : qx.ui.core.Widget,
  construct : function() {
    this.base(arguments);
    this.__container = new qx.ui.container.Composite(new qx.ui.layout.VBox(20));
    this._setLayout(new qx.ui.layout.Grow());
    this._add(this.__container);
    this.__service = prems.service.Service.getInstance();
    var form = new qx.ui.form.Form();

    //recherche des valeurs distinctes par liste
    this.__liste = new qx.ui.form.List().set(
    {
      width : 250,
      height : 200,
      minWidth : 50,
      minHeight : 100,
      maxHeight : 400,
      maxWidth : 500
    });

    //boutons
    this.__buttonEdit = new qx.ui.form.Button(this.tr("Modifier"));
    this.__buttonNew = new qx.ui.form.Button(this.tr("Ajouter"));
    form.add(this.__liste, "", null, "liste");
    form.addButton(this.__buttonNew);
    //suppression bouton modifier
    //form.addButton(this.__buttonEdit);
    this.__container.add(new qx.ui.form.renderer.Single(form));
    //this.__liste.addListener("dblclick", this.__edit, this);
    this.__buttonEdit.addListener("execute", this.__edit, this);
    this.__buttonNew.addListener("execute", this.__new, this);
    this.__update();
  },
  members :
  {
    /**
     * edit
     */
    __edit : function() {
      if (this.__liste.getModelSelection().getLength() > 0) {
        var form = new prems.FormSite();
        form.setId(this.__liste.getModelSelection().getItem(0));
        var win = new prems.MyWindow(this.tr("Sites"));
        win.setLayout(new qx.ui.layout.Grow());
        win.add(form);
        win.open();
        win.center();
        form.addListener("saved", function() {
          this.__update();
          win.close();
        }, this);
      }
    },

    /**
     * new
     */
    __new : function() {
      var form = new prems.FormSite();
      var win = new prems.MyWindow(this.tr("Sites"));
      win.setLayout(new qx.ui.layout.Grow());
      win.add(form);
      win.open();
      win.center();
      form.addListener("saved", function() {
        this.__update();
        win.close();
      }, this);
    },

    /**
     * update
     */
    __update : function() {
      var service = this.__service.query("getAllSites");
      service.addListener("changeResponseModel", function() {
        this.__liste.removeAll();
        var resultats = service.getResult();
        var data = {
          Choice : []
        };
        for (var collection in resultats) {
          data.Choice.push(
          {
            label : resultats[collection]["nom_site"],
            data : resultats[collection]["id_site"]
          });
        }
        var model = qx.data.marshal.Json.createModel(data);
        var listController = new qx.data.controller.List(null, this.__liste);
        listController.setDelegate({
          bindItem : function(controller, item, index) {
            controller.bindProperty("label", "label", null, item, index);
            controller.bindProperty("data", "model", null, item, index);
          }
        });
        listController.setModel(model.getChoice());
        this.__liste.show();
      }, this);
    }
  }
});
