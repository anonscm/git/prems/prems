/**
 * Pepinieriste object
*/
qx.Class.define("prems.Pepinieriste",
{
  extend : qx.core.Object,
  events :
  {
    /**
         * Fired when the pepinieriste is saved in the database.
         */
    "saved" : "qx.event.type.Data",

    /**
     * Fired when the pepinieriste is retrieved from the database.
     */
    "getEnd" : "qx.event.type.Data"
  },
  properties :
  {
    /**
       * id
       */
    id :
    {
      init : null,
      nullable : true,
      check : "Integer",
      event : "changeId"
    },

    /**
     * nom
     */
    nom :
    {
      init : null,
      nullable : true,
      check : "String",
      event : "changeNom"
    },

    /**
     * adresse1
     */
    adresse1 :
    {
      init : null,
      nullable : true,
      check : "String",
      event : "changeAdresse1"
    },

    /**
     * adresse2
     */
    adresse2 :
    {
      init : null,
      nullable : true,
      check : "String",
      event : "changeAdresse2"
    },

    /**
     * ville
     */
    ville :
    {
      init : null,
      nullable : true,
      check : "String",
      event : "changeVille"
    },

    /**
     * etat
     */
    etat :
    {
      init : null,
      nullable : true,
      check : "String",
      event : "changeEtat"
    },

    /**
     * code postal
     */
    codePostal :
    {
      init : null,
      nullable : true,
      check : "String",
      event : "changeCodePostal"
    },

    /**
     * pays
     */
    pays :
    {
      init : null,
      nullable : true,
      check : "String",
      event : "changePays"
    },

    /**
     * email
     */
    email :
    {
      init : null,
      nullable : true,
      check : "String",
      event : "changeEmail"
    },

    /**
     * telephone
     */
    telephone :
    {
      init : null,
      nullable : true,
      check : "String",
      event : "changeTelephone"
    },

    /**
     * fax
     */
    Fax :
    {
      init : null,
      nullable : true,
      check : "String",
      event : "changeFax"
    }
  },
  construct : function() {
    this.base(arguments);

    //this.__service = new prems.service.Service();
  },
  members :
  {
    /**
       * Saves (update or create)
       */
    save : function() {
    },

    /**
       * Get
       */
    get : function() {
    },

    /**
       * Create
       */
    create : function() {
    },

    /**
       * Update
       */
    update : function() {
    }
  }
});
