/**
* Introduction clone object.
* Stores all the informations of the "Intrduction clone" table.
*/
qx.Class.define("prems.IntroductionClone", {
  extend : qx.core.Object,
  events : {
    /**
    * Fired when the introduction is saved in the database.
    */
    "saved" : "qx.event.type.Event",

    /**
    * Fired when the introduction is retrieved from the database.
    */
    "getEnd" : "qx.event.type.Event"
  },
  properties : {
    /**
    * id
    */
    id : {
      init : null,
      nullable : true,
      check : "Integer",
      event : "changeId"
    },

    /**
    * Etat sanitaire
    */
    etatSanitaire : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeEtatSanitaire"
    },

    /**
    * Mode introduction
    */
    modeIntroduction : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeModeIntroduction"
    },
    
    /**
    * Numéro passeport phytosanitaire
    */
    numeroPasseport : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeNumeroPasseport"
    },

    /**
    * Lieu greffage
    */
    lieuGreffage : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeLieuGreffage"
    }
  },

  /**
  * constructor
  * @param idIntroduction {Integer} id of the introduction if it exists in the database.
  */
  construct : function(idIntroduction) {
    this.base(arguments);

    //Service
    this.__service = prems.service.Service.getInstance();

    //Sets the variete id
    if (idIntroduction) {
      this.setId(idIntroduction);
      this.get();
    }
  },
  members : {
    /**
    * Saves (update or create)
    */
    save : function() {
      if (this.getId() === null) {
        this.create();
      } else {
        this.update();
      }
    },

    /**
    * Get
    */
    get : function() {
      var service = this.__service.query("getIntroductionCloneById", [this.getId()]);
      service.addListener("changeResponseModel", function() {
        this.setId(service.getResult()[0]["id"]);
        this.setModeIntroduction(service.getResult()[0]["mode_introduction"]);
        this.setLieuGreffage(service.getResult()[0]["lieu_greffage"]);
        this.setEtatSanitaire(service.getResult()[0]["etat_sanitaire"]);
        this.setNumeroPasseport(service.getResult()[0]["numero_passeport_phytosanitaire"]);
        this.fireEvent("getEnd");
      }, this);
    },

    /**
    * Create
    */
    create : function() {
      var service = this.__service.query("createIntroductionClone", [this.getEtatSanitaire(), this.getModeIntroduction(), this.getNumeroPasseport(), this.getLieuGreffage()]);
      service.addListener("changeResponseModel", function() {
        this.setId(service.getResult()[0]["id"]);
        this.fireEvent("saved");
      }, this);
    },

    /**
    * Update
    */
    update : function() {
      var service = this.__service.query("updateIntroductionClone", [this.getId(), this.getEtatSanitaire(), this.getModeIntroduction(), this.getNumeroPasseport(), this.getLieuGreffage()]);
      service.addListener("changeResponseModel", function() {
        this.setId(service.getResult()[0]["id"]);
        this.fireEvent("saved");
      }, this);
    }
  }
});
