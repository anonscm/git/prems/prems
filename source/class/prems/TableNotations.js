/**
 * Table notations
*/
qx.Class.define("prems.TableNotations",
{
  extend : qx.ui.core.Widget,
  construct : function(obj) {
    this.base(arguments);

    //Layouts
    var container = new qx.ui.container.Composite(new qx.ui.layout.VBox(10));
    this._setLayout(new qx.ui.layout.Grow());
    this._add(container);

    //Service

    //this.__service = prems.service.Service.getInstance();

    //obj = variete ou lot

    // Customize the table column model.  We want one that automatically

    // resizes columns.
    var custom = {
      tableColumnModel : function(obj) {
        return new qx.ui.table.columnmodel.Resize(obj);
      }
    };

    //Tableaux noms
    var tableModel = new qx.ui.table.model.Filtered();
//    tableModel.setColumnIds(["id", "type_notation", "valeur", "date", "notateur", "site", "hash"]);
//    tableModel.setColumnNamesByIndex(["Id", "Type", "Valeur", "Date", "Notateur", "Site", "Hash"]);
    tableModel.setColumnIds(["id", "type_notation", "valeur", "date", "notateur", "site", "hash", "remarque"]);
    tableModel.setColumnNamesByIndex(["Id", "Type", "Valeur", "Date", "Notateur", "Site", "Hash", "Remarque"]);
    var table = new qx.ui.table.Table(tableModel, custom);
    table.setStatusBarVisible(false);
    table.getTableColumnModel().setColumnVisible(0, false);
    table.getTableColumnModel().setColumnVisible(6, false);
    tableModel.setColumnEditable(1, true);
    tableModel.setColumnEditable(2, true);
    tableModel.setColumnEditable(3, true);
    tableModel.setColumnEditable(4, true);
    tableModel.setColumnEditable(5, true);
    var tcm = table.getTableColumnModel();
    var cellRenderer = new qx.ui.table.cellrenderer.Date();
    var dateFormat = new qx.util.format.DateFormat("dd/MM/yyyy");
    cellRenderer.setDateFormat(dateFormat);
    tcm.setDataCellRenderer(3, cellRenderer);
    tcm.setCellEditorFactory(3, new prems.DateCellEditor());
    tcm.setCellEditorFactory(1, new prems.AutoCompleteCellEditor("typeNotation"));
    tcm.setCellEditorFactory(5, new prems.AutoCompleteCellEditor("site"));

    // Resize the table columns
    var resizeBehavior = tcm.getBehavior();
    resizeBehavior.set(1, {
      width : 250
    });
    resizeBehavior.set(2, {
      width : 150
    });
    resizeBehavior.set(3, {
      width : 100
    });
    container.add(table);
    var layoutButtons = new qx.ui.container.Composite(new qx.ui.layout.HBox(10));
    

    //Bouton ajout lieu
    var buttonAddNotation = new qx.ui.form.Button(this.tr("+"));
    layoutButtons.add(buttonAddNotation);
    buttonAddNotation.setAllowStretchX(false);
    buttonAddNotation.setAlignX("right");
    buttonAddNotation.addListener("execute", function() {
      obj.addNotation(new prems.Notation());
    }, this);
    var command = new qx.ui.command.Command("Delete");
    var buttonRemoveLieu = new qx.ui.form.Button(this.tr("-"), null, command);
    command.addListener("execute", function() {
      try {
        var line = table.getSelectionModel().getSelectedRanges()[0]["maxIndex"];
        var hash = table.getTableModel().getDataAsMapArray()[line]["hash"];
        var notation = qx.core.ObjectRegistry.fromHashCode(hash);
        notation.setToDelete(true);
        obj.fireDataEvent("changeNotations", null);
      } catch (e) {
      }
    }, this);
    layoutButtons.add(buttonRemoveLieu);
    buttonRemoveLieu.setAllowStretchX(false);
    buttonRemoveLieu.setAlignX("right");
    container.add(layoutButtons);
    this.__updateTableNotations(obj, table);
    obj.addListener("changeNotations", function() {
      this.__updateTableNotations(obj, table);
    }, this);
    tableModel.addListener("dataChanged", function(e) {
      this.__updateModelTableNotations(obj, table);
    }, this);
  },
  members :
  {
    /**
     * update table notations
     *@param obj {prems.Lot} lot
     *@param table {qx.ui.table.Table} table
     */
    __updateTableNotations : function(obj, table) {
      var rows = [];
      obj.getNotations().forEach(function(notation) {
        if (notation.getToDelete() == false) {
          rows.push(notation.getRow());
        }
      }, this);
      var tableModel = table.getTableModel();
      tableModel.setDataAsMapArray(rows);
    },

    /**
     * update table model notations
     *@param obj {prems.Lot} lot
     *@param table {qx.ui.table.Table} table
     */
    __updateModelTableNotations : function(obj, table) {
      var tableModel = table.getTableModel();
      var data = tableModel.getData();
      for (var r in data) {
        var notation = qx.core.ObjectRegistry.fromHashCode(data[r][6]);
        notation.setId(data[r][0]);
        notation.setTypeNotation(data[r][1]);
        notation.setValeur(data[r][2]);
        notation.setDate(data[r][3]);
        notation.setNotateur(data[r][4]);
        notation.setSite(data[r][5]);
      }
    }
  }
});
