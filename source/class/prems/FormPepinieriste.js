/**
 * Pepinieriste form
*/
qx.Class.define("prems.FormPepinieriste",
{
  extend : qx.ui.core.Widget,
  events : {
    /**
       * Fired when the pepinieriste is saved.
       */
    "saved" : "qx.event.type.Event"
  },
  construct : function() {
    this.base(arguments);
    this.__service = prems.service.Service.getInstance();

    //Layouts
    this.__container = new qx.ui.container.Composite(new qx.ui.layout.VBox(20));
    this._setLayout(new qx.ui.layout.Grow());
    this._add(this.__container);

    //Formulaire
    this.__form = new qx.ui.form.Form();
    this.__formController = new qx.data.controller.Form(null, this.__form);

    //Textfield nom
    this.__nom = new qx.ui.form.TextField();
    this.__nom.setRequired(true);
    this.__form.add(this.__nom, this.tr("Nom"), null, "nom");

    //Textfield adresse1
    var adresse1 = new qx.ui.form.TextField();
    this.__form.add(adresse1, this.tr("Adresse 1"), null, "adresse1");

    //Textfield adresse2
    var adresse2 = new qx.ui.form.TextField();
    this.__form.add(adresse2, this.tr("Adresse 2"), null, "adresse2");

    //Textfield ville
    var ville = new qx.ui.form.TextField();
    this.__form.add(ville, this.tr("Ville"), null, "ville");

    //Textfield etat
    var etat = new qx.ui.form.TextField();
    this.__form.add(etat, this.tr("Etat"), null, "etat");

    //Textfield code postal
    var codePostal = new qx.ui.form.TextField();
    this.__form.add(codePostal, this.tr("Code postal"), null, "codePostal");

    //Textfield pays
    this.__pays = new prems.AutoCompleteBox();
    this.__service.getAutoCompleteModel("getAllPays", [], "nom", this.__pays);
    this.__form.add(this.__pays, this.tr("Pays"), null, "pays");
    this.__pays.setValue("");

    //Textfield email
    var email = new qx.ui.form.TextField();
    this.__form.add(email, this.tr("Email"), null, "email");

    //Textfield telephone
    var telephone = new qx.ui.form.TextField();
    this.__form.add(telephone, this.tr("Téléphone"), null, "telephone");

    //Textfield fax
    var fax = new qx.ui.form.TextField();
    this.__form.add(fax, this.tr("Fax"), null, "fax");
    var buttonSave = new qx.ui.form.Button(this.tr("Enregistrer"));
    this.__form.addButton(buttonSave);
    buttonSave.addListener("execute", function() {
      if (this.__validate()) {
        this.__save();
      }
    }, this);
    this.__container.add(new qx.ui.form.renderer.Single(this.__form));
  },
  members :
  {
    /**
       * Check if a field is empty.
       *@param str {String} string to check.
       *@return {Boolean} whether the string is empty or not.
       */
    isEmpty : function(str) {
      return (!str || str.length === 0);
    },

    /**
       * Validate.
       *@return {Boolean} wether the form is valid or not.
       */
    __validate : function() {
      var returnValue = true;
      this.__nom.setValid(true);

      //Test nom
      if (this.isEmpty(this.__nom.getValue())) {
        returnValue = false;
        this.__nom.setValid(false);
      }

      //Test pays
      var pays = this.__pays.getValue();
      if (!this.isEmpty(pays)) {
        var result = this.__service.syncQuery("getPays", pays);
        if (result.length == 0) {
          this.__pays.setValid(false);
          returnValue = false;
        }
      }
      return returnValue;
    },

    /**
       * Save.
       */
    __save : function() {
      // create the model
      let model = this.__formController.createModel();
      let service = null;
      if (this.__id) {
        service = this.__service.query("updatePepinieriste", [this.__id, model.getNom(), model.getAdresse1(), model.getAdresse2(), model.getVille(), model.getEtat(), model.getCodePostal(), model.getPays(), model.getEmail(), model.getTelephone(), model.getFax()]);
      } else {
        service = this.__service.query("createPepinieriste", [model.getNom(), model.getAdresse1(), model.getAdresse2(), model.getVille(), model.getEtat(), model.getCodePostal(), model.getPays(), model.getEmail(), model.getTelephone(), model.getFax()]);
      }
      service.addListener("changeResponseModel", function() {
        if (service.noError()) {
          this.fireEvent("saved");
          this.__service.fireEvent("dbChange");
        } else {
          new prems.Dialog("Erreur"); // TODO To be replaced by event
        }
      }, this);
    },

    /**
       * Set id pepinieriste.
       *@param id {Integer} id pepinieriste.
       */
    setId : function(id) {
      this.__id = id;
      var service = this.__service.query("getPepinieristeById", [id]);
      service.addListener("changeResponseModel", function() {
        var controller = new qx.data.controller.Form(null, this.__form);
        var model = controller.createModel();
        model.setNom(service.getResult()[0]["nom"]);
        model.setAdresse1(service.getResult()[0]["adresse1"]);
        model.setAdresse2(service.getResult()[0]["adresse2"]);
        model.setVille(service.getResult()[0]["ville"]);
        model.setEtat(service.getResult()[0]["etat"]);
        model.setCodePostal(service.getResult()[0]["code_postal"]);
        model.setPays(service.getResult()[0]["pays"]);
        model.setEmail(service.getResult()[0]["email"]);
        model.setTelephone(service.getResult()[0]["telephone"]);
        model.setFax(service.getResult()[0]["fax"]);
      }, this);
    }
  }
});
