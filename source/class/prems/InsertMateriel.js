/**
* Insert materiel.
* etape 1 : __validate : validation des types de noms et de formats
*
* etape 2  : __getConformityToDB : vérifie la conformité et recupère les id
*           renvoie 3 valeurs : errors, infos, data
* etape 3 ; __saveToDb
*         sauvegarde dans la base
* TODO re-definir les colonnes obligatoires dans le fichier data
*/
qx.Class.define("prems.InsertMateriel", {
  extend : qx.ui.core.Widget,
  events : {
    /**
    * Fired when at the end of the validation.
    */
    "validateEnd" : "qx.event.type.Data",
    /**
    * Fired at the end of the conformity
    */
    "conform" : "qx.event.type.Data",
    /**
    * Fired at the end of the import.
    */
    "end" : "qx.event.type.Data",
    /**
    * Fired when a line is saved.
    */
    "lineSaved" : "qx.event.type.Data",
    /**
    * Fired when all the data is retrieved.
    */
    "getDataEnd" : "qx.event.type.Data"
  },

  properties : {
    experiment : {
      nullable : true,
      event : "changeExperiment"
    },
    userlot : {
      nullable : true,
      event : "changeUserlot"
    },
    usergroup : {
      nullable : false,
      event : "changeUsergroup"
    }
  },
  //constructeur
  construct : function() {
    this.base(arguments);
    this.__listeVariete = [];
    this.__listeLot = [];
    this.__listeAccession = [];
    this.__cols = [];
    this.__lines = [];

    //Service
    this.__service = prems.service.Service.getInstance();
    //this.__servicePlant = prems.ServicePlant.getInstance();
    this.__serviceNot = prems.service.notation.ServiceNotation.getInstance();
    let session = qxelvis.session.service.Session.getInstance();

    /* -- Data Management --*/

    /*-- UI drawing --*/
    this._setLayout(new qx.ui.layout.Grow());

    //Containers
    this.__container = new qx.ui.container.Composite(new qx.ui.layout.VBox(5));
    this._add(this.__container);
    // create the form
    this.__form = new qx.ui.form.Form();
    this.__container.add(new qx.ui.form.renderer.Single(this.__form));

    //textfield du user
    let userlotTF = new qx.ui.form.TextField();
    this.__form.add(userlotTF, "user");
    //this.setUserlot("");
    if (session.userLoggedIn()) {
      this.setUserlot(session.getUserInfo().getFullName());
    }
    this.bind("userlot", userlotTF, "value");
    userlotTF.bind("value", this, "userlot");

    //textefield du userGroup
    let usergroupTF = new qx.ui.form.TextField();
    this.__form.add(usergroupTF, "users Group");

    //Fenêtre choix du fichier
    let fileChooser = new qxfileio.FileChooser();
    let fr = new qxfileio.FileReader();

    //Button ouvrir fichier
    this.__fileButton = new qx.ui.form.Button("Ouvrir fichier...");
    this.__form.addButton(this.__fileButton);

    //Bouton insérer
    this.__insertButton = new qx.ui.form.Button("Insérer");
    this.__form.addButton(this.__insertButton);
    this.__insertButton.setEnabled(false);

    //Boite affichage des messages d'erreurs
    this.__remTA = new qx.ui.form.TextArea();
    this.__container.add(this.__remTA, {flex: 1});

    //Loading gif
    let loading = new qx.ui.basic.Image("prems/loading.gif");
    loading.setAlignX("center");
    this.__container.add(loading);
    loading.exclude();

    //Bouton pour télécharger le fichier exemple
    let helpButton = new qx.ui.form.Button(this.tr("Aide"));
    this.__container.add(helpButton);

    /* -- Listeners --*/

    helpButton.addListener("execute", function() {
      let testFile = new prems.ImportFileExample();
      let fileWriter = new qxfileio.FileWriter();
      fileWriter.saveTextAsFile(testFile.getFile(), "Import_format_plantDb.csv");
    }, this);

    //Bouton choix du fichier
    this.__fileButton.addListener("execute", function() {
      fileChooser.open();
    }, this);

    //Ouverture de la fenêtre de choix de fichier
    fileChooser.addListener("filesChange", function(e) {
      if (e.getData().length > 0) {
        this.__file = e.getData()[0];
        this.__remTA.setValue("Fichier ouvert: " +e.getData()[0]["name"]);
        fr.loadAsText(e.getData()[0]);
      }
    }, this);
    //Conformite du fichier'
    this.addListener("conform", function(e) {
      this.__fileButton.setEnabled(true);
      loading.exclude();
    }, this);
    //Fin de l'import du fichier'
    this.addListener("end", function(e) {
      this.__fileButton.setEnabled(true);
      this.__insertButton.setEnabled(false);
      loading.exclude();
    }, this);

    //Bouton insérer
    this.__insertButton.addListener("execute", function() {
      this.__fileButton.setEnabled(false);
      this.__insertButton.setEnabled(false);
      this.__saveToDb();
    }, this);



    //FileReader
    fr.addListener("load", function(e) {
      loading.show();
      //code de fin de ligne pour les fichiers unix "\n" ou les fichiers windows "\r"
      let data = e.getData();
      let tabFile = "";
      if (data.indexOf("\r")==-1) {
        tabFile = data.split("\n");
      } else {
        tabFile = data.split("\r\n");
      }

      //Noms de colonnes
      let cols = tabFile[0].split("\t");
      //Données
      let lines = [];
      for (let i = 1; i < tabFile.length; i++) {
        let line = tabFile[i].split("\t");
        lines.push(line);
      }
      this.__lines = lines;
      this.__cols = cols;
      this.__validate(this.__cols, this.__lines);
    }, this);

    this.addListener("validateEnd", function(e) {
      this.__getConformityToDB(this.__cols, this.__lines);
    });
  },
  members : {
    __listeVariete : null,
    __listeAccession : null,
    __listeLot : null,
    __cols : null,
    __lines : null,
    __varietyConform : null,
    __accessionConform : null,
    __lotConform : null,
    __errorLog : null,

    __setLog : function(value) {
      this.__remTA.setValue(value);
    },

    __appendLog : function(value) {
      var logField = this.__remTA;
      var v = logField.getValue().trim();
      if (v == "") {
        this.__setLog(value);
      } else {
        logField.setValue(logField.getValue() + "\n" + value);
      }
    },
    /**
    * Validate the file.
    * @param cols {Array} columns
    * @param lines {Array} lines
    */

    __validate : function(cols, lines) {
      //   let listColumn = ["Genre", "Espèce", "Cultivée", "Obtenteur", "Éditeur",
      //   "Nom accession", "Collection", "Date nom",
      //   "Remarque variété", "Numéro accession",
      //   "Date introduction", "Fournisseur",
      //   "Numéro lot", "Type lot",
      //   "Date récolte", "Date multiplication", "Quantité",
      //   "Site", "Collecteur"
      // ];
      let genreRequired = false;
      let especeRequired = false;
      let numeroAccessionRequired = false;
      let numeroLotRequired = false;
      let nomRequired = false;
      let typeLotRequired = false;
      let numberLines = -1;
      // let typeNom = "";
      // let typeLieu = "";
      for (let i in cols) {
        //Test des types de noms
        if (cols[i].indexOf("Nom:") > -1) {
          nomRequired = true;
          // typeNom = cols[i].split(":")[1];
        }
        //Test des types de lieux
        // if (cols[i].indexOf("Lieu:") > -1) {
        //   typeLieu = cols[i].split(":")[1];
        // }
        if (cols[i] == "Numéro accession") {
          numeroAccessionRequired = true;
        }
        if (cols[i] == "Numéro lot") {
          numeroLotRequired = true;
        }
        if (cols[i] == "Type lot") {
          typeLotRequired = true;
        }
        if (cols[i] == "Genre") {
          genreRequired = true;
        }
        if (cols[i] == "Espèce") {
          especeRequired = true;
        }
      }
      if (nomRequired == false) {
        this.__appendLog("Au moins un nom de lignée ou de variété obligatoire.");
      }
      if (numeroAccessionRequired == false) {
        this.__appendLog("La colonne \"Numéro accession\" est obligatoire.");
      }
      if (numeroLotRequired == false) {
        this.__appendLog("La colonne \"Numéro lot\" est obligatoire.");
      }
      if (typeLotRequired == false) {
        this.__appendLog("La colonne \"Type lot\" est obligatoire (arbre ou graines).");
      }
      if (genreRequired == false) {
        this.__appendLog("La colonne \"Genre\" est obligatoire.");
      }
      if (especeRequired == false) {
        this.__appendLog("La colonne \"Espèce\" est obligatoire. ");
      }
      for (let i in lines) {
        numberLines += 1;
        for (let j in lines[i]) {
          //Valeur de la cellule
          let cellValue = lines[i][j];
          if (cellValue != "") {
            //Test des collections
            //Test des dates
            if (cols[j] == "Date récolte" || cols[j] == "Date mort" ||
            cols[j] == "Date multiplication" || cols[j] == "Date nom" ||
            cols[j] == "Date collecte") {
              if (!prems.DataTypeValidator.isDate(cellValue)) {
                this.__appendLog("Mauvais format de la date :" + cellValue);
              }
            }

            //Type lot
            if (cols[j] == "Type lot") {
              if (cellValue != "arbre" && cellValue != "graines") {
                this.__appendLog("Le type de lot \"" + cellValue + "\" n'existe pas (arbre ou graines).");
              }
            }
          }
        }
      }
      this.__appendLog("Le fichier contient : " + numberLines +" lots");
      this.fireDataEvent("validateEnd");
    },

    /**
    * verifie la conformité avec les données dans la base.
    * @param cols {Array} columns
    * @param lines {Array} lines
    */
    __getConformityToDB : function(cols, lines) {
      this.__varietes = [];
      //Lignes
      for (let i in lines) {
        if (lines[i].length > 2) {
          //Une variété par ligne
          let variete = new prems.data.plant.Variety();
          let clone = new prems.data.plant.Accession();
          let lot = new prems.data.plant.Lot();
          let emplacement = new prems.data.plant.Place();
          lot.setPlace(emplacement);
          //let collections = [];

          //Colonnes
          for (let j in lines[i]) {
            //Valeur de la cellule
            let cellValue = lines[i][j];
            let nom = null;
            if (cellValue != "") {
              /////////Variété////////////////
              if (cols[j].indexOf("Nom:") > -1) {
                nom = new prems.data.plant.VarietyName();
                nom.setValue(cellValue.split(":")[0].trim());
                nom.setType(cols[j].split(":")[1].trim());
                variete.addNom(nom);
                if (cols[x] == "Date nom") {
                  nom.setDate(lines[i][j + 1].trim());
                }
              }
              if (cols[j] == "Obtenteur") {
                let person = new prems.data.addressBook.Person();
                person.setLastname(cellValue.trim());
                variete.setBreeder(cellValue.trim());
              }
              if (cols[j] == "Éditeur") {
                let person = new prems.data.addressBook.Person();
                person.setLastname(cellValue.trim());
                variete.setEditor(cellValue.trim());
              }
              if (cols[j] == "Genre") {
                variete.setGenus(cellValue.trim());
              }
              if (cols[j] == "Espèce") {
                variete.setSpecie(cellValue.trim());
              }
              if (cols[j] == "Remarque variété") {
                variete.setComment(cellValue.trim());
              }

              // /////////Clone////////////////
              if (cols[j] == "Nom accession") {
                clone.setIntroductionName(cellValue.trim());
              }
              if (cols[j] == "Remarque accession") {
                clone.setComment(cellValue.trim());
              }
              if (cols[j] == "Fournisseur") {
                clone.setProvider(cellValue.trim());
              }
              if (cols[j] == "Date introduction") {
                clone.setIntroductionDate(cellValue.trim());
              }
              /////////Lot////////////////
              if (cols[j] == "Numéro lot") {
                lot.setName(cellValue.trim());
              }
              if (cols[j] == "Type lot") {
                lot.setType(cellValue.trim());
              }
              if (cols[j] == "Date multiplication") {
                lot.setMultiplicationDate(cellValue.trim());
              }
              if (cols[j] == "Date récolte") {
                lot.setHarvestingDate(cellValue.trim());
              }
              if (cols[j] == "Quantité") {
                cellValue = cellValue.replace(",", ".");
                lot.setQuantity(cellValue.trim());
              }
              if (cols[j] == "Date récolte") {
                lot.setHarvestingDate(cellValue.trim());
              }
              if (cols[j] == "Date mort") {
                lot.setDestructionDate(cellValue.trim());
              }
              if (cols[j].indexOf("Lieu:") != -1) {
                emplacement.addLocation(cols[j].split(":")[1], cellValue);
              }
            } //si cellule est non nulle
          } //fin de chaque colonne
          this.__listeVariete.push(variete);
          this.__listeAccession.push(clone);
          this.__listeLot.push(lot);
        } //si longueur de la ligne
      } //fin de chaque ligne

      let serviceVar = prems.service.plant.ServiceVariety.getInstance();
      this.__serviceAccession = prems.service.plant.ServiceAccession.getInstance();
      this.__serviceLot = prems.service.plant.ServiceLot.getInstance();

      // getconformity pour variety accession et lot imbriqués
      // si les 3 ok fireDataEvent conform et end
      serviceVar.addListenerOnce("gotConformityVarietyList", function(e) {
        //["result"][0] = error message
        //["result"][1] = infos message
        //["result"][2] = variety list
        this.__appendLog(e.getData()["result"][0]);
        this.__appendLog(e.getData()["result"][1]);
        this.__errorLog = this.__errorLog + e.getData()["result"][0];
        this.__varietyconform = e.getData()["result"][2];
        this.__serviceAccession.addListenerOnce("gotConformityAccessionList", function(e) {
          this.__appendLog(e.getData()["result"][0]);
          this.__appendLog(e.getData()["result"][1]);
          this.__errorLog = this.__errorLog + e.getData()["result"][0];
          this.__accessionConform = e.getData()["result"][2];
          this.__serviceLot.addListenerOnce("gotConformityLotList", function(e) {
            this.__appendLog(e.getData()["result"][0]);
            this.__appendLog(e.getData()["result"][1]);
            this.__errorLog = this.__errorLog + e.getData()["result"][0];
            this.__lotConform = e.getData()["result"][2];

            this.fireDataEvent("conform", null);
            // valide le bouton save
            if (this.__errorLog === "null") {
              this.__insertButton.setEnabled(true);
            } else {
              this.__insertButton.setEnabled(false);
            }
          }, this);
          this.__serviceLot.getConformityLotList(this.__listeLot);
        }, this);
        this.__serviceAccession.getConformityAccessionList(this.__listeAccession);
      }, this);
      serviceVar.getConformityVarietyList(this.__listeVariete);
    }, /// fin du .__getConformityToDB(cols, lines)

    /**
    * Save to database.
    * @param i {Integer} index of the line.
    */
    __saveToDb : function(i) {
      // FIXME Broken due to ServiceLot not having setlotList method

      let serviceVar = prems.service.plant.ServiceVariety.getInstance();
      serviceVar.saveVarieties(this.__varietyconform);
      serviceVar.addListenerOnce("varietiesListSaved", function(e) {
        this.debug("varietiesListSaved");
      }, this);

      let serviceLot = prems.service.plant.ServiceLot.getInstance();
      serviceLot.saveLots(this.__listeVariete, this.__listeAccession, this.__listeLot);
      this.fireDataEvent("end", null);
    }
  }
});
