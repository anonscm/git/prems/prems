/**
 * Recherche variétés, lots, accessions
*/
qx.Class.define("prems.PepinieristeDescripteur",
{
  extend : qx.ui.core.Widget,
  construct : function() {
    this.base(arguments);
    this.__container = new qx.ui.container.Composite(new qx.ui.layout.VBox(20));
    this._setLayout(new qx.ui.layout.Grow());
    this._add(this.__container);
    this.__service = prems.service.Service.getInstance();
    var form = new qx.ui.form.Form();
    this.__table = new qx.ui.table.Table();

    //boutons
    this.__buttonEdit = new qx.ui.form.Button(this.tr("Modifier"));
    this.__buttonNew = new qx.ui.form.Button(this.tr("Nouveau contact"));
    form.addButton(this.__buttonNew);
    //suppression du bouton moodifier
    //form.addButton(this.__buttonEdit);
    this.__container.add(new qx.ui.form.renderer.Single(form));
    //this.__table.addListener("dblclick", this.__edit, this);
    this.__buttonEdit.addListener("execute", this.__edit, this);
    this.__buttonNew.addListener("execute", this.__new, this);
    this.__update();
  },
  members :
  {
    /**
      * edit
      */
    __edit : function() {
      try {
        var row = this.__table.getSelectionModel().getSelectedRanges()[0]["minIndex"];
        var rowData = this.__table.getTableModel().getRowDataAsMap(row);
        var id = rowData["id_pepinieriste"];
        var form = new prems.FormPepinieriste();
        form.setId(id);
        var win = new prems.MyWindow(this.tr("Modifier contact"));
        win.setLayout(new qx.ui.layout.Grow());
        win.add(form);
        win.open();
        win.center();
        form.addListener("saved", function() {
          this.__update();
          win.close();
        }, this);
      } catch (error) {
      }
    },

    /**
     * new
     */
    __new : function() {
      var formPepinieriste = new prems.FormPepinieriste();
      var win = new prems.MyWindow(this.tr("Modifier contact"));
      win.setLayout(new qx.ui.layout.Grow());
      win.add(formPepinieriste);
      win.open();
      win.center();
      formPepinieriste.addListener("saved", function() {
        this.__update();
        win.close();
      }, this);
    },

    /**
     * update
     */
    __update : function() {
      var service = this.__service.query("getAllPepinieristePays");
      service.addListener("changeResponseModel", function() {
        var data = service.getResult();
        this.__model = new qx.ui.table.model.Filtered();
        if (data.length != 0) {
          this.__model.setColumns(Object.keys(data[0]));
        }
        this.__model.setRowsAsMapArray(data);
        this.__table.setTableModel(this.__model);
        this.__table.getTableColumnModel().setColumnVisible(6, false);
        this.__table.getTableColumnModel().setColumnVisible(9, false);
        this.__table.getTableColumnModel().setColumnsOrder([1, 8, 10, 4, 0, 3, 5, 11, 7, 2, 6, 9]);

        //this.__table.getTableColumnModel().setColumns(["Nom", "Adresse 1", "Adresse 2", "Ville", "Etat", "Code postal", "Pays", "Email", "Téléphone", "Fax", "id_pepinieriste", "id_pays"]);
        this.__container.addAt(this.__table, 0);
        this.__table.show();
      }, this);
    }
  }
});
