/**
* Lieu object.
* Stores all the informations of the Lieu table.
*/
qx.Class.define("prems.Lieu", {
  extend : qx.core.Object,
  events : {
    /**
    * Fired when the emplacement is saved in the database.
    */
    "saved" : "qx.event.type.Event",

    /**
    * Fired when the emplacement is retrieved from the database.
    */
    "getEnd" : "qx.event.type.Event"
  },
  properties : {
    /**
    * id
    */
    id : {
      init : null,
      nullable : true,
      check : "Integer",
      event : "changeId"
    },

    /**
    * type
    */
    type : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeType"
    },

    /**
    * valeur
    */
    valeur : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeValeur"
    },

    /**
    * toDelete
    */
    toDelete : {
      init : false,
      check : "Boolean",
      event : "changeToDelete"
    }
  },

  /**
  * constructor
  * @param idLieu {Integer} id of the lieu if it exists in the database.
  */
  construct : function(idLieu) {
    this.base(arguments);

    //Service
    this.__service = prems.service.Service.getInstance();

    //Sets the variete id
    if (idLieu) {
      this.setId(idLieu);
      this.get();
    }
  },
  members : {
    /**
    * Saves (updates or creates)
    * @param idEmplacement {Integer} id emplacement.
    */
    save : function(idEmplacement) {
      if (this.getToDelete()) {
        this.remove();
      } else {
        var serviceTypeLieu = this.__service.query("getTypeLieu", [this.getType()]);
        serviceTypeLieu.addListener("changeResponseModel", function() {
          try {
            var idTypeLieu = serviceTypeLieu.getResult()[0]["id"];
          } catch (e) {
            idTypeLieu = null;
          }
          if (this.getId() === null) {
            this.create(idEmplacement, idTypeLieu);
          } else {
            this.update(idEmplacement, idTypeLieu);
          }
        }, this);
      }
    },

    /**
    * Get
    */
    get : function() {
      var serviceLieu = this.__service.query("getLieu", [this.getId()]);
      serviceLieu.addListener("changeResponseModel", function() {
        this.setValeur(serviceLieu.getResult()[0]["valeur"]);
        var idTypeLieu = serviceLieu.getResult()[0]["type_lieu"];
        var serviceTypeLieu = this.__service.query("getTypeLieuById", [idTypeLieu]);
        serviceTypeLieu.addListener("changeResponseModel", function() {
          this.setType(serviceTypeLieu.getResult()[0]["type"]);
          this.fireEvent("getEnd");
        }, this);
      }, this);
    },

    /**
    * Remove from the database.
    */
    remove : function() {
      var service = this.__service.query("deleteLieu", [this.getId()]);
      service.addListener("changeResponseModel", function() {
        this.fireEvent("saved");
      }, this);
    },

    /**
    * Create
    * @param idEmplacement {Integer} id emplacement
    * @param typeLieu {String} type lieu
    */
    create : function(idEmplacement, typeLieu) {
      var serviceLieu = this.__service.query("createLieu", [idEmplacement, typeLieu, this.getValeur()]);
      serviceLieu.addListener("changeResponseModel", function() {
        this.fireEvent("saved");
      }, this);
    },

    /**
    * Update
    * @param idEmplacement {Integer} id emplacement
    * @param typeLieu {String} type lieu
    */
    update : function(idEmplacement, typeLieu) {
      var serviceLieu = this.__service.query("updateLieu", [this.getId(), typeLieu, this.getValeur()]);
      serviceLieu.addListener("changeResponseModel", function() {
        this.fireEvent("saved");
      }, this);
    },

    /**
    * Update
    * @return {Object} object to show in the tables.
    */
    getRow : function() {
      return {
        "id" : this.getId(),
        "type" : this.getType(),
        "valeur" : this.getValeur(),
        "hash" : this.toHashCode()
      };
    },

    /**
    * Update
    * @return {Object} object to write in the file for export.
    */
    toCsv : function() {
      var data = {

      };
      var colName = "Lieu:" + this.getType();
      data[colName] = this.getValeur();
      return data;
    }
  }
});
