/**
* Type nom form
*/
qx.Class.define("prems.FormTypeNom",
{
  extend : qx.ui.core.Widget,
  events : {
    /**
    * Fired when the type nom is saved.
    */
    "saved" : "qx.event.type.Event"
  },
  construct : function() {
    this.base(arguments);

    //Layouts
    this.__container = new qx.ui.container.Composite(new qx.ui.layout.VBox(20));
    this._setLayout(new qx.ui.layout.Grow());
    this._add(this.__container);

    //Service
    this.__service = prems.service.Service.getInstance();
    var form = new qx.ui.form.Form();
    this.__typeTf = new qx.ui.form.TextField();
    form.add(this.__typeTf, this.tr("Type nom"), null, "type");
    var saveButton = new qx.ui.form.Button(this.tr("Enregistrer"));
    form.addButton(saveButton);
    saveButton.addListener("execute", function() {
      if (this.__validate()) {
        this.__save();
      }
    }, this);
    this.__container.add(new qx.ui.form.renderer.Single(form));
  },
  members :
  {
    /**
    * Check if a field is empty.
    *@param str {String} string to check.
    *@return {Boolean} whether the string is empty or not.
    */
    isEmpty : function(str) {
      return (!str || str.length === 0);
    },

    /**
    * Validate.
    *@return {Boolean} wether the form is valid or not.
    */
    __validate : function() {
      var returnValue = true;
      this.__typeTf.setValid(true);
      if (this.isEmpty(this.__typeTf.getValue())) {
        this.__typeTf.setValid(false);
      }
      return returnValue;
    },

    /**
    * Save.
    */
    __save : function() {
      let typeNom = this.__typeTf.getValue();
      let service = null;
      if (!this.__idTypeNom) {
        service = this.__service.query("createTypeNom", [typeNom]);
      } else {
        service = this.__service.query("updateTypeNom", [typeNom, this.__idTypeNom]);
      }
      service.addListener("changeResponseModel", function() {
        if (service.noError()) {
          this.fireEvent("saved");
          this.__service.fireEvent("dbChange");
        } else {
          new prems.Dialog("Erreur"); // TODO To be replaced by event
        }
      }, this);
    },

    /**
    * Set id type nom.
    *@param idTypeNom {Integer} id type nom.
    */
    setId : function(idTypeNom) {
      this.__idTypeNom = idTypeNom;
      if (this.__idTypeNom !== null) {
        var service = this.__service.query("getTypeNomById", [this.__idTypeNom]);
        service.addListener("changeResponseModel", function() {
          this.__typeTf.setValue(service.getResult()[0]["valeur"]);
        }, this);
      }
    }
  }
});
