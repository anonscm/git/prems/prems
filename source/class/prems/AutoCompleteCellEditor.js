/**
 * AutoCompleteCellEditor
 * Displays AutoCompleteBox in table cells.
 * Aurélie Lelievre
 */
qx.Class.define("prems.AutoCompleteCellEditor",
{
  extend : qx.ui.table.celleditor.AbstractField,
  construct : function(type) {
    //Service
    this.__service = prems.service.Service.getInstance();

    //Type (typeLieux,typeNoms,nom,site,typeNotation,collection)
    this.__type = type;
  },
  members :
  {
    /**
     * _createEditor
     * @return {prems.AutoCompleteBox} return the field (AutocompleteBox).
     *
     */
    _createEditor : function() {
      var field = new prems.AutoCompleteBox();

      //Groupes indidpensables pour certaines requêtes.
      var session = qxelvis.session.service.Session.getInstance();
      var userGroups = session.getUserGroupsInfo().getReadGroupIds();

      switch (this.__type) {
        case "typeLieu":
          this.__service.getAutoCompleteModel("getAllTypeLieu", [], "type", field);
          break;
        case "typeNom":
          this.__service.getAutoCompleteModel("getAllTypeNom", [], "valeur", field);
          break;
        case "nom":
          this.__service.getAutoCompleteModel("getAllNomVariete", [userGroups], "nom", field);
          break;
        case "site":
          this.__service.getAutoCompleteModel("getAllSites", [], "nom_site", field);
          break;
        case "typeNotation":
          this.__service.getAutoCompleteModel("getAllTypeNotation", [], "nom", field);
          break;
        case "collection":
          this.__service.getAutoCompleteModel("getAllCollection", [], "nom", field);
          break;
      }
      return field;
    },

    /**
        * createCellEditor
        * @param cellInfo {prems.AutoCompleteBox} cell info.
        * @return  {prems.AutoCompleteBox} cell editor.
        */
    createCellEditor : function(cellInfo) {
      var cellEditor = this._createEditor();
      cellEditor.originalValue = cellInfo.value;
      if (cellInfo.value === null || cellInfo.value === undefined) {
        cellInfo.value = null;
      }
      cellEditor.setValue(cellInfo.value);
      cellEditor.addListener("appear", function() {
        //cellEditor.selectAllText();
      });
      return cellEditor;
    }
  }
});
