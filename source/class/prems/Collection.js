/**
 * Lieu object.
 * Stores all the informations of the collection.
*/
qx.Class.define("prems.Collection",
{
  extend : qx.core.Object,
  events :
  {
    /**
     * fired when the collection is saved in the database.
     */
    "saved" : "qx.event.type.Event",

    /**
     * Fired chen the collection is retrieved from the database.
     */
    "getEnd" : "qx.event.type.Event"
  },
  properties :
  {
    /**
    * nom
    */
    nom :
    {
      init : null,
      nullable : true,
      check : "String",
      event : "changeValeur"
    },

    /**
    * toDelete
    */
    toDelete :
    {
      init : false,
      check : "Boolean",
      event : "changeToDelete"
    }
  },

  /**
   * constructor
   */
  construct : function() {
    this.base(arguments);

    //Service
    this.__service = prems.service.Service.getInstance();
  },
  members :
  {
    /**
       * Saves
       *@param idClone {Interger} id clone
       */
    save : function(idClone) {
      let service = null;
      if (this.getToDelete() == true) {
        service = this.__service.query("removeCloneFromCollection", [this.getNom(), idClone]);
      } else {
        service = this.__service.query("addCloneToCollection", [this.getNom(), idClone]);
      }
      service.addListener("changeResponseModel", function() {
      }, this);
    },

    /**
       * Get
       */
    get : function() {
    },

    /**
       * Remove
       */
    remove : function() {
    },

    /**
       * Create
       */
    create : function() {
    },

    /**
       * Update
       */
    update : function() {
    },

    /**
       * Get row to display in tables
       *@return {Object} object
       */
    getRow : function() {
      return {
        "nom" : this.getNom(),
        "hash" : this.toHashCode()
      };
    }
  }
});
