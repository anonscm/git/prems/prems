/**
* Présentation des information d'une variete le formulaire de recherche
* fabrice février 2019
*/
qx.Class.define("prems.PresentationVariete", {
  extend : qx.ui.core.Widget,
  properties : {
    id : {
      init : null,
      nullable : true,
      check : "Integer",
      event : "changeId"
    },
    cultivated : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeCultivated"
    },
    genus : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeGenus"
    },
    specie : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeSpecie"
    },
    allNames : {
      init : null,
      check : "new qx.ui.basic.Label()",
      nullable : true,
      event : "changeAllNames"
    },
    informations : {
      init : null,
      check : "new qx.ui.basic.Label()",
      nullable : true,
      event : "changeInformations"
    },
    taxonomy : {
      init : null,
      check : "new qx.ui.basic.Label()",
      nullable : true,
      event : "changeTaxonomy"
    },
    breeder : {
      init : null,
      check : "new qx.ui.basic.Label()",
      nullable : true,
      event : "changeBreeder"
    },
    editor : {
      init : null,
      check : "new qx.ui.basic.Label()",
      nullable : true,
      event : "changeEditor"
    }
  },

  construct : function() {
    this.base(arguments);
    //service
    this.__service = prems.service.Service.getInstance();
    this.__servicePlant = prems.ServicePlant.getInstance();
    this.__serviceNot = prems.ServiceNotation.getInstance();

    /* -- Data Management --*/
    this.setTaxonomy(new qx.ui.basic.Label());
    this.setAllNames(new qx.ui.basic.Label());
    this.setInformations(new qx.ui.basic.Label());
    this.setBreeder(new qx.ui.basic.Label());
    this.setEditor(new qx.ui.basic.Label());

    /*-- UI drawing --*/
    this._setLayout(new qx.ui.layout.VBox());
    this.__container = new qx.ui.container.Composite(new qx.ui.layout.VBox(10));
    this.__container.setAllowStretchX(true, true);
    this.__container.setAllowStretchY(true, true);
    this._add(this.__container, {flex : 1});
    this.__container.add(this.getTaxonomy());
    this.__container.add(this.getAllNames());
    this.__container.add(this.getInformations());
    this.__container.add(this.getBreeder());
    this.__container.add(this.getEditor());
    this.getTaxonomy().setVisibility("excluded");
    this.getAllNames().setVisibility("excluded");
    this.getInformations().setVisibility("excluded");
    this.getBreeder().setVisibility("excluded");
    this.getEditor().setVisibility("excluded");

    /* -- Listeners --*/
    this.addListener("changeId", function() {
      let session = qxelvis.session.service.Session.getInstance();
      let sessionId = session.getSessionId();
      let servicePlantName = this.__servicePlant.query("getAllNamesByVariete", [sessionId, this.getId()]);
      servicePlantName.addListener("changeResponseModel", function(e) {
        let str = "Noms : ";
        let info = "";
        let taxonom = "";
        if (e.getData()["result"].length > 0) {
          let r = e.getData()["result"];
          for (let l in r) {
            if (r[l]["date"] === null) {
              str = str + r[l]["type"].toString() + " : "+r[l]["value"].toString() + " ; ";
            } else {
              str = str + r[l]["type"].toString() + " : "+r[l]["value"].toString() + " ("+ r[l]["date"] +") ; ";
            }
          }
          this.getAllNames().setValue(str);
          this.getAllNames().setVisibility("visible");
          if (r[0]["genus"] !== null | r[0]["species"] !== null) {
            taxonom = "Genre : "+ r[0]["genus"]+" ; Espèce : "+r[0]["species"];
            this.getTaxonomy().setValue(taxonom);
            this.getTaxonomy().setVisibility("visible");
          }

          if (r[0]["comment"] !== null) {
            this.getInformations().setValue("Remarques : "+ r[0]["comment"]);
            this.getInformations().setVisibility("visible");
          }
          if (r[0]["breeder_firstname"] !== null || r[0]["breeder_lastname"] !== null) {
            info = " Obtenteur : ";
            if (r[0]["breeder_firstname"] !== null) {
              info = info + r[0]["breeder_firstname"] + " ";
            }
            if (r[0]["breeder_lastname"] !== null) {
              info = info + r[0]["breeder_lastname"] + " ";
            }
            this.getBreeder().setValue(info);
            this.getBreeder().setVisibility("visible");
          }
          if (r[l]["editor_firstname"] !== null || r[l]["editor_lastname"] !== null) { // FIXME l is not defind here
            info = " Editeur : ";
            if (r[l]["editor_firstname"] !== null) {
              info = info + r[0]["editor_firstname"] + " ";
            }
            if (r[l]["editor_lastname"] !== null) {
              info = info + r[0]["editor_lastname"] + " ";
            }
            this.getEditor().setValue(info);
            this.getEditor().setVisibility("visible");
          }
        }
        var userGroups = [];
        var session = qxelvis.session.service.Session.getInstance();
        var sessionId = session.getSessionId();
        if (session.getUserGroupsInfo() !== null) {
          userGroups = session.getUserGroupsInfo().getReadGroupIds();
        }
        var serviceVarNot = this.__serviceNot.query("getNotationsForVariety", [sessionId, userGroups, null, this.getId()]);
        serviceVarNot.addListener("changeResponseModel", function(e) {
          var result = [];
          if (e.getData()["result"].length > 0) {
            var r = e.getData()["result"];
            for (let l in r) {
              result.push({
                "Notation":r[l]["name"],
                "Value":r[l]["value"].toString(),
                "experimentator":r[l]["experimentator"],
                "date":r[l]["date"]
              });
            }
            //cree une nouvelle table
            var tableResult = new qx.ui.table.Table();
            tableResult.setHeight(100);
            tableResult.setAllowStretchX(true, true);
            tableResult.setAllowStretchY(true, true);
            var tableModel = new qx.ui.table.model.Filtered();
            tableModel.setColumns(Object.keys(result[0]));
            tableModel.setDataAsMapArray(result);
            tableResult.setTableModel(tableModel);
            var tcm = tableResult.getTableColumnModel();
            var tm = tableResult.getTableModel();
            for (let i =0; i< tm.getColumnCount(); i++) {
              tcm.setColumnVisible(i, true);
            }
            if (this.getCultivated()) {
              this.__container.add(new qx.ui.basic.Label(this.tr("Notations sur la variété : ")));
            } else {
              this.__container.add(new qx.ui.basic.Label(this.tr("Notations sur la lignée : ")));
            }
            this.__container.add(tableResult, {flex : 1});
          }
        }, this);//listener notations
      }, this);//listener names
    }, this); //listener changeId
  }
});
