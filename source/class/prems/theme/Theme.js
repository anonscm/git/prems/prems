/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("prems.theme.Theme",
{
  meta :
  {
    color : prems.theme.Color,
    decoration : prems.theme.Decoration,
    font : prems.theme.Font,
    icon : qx.theme.icon.Tango,
    appearance : prems.theme.Appearance
  }
});
