/**
 * Ajouter, modfifier la taxinomie
*/
qx.Class.define("prems.TaxinomieDescripteur",
{
  extend : qx.ui.core.Widget,
  construct : function(critere) {
    this.base(arguments);
    this.__container = new qx.ui.container.Composite(new qx.ui.layout.VBox(20));
    this._setLayout(new qx.ui.layout.Grow());
    this._add(this.__container);
    this.__service = prems.service.Service.getInstance();
    this.__critere = critere;
    var form = new qx.ui.form.Form();
    this.__table = new qx.ui.table.Table();

    //boutons
    this.__buttonEdit = new qx.ui.form.Button(this.tr("Modifier"));
    this.__buttonNew = new qx.ui.form.Button(this.tr("Nouvelle taxinomie"));
    form.addButton(this.__buttonNew);
    //suppression du bouton moodifier
    //form.addButton(this.__buttonEdit);
    this.__container.add(new qx.ui.form.renderer.Single(form));
    //this.__table.addListener("dblclick", this.__edit, this);
    this.__buttonEdit.addListener("execute", this.__edit, this);
    this.__buttonNew.addListener("execute", this.__new, this);
    this.__update();
  },
  members :
  {
    /**
     * edit
     */
    __edit : function() {
      try {
        var row = this.__table.getSelectionModel().getSelectedRanges()[0]["minIndex"];
        var rowData = this.__table.getTableModel().getRowDataAsMap(row);
        var id = rowData["id"];
        var form = new prems.FormTaxinomie();
        form.setId(id);
        var win = new prems.MyWindow(this.tr("Modifier taxinomie"));
        win.setLayout(new qx.ui.layout.Grow());
        win.add(form);
        win.open();
        win.center();
        form.addListener("saved", function() {
          this.__update();
          win.close();
        }, this);
      } catch (error) {
      }
    },

    /**
     * new
     */
    __new : function() {
      var form = new prems.FormTaxinomie();
      var win = new prems.MyWindow(this.tr("Modifier taxinomie"));
      win.setLayout(new qx.ui.layout.Grow());
      win.add(form);
      win.open();
      win.center();
      form.addListener("saved", function() {
        this.__update();
        win.close();
      }, this);
    },

    /**
     * update
     */
    __update : function() {
      var service = this.__service.query("getAllTaxinomie");
      service.addListener("changeResponseModel", function() {
        var data = service.getResult();
        this.__model = new qx.ui.table.model.Filtered();
        if (data.length != 0) {
          this.__model.setColumns(Object.keys(data[0]));
        }
        this.__model.setRowsAsMapArray(data);
        this.__table.setTableModel(this.__model);
        this.__table.getTableColumnModel().setColumnVisible(2, false);
        this.__container.addAt(this.__table, 0);
        this.__table.show();
      }, this);
    }
  }
});
