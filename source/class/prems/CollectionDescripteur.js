/**
 * Ajouter modifier des collections.
*/
qx.Class.define("prems.CollectionDescripteur",
{
  extend : qx.ui.core.Widget,
  construct : function() {
    this.base(arguments);
    this.__container = new qx.ui.container.Composite(new qx.ui.layout.VBox(20));
    this._setLayout(new qx.ui.layout.Grow());
    this._add(this.__container);
    this.__service = prems.service.Service.getInstance();
    var form = new qx.ui.form.Form();

    //recherche des valeurs distinctes par liste
    this.__liste = new qx.ui.form.List().set(
    {
      width : 250,
      height : 200,
      minWidth : 50,
      minHeight : 100,
      maxHeight : 400,
      maxWidth : 500
    });
    this.__table = new qx.ui.table.Table();

    //boutons
    this.__buttonEdit = new qx.ui.form.Button(this.tr("Modifier"));
    this.__buttonNew = new qx.ui.form.Button(this.tr("Ajouter"));
    form.add(this.__liste, "", null, "liste");
    this.__liste.exclude();
    form.addButton(this.__buttonNew);
    //suppression du bouton moodifier
    //form.addButton(this.__buttonEdit);
    this.__container.add(new qx.ui.form.renderer.Single(form));
    //this.__liste.addListener("dblclick", this.__edit, this);
    this.__buttonEdit.addListener("execute", this.__edit, this);
    this.__buttonNew.addListener("execute", this.__new, this);
    this.__update();
  },
  members :
  {
    /**
       * Edit
       */
    __edit : function() {
      if (this.__liste.getModelSelection().getLength() > 0) {
        var formCollection = new prems.FormCollection();
        formCollection.setId(this.__liste.getModelSelection().getItem(0));
        var win = new prems.MyWindow(this.tr("Modifier collection"));
        win.setLayout(new qx.ui.layout.Grow());
        win.add(formCollection);
        win.open();
        win.center();
        formCollection.addListener("saved", function() {
          this.__update();
          win.close();
        }, this);
      }
    },

    /**
       * New
       */
    __new : function() {
      var formCollection = new prems.FormCollection();
      var win = new prems.MyWindow(this.tr("Nouvelle collection"));
      win.setLayout(new qx.ui.layout.Grow());
      win.add(formCollection);
      win.open();
      win.center();
      formCollection.addListener("saved", function() {
        this.__update();
        win.close();
      }, this);
    },

    /**
       * Update
       */
    __update : function() {
      var service = this.__service.query("getAllCollection");
      service.addListener("changeResponseModel", function() {
        this.__liste.removeAll();
        var resultats = service.getResult();
        var data = {
          Choice : []
        };
        for (var collection in resultats) {
          data.Choice.push(
          {
            label : resultats[collection]["nom"],
            data : resultats[collection]["id_collection"]
          });
        }
        var model = qx.data.marshal.Json.createModel(data);
        var listController = new qx.data.controller.List(null, this.__liste);
        listController.setDelegate({
          bindItem : function(controller, item, index) {
            controller.bindProperty("label", "label", null, item, index);
            controller.bindProperty("data", "model", null, item, index);
          }
        });
        listController.setModel(model.getChoice());
        this.__liste.show();
      }, this);
    }
  }
});
