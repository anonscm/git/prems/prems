/**
* Tree variete, accession, lot
*/
qx.Class.define("prems.TreeVariete", {
  extend : qx.ui.core.Widget,

  properties : {
    idVariete : {
      init : null,
      nullable : true,
      check : "Integer",
      event : "changeIdVariete"
    },
    cultivated : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeCultivated"
    },
    genus : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeGenus"
    },
    specie : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeSpecie"
    },
    formName : {
      init : "Varieté ou Lignée",
      nullable : true,
      check : "String",
      event : "changeFormName"
    },
    initialFocus : {
      init : null,
      nullable : true,
      event : "changeInitialFocus"
    }
  },

  /**
  *@param idVariete {Integer} id variete
  */
  construct : function() {
    this.base(arguments);
    //Service
    this.__service = prems.service.Service.getInstance();
    this.__servicePlant = prems.ServicePlant.getInstance();
    /* -- Data Management --*/
    this.getFormName("Varieté ou Lignée");

    /*-- UI drawing --*/

    this._setLayout(new qx.ui.layout.Grow());
    var scrollContainer = new qx.ui.container.Scroll();
    this.__containerForm = new qx.ui.container.Composite(new qx.ui.layout.HBox(20));
    //    this.__containerForm.setAllowStretchX(true,true);
    //    this.__containerForm.setAllowStretchY(true,true);
    this._add(scrollContainer);
    scrollContainer.add(this.__containerForm);

    //Loading gif
    var loading = new qx.ui.basic.Image("prems/loading.gif");
    loading.setScale(true);
    loading.setAlignX("center");
    loading.setAlignY("middle");
    loading.setWidth(30);


    //Arbre d'affichage hierarchie
    this.__tree = new qx.ui.treevirtual.TreeVirtual("");
    //this.__tree = new qx.ui.tree.VirtualTree("")
    this.__tree.setAlwaysShowOpenCloseSymbol(true);
    this.__tree.setStatusBarVisible(false);
    this.__tree.setColumnWidth(0, 200);
    this.__tree.setAllowStretchY(true, true);
    this.__containerForm.add(this.__tree, {flex : 0});

    /* -- Listeners --*/

    //affichage variete ou lignée
    this.addListener("changeCultivated", function() {
      if (this.getCultivated() == "annuelle") {
        this.setFormName("Lignée");
      } else {
        this.setFormName("Variété");
      }
    }, this);

    //Met a jour la fenetre lorsque l'id de la variete change
    this.addListener("changeIdVariete", function() {
      this.__update();
    }, this);

    //Changement du formulaire lorsque la selection change
    this.__tree.addListener("changeSelection", function(e) {
      //Niveau variété
      if (e.getData()[0]["level"] == 1) {
        this.__containerForm.removeAll();
        this.__containerForm.add(this.__tree, {flex : 0});
        this.__containerForm.add(new prems.PresentationVariete().set({
          id : this.getIdVariete(),
          cultivated : this.getCultivated()
        }), {flex : 1});
      }

      //Niveau accession
      if (e.getData()[0]["level"] == 2) {
        var idClone = e.getData()[0]["iconSelected"];
        this.__containerForm.removeAll();
        this.__containerForm.add(this.__tree, {flex : 0});
        this.__containerForm.add(new prems.PresentationClone().set({
          id : idClone,
          cultivated : this.getCultivated()
        }), {flex : 1});
      }

      //Niveau Lot
      if (e.getData()[0]["level"] >= 3) {
        var id_lot = e.getData()[0]["iconSelected"];
        this.__containerForm.removeAll();
        this.__containerForm.add(this.__tree, {flex : 0});
        this.__containerForm.add(new prems.PresentationLot().set({
          id : id_lot,
          cultivated : this.getCultivated()
        }), {flex : 1});
      }
    }, this);
    // initial Focus : ou comment placer le focus selon les informations reçues
    this.addListener("changeInitialFocus", function() {
    }, this);
  },
  members : {
    /**
    * Update
    */
    __update : function() {
      this.__tree.getDataModel().prune(0, true);
      var dataModel = this.__tree.getDataModel();
      dataModel.setData();
      var lotSource = [];
      var variete = dataModel.addBranch(null, this.getFormName(), true, true, null, this.getIdVariete());
      var serviceClone = this.__service.query("getCloneByIdVariete", [this.getIdVariete()]);
      serviceClone.addListener("changeResponseModel", function() {
        for (let i in serviceClone.getResult()) {
          var clone;
          if (serviceClone.getResult()[i]["nom"] !== null) {
            clone = dataModel.addBranch(
              variete,
              serviceClone.getResult()[i]["nom"],
              true,
              false,
              null,
              serviceClone.getResult()[i]["id_clone"]
            );
          } else if (serviceClone.getResult()[i]["remarque"] !== null) {
            clone = dataModel.addBranch(
              variete,
              serviceClone.getResult()[i]["remarque"],
              true,
              false,
              null,
              serviceClone.getResult()[i]["id_clone"]
            );
          } else {
            clone = dataModel.addBranch(
              variete,
              "",
              true,
              false,
              null,
              serviceClone.getResult()[i]["id_clone"]
            );
          }
          //var serviceArbre = this.__service.query("getArbreByClone", [serviceClone.getResult()[i]["id_clone"]]);
          var userGroups = [];
          var session = qxelvis.session.service.Session.getInstance();
          var sessionId = session.getSessionId();
          if (session.getUserGroupsInfo() !== null) {
            userGroups = session.getUserGroupsInfo().getReadGroupIds();
          }
          var serviceArbre = this.__servicePlant.query("getAllIdLotsTreeByIdAccession", [sessionId, userGroups, serviceClone.getResult()[i]["id_clone"]]);
          serviceArbre.addListener("changeResponseModel", function() {
            for (let j in serviceArbre.getResult()) {
              if (serviceArbre.getResult()[j]["numero_lot"] !== null) {
                dataModel.addBranch(
                  clone,
                  serviceArbre.getResult()[j]["numero_lot"],
                  true,
                  true,
                  "prems/plant.png",
                  serviceArbre.getResult()[j]["id_lot"]
                );
              } else {
                dataModel.addBranch(clone, null, true, true, null, serviceArbre.getResult()[j]["id_lot"]);
              }
              if (serviceArbre.getResult()[j]["lot_parent"] !== null) {
                lotSource.push({"idLot" : serviceArbre.getResult()[j]["id_lot"], "idLotParent" : serviceArbre.getResult()[j]["lot_parent"]});
              }
            }
            dataModel.setData();
            var serviceGraines = this.__servicePlant.query("getAllIdLotsSeedByIdAccession", [sessionId, userGroups, serviceClone.getResult()[i]["id_clone"]]);
            serviceGraines.addListener("changeResponseModel", function() {
              for (let j in serviceGraines.getResult()) {
                if (serviceGraines.getResult()[j]["numero_lot"] !== null) {
                  dataModel.addBranch(
                    clone,
                    serviceGraines.getResult()[j]["numero_lot"],
                    true,
                    true,
                    "prems/seed.png",
                    serviceGraines.getResult()[j]["id_lot"]
                  );
                } else {
                  dataModel.addBranch(clone, null, true, true, null, serviceGraines.getResult()[j]["id_lot"]);
                }
                if (serviceGraines.getResult()[j]["lot_parent"] !== null) {
                  lotSource.push({"idLot" : serviceGraines.getResult()[j]["id_lot"], "idLotParent" : serviceGraines.getResult()[j]["lot_parent"]});
                }
              }
              dataModel.setData();

              if (lotSource.length > 0) {
                this.__aligneLot(lotSource);
              }
              this.focusOnSelection();
            }, this);
          }, this);
        }
      }, this);
    },
    /*
    * __aligneLot :
    * Permet d'incrémenter les lots dont l'origine est un autre lot
    */
    __aligneLot : function(lotSource) {
      var arbre = this.__tree.getDataModel().getData();
      for (let l in lotSource) {
        var parentNodeId = null;
        var nodeId = null;
        //var level = null;
        //parcours arbre pour trouver origine = parentNodeId et nodeID
        for (let a in arbre) {
          if (a > 0) {
            if (arbre[a]["iconSelected"] == lotSource[l]["idLotParent"]) {
              parentNodeId = arbre[a]["nodeId"];
            }
            if (arbre[a]["iconSelected"] == lotSource[l]["idLot"]) {
              nodeId = arbre[a]["nodeId"];
              //level = arbre[a]["level"] + 1;
            }
          }
        }
        var dm = this.__tree.getDataModel();
        dm.move(nodeId, parentNodeId);
        dm.setData();
      }
    },
    /*
    * Positionne le curseur sur l'objet selectionné
    * Affiche les informations associées
    */
    focusOnSelection  : function() {
      var gif = this.getInitialFocus();
      var arbre = this.__tree.getDataModel().getData();
      //si variété
      if (gif["introduction_name"] === undefined) {
        this.__tree.setFocusedCell(0, 0, true);
        this.__containerForm.removeAll();
        this.__containerForm.add(this.__tree, {flex : 0});
        this.__containerForm.add(new prems.PresentationVariete().set({
          id : this.getIdVariete(),
          cultivated : this.getCultivated()
        }), {flex : 1});
      } else
      //si accession
      if (gif["lot_name"] === undefined) {
        //le focus sur le clone se fait avec le nom.
        //Aucune possiblilité de écuperer id_accession dans la liste des lots
        for (let a in arbre) {
          if (a > 0) {
            if (gif["introduction_name"] == arbre[a]["label"]) {
              let ligne = arbre[a]["nodeId"]-1;
              this.__tree.setFocusedCell(0, ligne, true);
              let idClone = Number(arbre[a]["iconSelected"]);
              this.__containerForm.removeAll();
              this.__containerForm.add(this.__tree, {flex : 0});
              this.__containerForm.add(
                new prems.PresentationClone().set(
                  {id : idClone, cultivated : this.getCultivated() }
                ),
                {flex : 1}
              );
              break;
            }
          }
        }
      } else {
        //si lot
        for (let a in arbre) {
          if (a > 0) {
            if (gif["id"] == arbre[a]["iconSelected"]) {
              let ligne = arbre[a]["nodeId"]-1;
              this.__tree.setFocusedCell(0, ligne, true);
              let id_lot = arbre[a]["iconSelected"];
              this.__containerForm.removeAll();
              this.__containerForm.add(this.__tree, {flex : 0});
              this.__containerForm.add(new prems.PresentationLot().set({
                id : id_lot,
                cultivated : this.getCultivated()
              }), {flex : 1});
            }
          }
        }
      }
    }
  }
});
