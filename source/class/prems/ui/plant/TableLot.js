/* ************************************************************************

  Copyright: 2018 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/LicenceCeCILLV2-en.html
    See the LICENCE file in the project's top-level directory for details.

  Authors:
    * Lysiane Hauguel, bioinfo team, IRHS

************************************************************************ */

/**
 * This is the widget for the table lot.
 * Authors : Lysiane Hauguel,
 * May 2019.
 * @asset(glams/*)
 */
qx.Class.define("prems.ui.plant.TableLot", {
  extend: qx.ui.core.Widget,

  events: {
    /**
     * Fired to display information on a plant
     */
    "displayInformations" : "qx.event.type.Data"
  },

  properties: {
    geneticBackground : {
      init : null,
      nullable : true,
      event : "changeGeneticBackground"
    },

    varietyLot : {
      init : null,
      nullable : true,
      event : "changeVarietyLot"
    },

    listeRecherche : {
      init : null,
      nullable : true,
      check : "Array",
      event : "changeListeRecherche"
    },

    resultTable : {
      init : null,
      nullable : true,
      check : "Array",
      event : "changeResultTable"
    }
  },

  construct: function() {
    this.base(arguments);

    //Service
    this.serviceLot = prems.service.plant.ServiceLot.getInstance();

    //Layouts
    var container = new qx.ui.container.Composite(new qx.ui.layout.HBox(10));
    this._setLayout(new qx.ui.layout.Grow());
    this._add(container);

    // Widgets
    var tableModel = new qx.ui.table.model.Filtered();
    tableModel.setColumnIds(
      ["lignee", "concept", "lot", "date", "id", "id_accession"]
      );
    tableModel.setColumnNamesByIndex(
      ["Lignée", "origine", "Lot", "Date récolte", "id", "id_accession"]
      );
    this.tableResult = new qx.ui.table.Table(tableModel);
    this.tableResult.setMaxHeight(300);
    this.tableResult.setAllowStretchX(true);
    this.tableResult.setAllowStretchY(true);
    var tcm = this.tableResult.getTableColumnModel();
    var tm = this.tableResult.getTableModel();
    for (let i = 0; i < tm.getColumnCount(); i++) {
      if (tm.getColumnName(i) == "id") {
        tcm.setColumnVisible(i, false);
      } else if (tm.getColumnName(i) == "id_accession") {
        tcm.setColumnVisible(i, false);
      } else {
        tcm.setColumnVisible(i, true);
      }
    }
    container.add(this.tableResult, {flex : 1});

    //Loading gif
    this.__loading = new qx.ui.basic.Image("prems/loading.gif");
    this.__loading.setAlignX("center");

    // LISTENERS

    this.tableResult.addListener("cellTap", function(e) {
      this.tableLine = this.tableResult.getTableModel().getRowDataAsMap(e.getRow());
    }, this);

    this.serviceLot.addListener("gotLotIdsForGeneticBackgroundIds", function(e) {
      var loadData = e.getData();
      this.getPlantByLotIds(loadData);
    }, this);

    this.addListener("changeGeneticBackground", function(e) {
      var loadData = e.getData().getId();
      var geneticId = [loadData];
      var taxonomyId = 453;
      this.tableResult.getTableModel().removeRows(0, this.tableResult.getTableModel().getRowCount());
      this.serviceLot.getLotIdsForGeneticBackgroundIds(geneticId, taxonomyId);
    }, this);

    this.addListener("changeListeRecherche", function(e) {
      var loadData = e.getData();
      if (loadData !== null) {
        this.searchExecute(loadData);
      }
    }, this);

    this.addListener("changeVarietyLot", function(e) {
      var loadData = e.getData();
      var listLine = [];
      this.tableResult.setMaxHeight(60);
      if (loadData !== null) {
        listLine.push({
          "lot": loadData["lot"],
          "id": loadData["id"],
          "lignee": loadData["lignee"],
          "date": loadData["date"],
          "concept": loadData["concept"],
          "id_accession": loadData["id_accession"]
        });
        this.__updateTableLot(listLine);
      }
    });

    this.serviceLot.addListener("gotVarieteByLot", function(e) {
      var info = e.getData()[0];
      info["ligneTable"] = this.ligneTable;
      this.fireDataEvent("displayInformations", info);
    }, this);

    this.serviceLot.addListener("gotNamesForIdLot", function(e) {
      var r = e.getData();
      var allData = [];
      var listIdLot = [];
      // var listeNewKeys = ['id','lot_name','introduction_name','variety_name'];
      for (let l in r) {
        if (r[l]["introduction_name"] !== null) {
          allData.push({
            "lot":r[l]["lot_name"],
            "lignee":r[l]["variety_name"],
            "date":r[l]["date_recolte"],
            "concept":r[l]["introduction_name"],
            "id":r[l]["id"].toString(),
            "id_accession":r[l]["id_accession"].toString()
          });
        } else {
          allData.push({
            "lot":r[l]["lot_name"],
            "lignee":r[l]["variety_name"],
            "date":r[l]["date_recolte"],
            "concept":r[l]["remarque"],
            "id":r[l]["id"].toString(),
            "id_accession":r[l]["id_accession"].toString()
          });
        }
        listIdLot.push(r[l]["id"]);
      }
      this.__updateTableLot(allData);
    }, this);

    this.serviceLot.addListener("gotNamesLotsByNamesOrNotationsOrPlaces", function(e) {
      var r = e.getData();
      var allData = [];
      var listIdLot = [];
      // var listeNewKeys = ['id','lot_name','introduction_name','variety_name'];
      for (let l in r) {
        if (r[l]["introduction_name"] !== null) {
          allData.push({
            "lot": r[l]["lot_name"],
            "lignee": r[l]["variety_name"],
            "date": r[l]["date_recolte"],
            "concept": r[l]["introduction_name"],
            "id": r[l]["id"].toString(),
            "id_accession": r[l]["id_accession"].toString()
          });
        } else {
          allData.push({
            "lot": r[l]["lot_name"],
            "lignee": r[l]["variety_name"],
            "date": r[l]["date_recolte"],
            "concept": r[l]["remarque"],
            "id": r[l]["id"].toString(),
            "id_accession": r[l]["id_accession"].toString()
          });
        }
        listIdLot.push(r[l]["id"]);
      }
      this.__updateTableLot(allData);
    }, this);
  },

  members: {
    /**
     * update table lot
     *@param result
     */
    __updateTableLot : function(result) {
      this.setResultTable(result);
      var tableModel = this.tableResult.getTableModel();
      tableModel.setDataAsMapArray(result);
      this.tableResult.addListener("cellDbltap", function(e) {
        //Récupère l'id dans la colonne id du tableau
        var id = Number(
          this.tableResult.getTableModel().getRowDataAsMap(e.getRow())["id"]
          );
        this.ligneTable = this.tableResult.getTableModel().getRowDataAsMap(
          e.getRow()
        );
        this.serviceLot.getVarieteByLot(id);
      }, this);
    },
    /**
     * get plant by Lot Ids
     *@param genetic {array} array of lot ids
     */
    getPlantByLotIds : function(genetic) {
      // TODO
      this.serviceLot.getNamesForIdLot(genetic);
    },
    /**
     * instructions after change the property listeRecherche
     *@param listCriteres {list} list of criteres for the research
     */
    searchExecute : function(listCriteres) {
      //On affiche loading gif
      this.__loading.show();

      //3 listes des critères de recherche notations nom et place
      var listeNames = [];
      var listeNotation = [];
      var listePlaces = [];
      for (let s in listCriteres) {
        //recherche sur name_lot
        if (listCriteres[s].getCriterion()["type"] == "name_lot") {
          listeNames.push({
            "name_type": listCriteres[s].getCriterion()["type"],
            "value_name": listCriteres[s].getValue(),
            "operator": listCriteres[s].getOperator()
          });
        }
        //recherche sur name_accession
        if (listCriteres[s].getCriterion()["type"] == "name_accession") {
          listeNames.push({
            "name_type": listCriteres[s].getCriterion()["type"],
            "value_name": listCriteres[s].getValue(),
            "operator": listCriteres[s].getOperator()
          });
        }
        //recherche sur name_variete
        if (listCriteres[s].getCriterion()["type"] == "name_variety") {
          listeNames.push({
            "name_type": listCriteres[s].getCriterion()["type"],
            "value_name": listCriteres[s].getValue(),
            "operator": listCriteres[s].getOperator()
          });
        }
        //recherche avec type_notation
        if (listCriteres[s].getCriterion()["type"] == "notation_type") {
          listeNotation.push({
            "id_notation_type": listCriteres[s].getCriterion()["id"],
            "value_notation": listCriteres[s].getValue(),
            "operator": listCriteres[s].getOperator()
          });
        }
        //recherche sur place
        if (listCriteres[s].getCriterion()["type"] == "place_type") {
          listePlaces.push({
            "id_place_type": listCriteres[s].getCriterion()["id"],
            "value_place": listCriteres[s].getValue(),
            "operator": listCriteres[s].getOperator()
          });
        }
      }
      //effectue la recherche des plantes avec ces critères
      this.getNamesPlants(listeNames, listeNotation, listePlaces);
    },
    /**
     * Get Names Plants
     *
     *@param listeNames {list} list of names
     *@param listeNotations {list} list of notations
     *@param listePlaces {list} list of places
     */
    getNamesPlants : function(listeNames, listeNotations, listePlaces) {
     //charge les usergroups et idsession
      var lnot = [];
      var lnam = [];
      var lplace = [];
      var lnamIn = [];
      var lnotIn = [];
      var lplaceIn = [];
      var lnamNear = [];
      var lnotAbs = [];
      for (let n in listeNotations) {
        if (listeNotations[n]["operator"] == "=") {
          lnot.push({
            "id_notation_type": listeNotations[n]["id_notation_type"],
            "value_notation": listeNotations[n]["value_notation"]
          });
        }
        if (listeNotations[n]["operator"] == "in") {
          lnotIn.push({
            "id_notation_type": listeNotations[n]["id_notation_type"],
            "value_notation": listeNotations[n]["value_notation"]
          });
        }
        if (listeNotations[n]["operator"] == "abs") {
          lnotAbs.push({
            "id_notation_type": listeNotations[n]["id_notation_type"]
          });
        }
      }
      for (let n in listeNames) {
        if (listeNames[n]["operator"] == "=") {
          lnam.push({
            "name_type": listeNames[n]["name_type"],
            "value_name": listeNames[n]["value_name"]
          });
        }
        if (listeNames[n]["operator"] == "in") {
          lnamIn.push({
            "name_type": listeNames[n]["name_type"],
            "value_name": listeNames[n]["value_name"]
          });
        }
        if (listeNames[n]["operator"] == "near") {
          lnamNear.push({
            "name_type": listeNames[n]["name_type"],
            "value_name": listeNames[n]["value_name"]
          });
        }
      }
      for (let n in listePlaces) {
        if (listePlaces[n]["operator"] == "=") {
          lplace.push({
            "id_place_type": listePlaces[n]["id_place_type"],
            "value_place": listePlaces[n]["value_place"]
          });
        }
        if (listePlaces[n]["operator"] == "in") {
          lplaceIn.push({
            "id_place_type": listePlaces[n]["id_place_type"],
            "value_place": listePlaces[n]["value_place"]
          });
        }
      }
      //lance la recherche sur les notations noms et places
      this.serviceLot.getNamesLotsByNamesOrNotationsOrPlaces(
        lnam, lnot, lplace, lnamIn, lnotIn, lplaceIn, lnamNear, lnotAbs
      );
    }
  }
});
