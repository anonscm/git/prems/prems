/* ************************************************************************

  Copyright: 2018 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/LicenceCeCILLV2-en.html
    See the LICENCE file in the project's top-level directory for details.

  Authors:
    * Lysiane Hauguel, bioinfo team, IRHS

************************************************************************ */

/**
* This is the main widget to add new taking.
* Authors : Lysiane Hauguel,
* April 2019.
* @asset(glams/*)
*/
qx.Class.define("prems.ui.plant.CreateLot", {
  extend: qx.ui.core.Widget,

  events: {
    /**
     * save as a draft
     */
    "saveDraft": "qx.event.type.Data",
    /**
     * load the draft
     */
    "loadDraft": "qx.event.type.Data"
  },

  properties: {
    lot : {
      init : null,
      nullable : true,
      event : "changeLot"
    },

    listeCritereRecherche : {
      init : null,
      nullable : true,
      check : "Array",
      event : "changeListeCritereRecherche"
    },

    dateRecolte : {
      init : null,
      nullable : true,
      event : "changeDateRecolte"
    },

    multiplicationDate : {
      init : null,
      nullable : true,
      event : "changeMultiplicationDate"
    },

    creationDate : {
      init : new Date(),
      nullable : true,
      event : "changeCreationDate"
    },

    lotSourceId : {
      init : null,
      nullable : true,
      event : "changeLotSourceId"
    },

    accessionId : {
      init : null,
      nullable : true,
      event : "changeAccessionId"
    },

    introductionDate : {
      init : null,
      nullable : true,
      event : "changeIntroductionDate"
    }
  },

  construct: function(lot) {
    this.base(arguments);

    /*
    **************************************************************************
     INIT
    **************************************************************************
    */
    // Session management
    this.serviceLot = prems.service.plant.ServiceLot.getInstance();
    this.serviceNotation = prems.service.notation.ServiceNotation.getInstance();
    this.setLot(lot);

    //Containers
    var scrollContainer = new qx.ui.container.Scroll();
    this._add(scrollContainer);
    this.layout = new qx.ui.container.Composite(new qx.ui.layout.VBox(5));
    this._setLayout(new qx.ui.layout.Grow());
    scrollContainer.add(this.layout);

    // Initialisation des objets
    this.objVariety = new prems.data.plant.Variety();
    this.objAccession = new prems.data.plant.Accession();
    this.objLot = new prems.data.plant.Lot();

    // Initialisation des widgets et Hboxs
    // Premier bloc Organisme
    this.gpeOrga = new qx.ui.groupbox.GroupBox(this.tr("Organisme"));
    this.gpeOrga.setLayout(new qx.ui.layout.VBox(10));
    this.gpeOrga.setMaxWidth(700);
    var HboxOrga = new qx.ui.container.Composite(new qx.ui.layout.HBox(50));
    this.fcbGenre = new qxelvis.ui.FilteredComboBox();
    this.fcbEspece = new qxelvis.ui.FilteredComboBox();
    // ajout des widgets dans l'interface
    HboxOrga._add(new qx.ui.basic.Label().set({
      value: "Genre : ",
      rich: true
    }));
    HboxOrga._add(this.fcbGenre);
    HboxOrga._add(new qx.ui.basic.Label().set({
      value: "Espèce : ",
      rich: true
    }));
    HboxOrga._add(this.fcbEspece);
    this.gpeOrga.add(HboxOrga);


    // Deuxième bloc genetic Background
    this.gpeLigne = new qx.ui.groupbox.GroupBox();
    this.gpeLigne.setLayout(new qx.ui.layout.VBox(10));
    this.gpeLigne.setMaxWidth(700);
    var HboxLigne = new qx.ui.container.Composite(new qx.ui.layout.HBox(50));
    var geneticBackValidator = new qx.ui.form.validation.AsyncValidator(
      function(validator, value) {
        if (value === null || value.length == 0 || value.trim() == "") {
          validator.setValid(false, "Please select a genetic background");
        } else {
          validator.setValid(true);
        }
      }
    );
    var validationbtnAccess = new qx.ui.form.validation.Manager();
    this.fcbGenetique = new qxelvis.ui.FilteredComboBox();
    var btnLigne = new qx.ui.form.Button("Nouvelle lignée sauvage");
    HboxLigne._add(new qx.ui.basic.Label().set({
      value: "Fond génétique<b style='color:red'>*</b> : ",
      rich: true
    }));
    this.HboxSearch = new qx.ui.container.Composite(
      new qx.ui.layout.HBox(10)
    );
    this.btnSearch = new qx.ui.form.Button("Search");
    // ajout des widgets dans l'interface
    validationbtnAccess.add(this.fcbGenetique, geneticBackValidator);
    HboxLigne._add(this.fcbGenetique);
    HboxLigne._add(btnLigne);
    this.gpeLigne.add(HboxLigne);
    this.gpeLigne.add(this.HboxSearch);


    // Bloc nom nouvelle lignée sauvage
    this.gpeNameLigne = new qx.ui.groupbox.GroupBox(
      this.tr("Nom nouvelle lignée")
    );
    this.gpeNameLigne.setLayout(new qx.ui.layout.VBox(5));
    this.gpeNameLigne.setMaxWidth(700);
    this.gpeNameLigne.setVisibility("excluded");
    this.nameUsuelList = [];
    var dateFormat = new qx.util.format.DateFormat("dd/MM/yyyy");
    this.btnAddNameUsuel = new qx.ui.form.Button("+");
    this.btnAddNameUsuel.setAllowStretchX(false);
    this.btnAddNameUsuel.setAlignX("right");
    this.nameVarietyContainer = new qx.ui.container.Composite(
      new qx.ui.layout.VBox(20)
    );
    this.addNameWidget();
    this.gpeNameLigne.add(this.nameVarietyContainer);
    this.gpeNameLigne.add(this.btnAddNameUsuel);


    // Bloc Lignée accessions lot
    this.gpeAccessions = new qx.ui.groupbox.GroupBox(
      this.tr("Lignées, accessions et lots parents")
    );
    this.gpeAccessions.setLayout(new qx.ui.layout.VBox(10));
    this.gpeAccessions.setMaxWidth(700);
    this.gpeAccessions.setVisibility("excluded");
    var HboxAllLots = new qx.ui.container.Composite(
      new qx.ui.layout.HBox(250)
    );
    // Box et container
    var VboxBtnLot = new qx.ui.container.Composite(new qx.ui.layout.VBox(10));
    var HboxTable = new qx.ui.container.Composite(new qx.ui.layout.HBox());
    var containerInfo = new qx.ui.container.Composite(new qx.ui.layout.VBox());
    var window = new qx.ui.window.Window("Informations");
    var layoutVBox = new qx.ui.layout.VBox();
    var HboxLot = new qx.ui.container.Composite(new qx.ui.layout.HBox(30));
    // widgets
    var checkBoxAll = new qx.ui.form.CheckBox(
      "Toutes les accessions et lots de l'unité"
    );
    var btnMutant = new qx.ui.form.Button("Nouvelle lignée mutante");
    this.tableLot = new prems.ui.plant.TableLot();
    //liste des items de la virtualCombobox des criteres
    this.__listItemCriteres = [];
    this.searchWidgetList = [];
    var checkBoxLot = new qx.ui.form.CheckBox(
      "Nouveau lot à partir d'une accession mais sans lot parent"
    );
    var fcbLotParent = new qxelvis.ui.FilteredComboBox();
    var btnAccession = new qx.ui.form.Button("Nouvelle accession");
    btnAccession.setAllowStretchX(false);
    var btnDescription = new qx.ui.form.Button(
      "Description d'un nouveau lot"
    );
    btnDescription.setAllowStretchX(false);
    // ajout des widgets dans l'interface
    HboxAllLots._add(checkBoxAll);
    HboxAllLots._add(VboxBtnLot);
    VboxBtnLot._add(btnMutant);
    HboxTable._add(this.tableLot);
    HboxLot._add(checkBoxLot);
    HboxLot._add(fcbLotParent);
    this.gpeAccessions.add(HboxAllLots);
    this.gpeAccessions.add(HboxTable);
    this.gpeAccessions.add(HboxLot);
    this.gpeAccessions.add(btnAccession);
    this.gpeAccessions.add(btnDescription);


    // Bloc ligne de tableau
    this.gpeTable = new qx.ui.groupbox.GroupBox(
      this.tr("Lignées, accessions et lots parents")
    );
    this.gpeTable.setLayout(new qx.ui.layout.VBox(10));
    this.gpeTable.setMaxWidth(700);
    this.gpeTable.setVisibility("excluded");
    var HboxTableLine = new qx.ui.container.Composite(new qx.ui.layout.HBox());
    this.tableLotLine = new prems.ui.plant.TableLot();
    // ajout des widgets dans l'interface
    HboxTableLine._add(this.tableLotLine);
    this.gpeTable.add(HboxTableLine);


    // Bloc nom nouvelle lignée mutante
    this.gpeNameMutant = new qx.ui.groupbox.GroupBox(
      this.tr("Nom nouvelle lignée mutante")
    );
    this.gpeNameMutant.setLayout(new qx.ui.layout.VBox(5));
    this.gpeNameMutant.setMaxWidth(700);
    this.gpeNameMutant.setVisibility("excluded");
    this.nameVarietyMutantContainer = new qx.ui.container.Composite(
      new qx.ui.layout.VBox(20)
    );
    this.btnAddNameMutant = new qx.ui.form.Button("+");
    this.btnAddNameMutant.setAllowStretchX(false);
    this.btnAddNameUsuel.setAlignX("right");
    this.nameMutantList = [];
    this.addNameMutantWidget();
    this.gpeNameMutant.add(this.nameVarietyMutantContainer);
    this.gpeNameMutant.add(this.btnAddNameMutant);


    // Bloc Lot
    if (this.getLot() === "graine") {
      this.gpeDescription = new qx.ui.groupbox.GroupBox(
        this.tr("Description du nouveau lot de graine")
      );
    } else {
      this.gpeDescription = new qx.ui.groupbox.GroupBox(
        this.tr("Description du nouveau lot d'arbre")
      );
    }
    let layout = new qx.ui.layout.Grid().setSpacing(10);
    layout.setColumnFlex(0, 1);
    layout.setColumnFlex(1, 1);
    this.gpeDescription.setLayout(layout);
    this.gpeDescription.setMaxWidth(700);
    this.gpeDescription.setVisibility("excluded");
    this.dateSemi = new qx.ui.form.DateField();
    this.ftNewLot = new qx.ui.form.TextField();
    this.dateRecolte = new qx.ui.form.DateField();
    this.dateRecolte.setValue(new Date());
    this.dateRecolte.setDateFormat(dateFormat);
    this.dateSemi.setDateFormat(dateFormat);
    var fcbQuantite = new qxelvis.ui.FilteredComboBox();
    var nameLotValidator = new qx.ui.form.validation.AsyncValidator(
      function(validator, value) {
        if (value === null || value.length == 0 || value.trim() == "") {
          validator.setValid(false, "Please enter a name");
        } else {
          validator.setValid(true);
        }
      }
    );
    var validationLot = new qx.ui.form.validation.Manager();
    // ajout des widgets dans l'interface
    validationLot.add(this.ftNewLot, nameLotValidator);
    this.gpeDescription.add(new qx.ui.basic.Label().set({
      value: "Date de semi :",
      rich: true
    }), {
      row: 0,
      column: 0
    });
    this.gpeDescription.add(this.dateSemi, {
      row: 0,
      column : 1
    });
    this.gpeDescription.add(new qx.ui.basic.Label().set({
      value: "Nom du nouveau lot<b style='color:red'>*</b> :",
      rich: true
    }), {
      row: 1,
      column: 0
    });
    this.gpeDescription.add(this.ftNewLot, {
      row: 1,
      column : 1
    });
    this.gpeDescription.add(new qx.ui.basic.Label().set({
      value: "Date de récolte :",
      rich: true
    }), {
      row: 2,
      column: 0
    });
    this.gpeDescription.add(this.dateRecolte, {
      row: 2,
      column : 1
    });
    this.gpeDescription.add(new qx.ui.basic.Label().set({
      value: "Quantité",
      rich: true
    }), {
      row: 3,
      column: 0
    });
    this.gpeDescription.add(fcbQuantite, {
      row: 3,
      column : 1
    });


    // Bloc nouvelle accession
    this.gpeDescAccess = new qx.ui.groupbox.GroupBox(
      this.tr("Description d'une nouvelle accession")
    );
    let layout2 = new qx.ui.layout.Grid().setSpacing(10);
    layout2.setColumnFlex(0, 1);
    layout2.setColumnFlex(1, 1);
    this.gpeDescAccess.setLayout(layout2);
    this.gpeDescAccess.setMaxWidth(700);
    this.gpeDescAccess.setVisibility("excluded");
    this.ftAccession = new qx.ui.form.TextField();
    this.dateIntro = new qx.ui.form.DateField();
    this.dateIntro.setValue(new Date());
    this.dateIntro.setDateFormat(dateFormat);
    this.ftDescription = new qx.ui.form.TextField();
    var lbMore = new qx.ui.basic.Label("More");
    var nameAccessValidator = new qx.ui.form.validation.AsyncValidator(
      function(validator, value) {
        if (value === null || value.length == 0 || value.trim() == "") {
          validator.setValid(false, "Please enter a name");
        } else {
          validator.setValid(true);
        }
      }
    );
    var validationAccess = new qx.ui.form.validation.Manager();
    // ajout des widgets dans l'interface
    validationAccess.add(this.ftAccession, nameAccessValidator);
    this.gpeDescAccess.add(new qx.ui.basic.Label().set({
      value: "Nom de l'accession<b style='color:red'>*</b>",
      rich: true }), {
      row: 0,
      column: 0
    });
    this.gpeDescAccess.add(this.ftAccession, {
      row: 0,
      column : 1
    });
    this.gpeDescAccess.add(new qx.ui.basic.Label().set({
      value: "Date d'introduction<b style='color:red'>*</b>",
      rich: true }), {
      row: 1,
      column: 0
    });
    this.gpeDescAccess.add(this.dateIntro, {
      row: 1,
      column : 1
    });
    this.gpeDescAccess.add(new qx.ui.basic.Label().set({
      value: "Description",
      rich: true }), {
      row: 2,
      column: 0
    });
    this.gpeDescAccess.add(this.ftDescription, {
      row: 2,
      column : 1
    });
    this.gpeDescAccess.add(lbMore, {
      row: 2,
      column : 2
    });


    // Cinquième bloc Widget Notation
    this.gpeNotation = new qx.ui.groupbox.GroupBox(
      this.tr("Informations complémentaires")
    );
    this.gpeNotation.setLayout(new qx.ui.layout.VBox(10));
    this.gpeNotation.setMaxWidth(700);
    this.gpeNotation.setVisibility("excluded");
    this.notationContainer = new qx.ui.container.Composite(
      new qx.ui.layout.VBox(20)
    );
    this.notationList = [];
    this.btnAdd = new qx.ui.form.Button("+");
    this.btnAdd.setAllowStretchX(false);
    this.btnAdd.setAlignX("right");
    // ajout des widgets dans l'interface
    this.addNotationWidget();
    this.gpeNotation.add(this.notationContainer);
    this.gpeNotation.add(this.btnAdd);


    // Button d'enregistrement
    this.HboxButton = new qx.ui.container.Composite(
      new qx.ui.layout.HBox(20)
    );
    this.cbSaveOtherVar = new qx.ui.form.CheckBox(
      "Enregistrement une nouvelle variété"
    );
    this.cbSaveOtherVar.setVisibility("excluded");
    this.cbSaveOtherAccess = new qx.ui.form.CheckBox(
      "Enregistrement une nouvelle accession"
    );
    this.cbSaveOtherAccess.setVisibility("excluded");
    this.cbSaveOtherUser = new qx.ui.form.CheckBox(
      "Enregistrement pour une autre personne"
    );
    this.cbSaveOtherUser.setVisibility("excluded");
    var btnSave = new qx.ui.form.Button("Enregistrer");
    btnSave.setEnabled(false);
    var btnDraft = new qx.ui.form.Button("Enregistrer comme brouillon");
    var btnCancel = new qx.ui.form.Button("Annuler");
    this.btnSaveAddLot = new qx.ui.form.Button(
      "Enregistrer et créer un nouveau lot"
    );
    this.btnSaveAddLot.setVisibility("excluded");
    this.btnSaveAddLot.setAllowStretchX(false);
    this.btnSaveAddAccess = new qx.ui.form.Button(
      "Enregistrer et créer une accession"
    );
    this.btnSaveAddAccess.setVisibility("excluded");
    this.btnSaveAddAccess.setAllowStretchX(false);
    this.btnSaveMutantAddAccess = new qx.ui.form.Button(
      "Enregistrer et créer une accession"
    );
    this.btnSaveMutantAddAccess.setVisibility("excluded");
    this.btnSaveMutantAddAccess.setAllowStretchX(false);
    // ajout des widgets dans l'interface
    this.HboxButton._add(btnSave);
    this.HboxButton._add(btnDraft);
    this.HboxButton._add(btnCancel);

    // Binding
    this.bindingVariety(this.objVariety);
    this.bindingAccession(this.objAccession);
    this.bindingLot(this.objLot);

    // LISTENNERS

    // affiche les fonds génétiques
    this.serviceLot.addListener("gotGeneticBackgroundList", function(e) {
      var result = e.getData();
      var listBackground = [];
      for (let n in result) {
        listBackground.push({
        "label": result[n]["name"],
        "id": result[n]["id"]
        });
      }
      this.fcbGenetique.setListModel(listBackground);
    }, this);
    this.serviceLot.getGeneticBackgroundList(453);

    // Affiche tableau avec les lots du fond génétique choisi
    this.fcbGenetique.addListener("changeSelection", function(e) {
      var loadData = e.getData();
      if (loadData === null) {
        this.gpeAccessions.setVisibility("excluded");
      } else {
        this.gpeAccessions.setVisibility("visible");
        this.gpeTable.setVisibility("excluded");
        this.gpeNameMutant.setVisibility("excluded");
        this.cbSaveOtherVar.setVisibility("excluded");
        this.btnSaveAddAccess.setVisibility("excluded");
        this.btnSaveMutantAddAccess.setVisibility("excluded");
        this.gpeDescAccess.setVisibility("excluded");
        this.btnSaveAddLot.setVisibility("excluded");
        this.cbSaveOtherAccess.setVisibility("excluded");
        this.gpeNameLigne.setVisibility("excluded");
        this.gpeDescription.setVisibility("excluded");
        this.cbSaveOtherUser.setVisibility("excluded");
        this.gpeNotation.setVisibility("excluded");
        this.tableLot.setGeneticBackground(loadData);
      }
    }, this);

    // Affiche le bloc nouvelle lignée sauvage
    btnLigne.addListener("execute", function() {
      this.gpeNameLigne.setVisibility("visible");
      this.cbSaveOtherVar.setVisibility("visible");
      this.btnSaveAddAccess.setVisibility("visible");
      this.btnSaveMutantAddAccess.setVisibility("excluded");
      this.gpeTable.setVisibility("excluded");
      this.gpeAccessions.setVisibility("excluded");
      this.gpeNameMutant.setVisibility("excluded");
      this.gpeDescription.setVisibility("excluded");
      this.cbSaveOtherUser.setVisibility("excluded");
      this.gpeDescAccess.setVisibility("excluded");
      this.cbSaveOtherAccess.setVisibility("excluded");
      this.gpeNotation.setVisibility("visible");
      this.btnSaveAddLot.setVisibility("excluded");
    }, this);

    // Affiche le bloc nouvelle lignée mutante
    btnMutant.addListener("execute", function() {
      this.gpeNameMutant.setVisibility("visible");
      this.cbSaveOtherVar.setVisibility("visible");
      this.btnSaveMutantAddAccess.setVisibility("visible");
      this.btnSaveAddAccess.setVisibility("excluded");
      this.gpeDescAccess.setVisibility("excluded");
      this.cbSaveOtherAccess.setVisibility("excluded");
      this.gpeAccessions.setVisibility("excluded");
      this.gpeDescription.setVisibility("excluded");
      this.cbSaveOtherUser.setVisibility("excluded");
      this.gpeNameLigne.setVisibility("excluded");
      this.btnSaveAddLot.setVisibility("excluded");
      this.gpeNotation.setVisibility("visible");
    }, this);

    // Enregistre Variété sauvage et affiche bloc nouvelle accession
    this.btnSaveAddAccess.addListener("execute", function() {
      this.saveVar();
      this.tableLotLine.resetVarietyLot();
      var table = [];
      for (let i in this.nameUsuelList) {
        if (this.nameUsuelList[i].getType() === "nom usuel") {
          table["lignee"] = this.nameUsuelList[i].getValue();
        }
      }
      table["concept"] = null;
      table["lot"] = null;
      table["id"] = null;
      table["date"] = null;
      table["id_accession"] = null;
      this.tableLotLine.setVarietyLot(table);
      this.gpeTable.setVisibility("visible");
    }, this);

    // Enregistre Variété mutante et affiche bloc nouvelle accession
    this.btnSaveMutantAddAccess.addListener("execute", function() {
      this.gpeDescAccess.setVisibility("visible");
      this.cbSaveOtherAccess.setVisibility("visible");
      this.gpeNameMutant.setVisibility("excluded");
      this.cbSaveOtherVar.setVisibility("excluded");
      this.gpeNameLigne.setVisibility("excluded");
      this.gpeTable.setVisibility("excluded");
      this.btnSaveAddAccess.setVisibility("excluded");
      this.btnSaveMutantAddAccess.setVisibility("excluded");
      this.gpeDescription.setVisibility("excluded");
      this.cbSaveOtherUser.setVisibility("excluded");
      this.btnSaveAddLot.setVisibility("visible");
      this.gpeNotation.setVisibility("excluded");
      this.bindingVarietyMutant(this.objVariety);
      // console.log(this.objVariety);
    }, this);

    // Affiche le bloc nouvelle accession
    btnAccession.addListener("execute", function() {
      btnAccession.setEnabled(false);
      validationbtnAccess.validate();
    }, this);

    // Vérifie les valeurs obligatoires pour l'accession
    validationbtnAccess.addListener("complete", function() {
      btnAccession.setEnabled(true);
      if (validationbtnAccess.getValid()) {
        this.btnAccess();
        this.tableLotLine.resetVarietyLot();
        this.tableLot.tableLine["concept"] = null;
        this.tableLot.tableLine["id_accession"] = null;
        this.tableLot.tableLine["lot"] = null;
        this.tableLot.tableLine["id"] = null;
        this.tableLot.tableLine["date"] = null;
        this.tableLotLine.setVarietyLot(this.tableLot.tableLine);
        this.gpeTable.setVisibility("visible");
      }
    }, this);

    // Enregistre Accessopn et affiche bloc nouveau lot
    this.btnSaveAddLot.addListener("execute", function() {
      this.btnSaveAddLot.setEnabled(false);
      validationAccess.validate();
    }, this);

    // Vérifie les valeurs obligatoires pour enregistrer l'accession
    validationAccess.addListener("complete", function() {
      this.btnSaveAddLot.setEnabled(true);
      if (validationAccess.getValid()) {
        this.saveAccess();
        let table = this.tableLotLine.getVarietyLot();
        this.tableLotLine.resetVarietyLot();
        table["concept"] = this.objAccession.getIntroductionName();
        table["lot"] = null;
        table["id"] = null;
        table["date"] = null;
        table["id_accession"] = null;
        this.tableLotLine.setVarietyLot(table);
      }
    }, this);

    // Affiche tableau avec la ligne de recherche
    this.btnSearch.addListener("execute", function() {
      this.gpeAccessions.setVisibility("visible");
      this.gpeTable.setVisibility("excluded");
      this.gpeNameMutant.setVisibility("excluded");
      this.gpeDescAccess.setVisibility("excluded");
      this.btnSaveAddLot.setVisibility("excluded");
      this.cbSaveOtherAccess.setVisibility("excluded");
      this.cbSaveOtherVar.setVisibility("excluded");
      this.btnSaveAddAccess.setVisibility("excluded");
      this.btnSaveMutantAddAccess.setVisibility("excluded");
      this.gpeNameLigne.setVisibility("excluded");
      this.gpeDescription.setVisibility("excluded");
      this.cbSaveOtherUser.setVisibility("excluded");
      this.gpeNotation.setVisibility("excluded");
      this.tableLot.resetListeRecherche();
      this.tableLot.setListeRecherche(this.searchWidgetList);
    }, this);

    // Affiche bloc description d'un nouveau lot
    btnDescription.addListener("execute", function() {
      this.gpeLigne.setEnabled(false);
      if (checkBoxLot.getValue() == false) {
        this.ftNewLot.setValue(this.tableLot.tableLine["lot"]);
        this.setLotSourceId(this.tableLot.tableLine["id"]);
        this.setAccessionId(this.tableLot.tableLine["id_accession"]);
        this.tableLotLine.setVarietyLot(this.tableLot.tableLine);
      }
      this.gpeTable.setVisibility("visible");
      this.gpeNameLigne.setVisibility("excluded");
      this.gpeNameMutant.setVisibility("excluded");
      this.cbSaveOtherVar.setVisibility("excluded");
      this.gpeDescAccess.setVisibility("excluded");
      this.cbSaveOtherAccess.setVisibility("excluded");
      this.gpeAccessions.setVisibility("excluded");
      this.btnSaveAddLot.setVisibility("excluded");
      this.btnSaveAddAccess.setVisibility("excluded");
      this.btnSaveMutantAddAccess.setVisibility("excluded");
      this.gpeDescription.setVisibility("visible");
      this.cbSaveOtherUser.setVisibility("visible");
      this.gpeNotation.setVisibility("visible");
    }, this);

    // Lot à partir d'une accession
    checkBoxLot.addListener("execute", function() {
      if (checkBoxLot.getValue() == true) {
        this.tableLotLine.resetVarietyLot();
        var listAccession = [];
        var table = this.tableLot.getResultTable();
        for (let i in table) {
          if (table[i]["concept"] !== null && table[i]["concept"].trim() != "") {
            listAccession.push({
              "label": table[i]["concept"],
              "id": table[i]["id_accession"],
              "variety": table[i]["lignee"]
            });
          }
        }
        // enlève les doublons de la liste
        var idTmp = null;
        var result = [];
        listAccession.sort();
        for (let i in listAccession) {
          if (listAccession[i]["id"] != idTmp) {
            result.push(listAccession[i]);
            idTmp = listAccession[i]["id"];
          }
        }
        fcbLotParent.setListModel(result);
      }
      if (checkBoxLot.getValue() == false) {
        this.tableLotLine.resetVarietyLot();
        let listEmpty = [];
        fcbLotParent.resetValue();
        fcbLotParent.setListModel(listEmpty);
      }
    }, this);

    // Affiche la liste des accessions présentes dans le tableau
    fcbLotParent.addListener("changeSelection", function(e) {
      this.tableLotLine.resetVarietyLot();
      var loadData = e.getData().getLabel();
      var table = this.tableLot.getResultTable();
      for (let i in table) {
        if (table[i]["concept"] == loadData) {
          table[i]["lot"] = null;
          table[i]["id"] = null;
          table[i]["date"] = null;
          this.tableLotLine.setVarietyLot(table[i]);
        }
      }
    }, this);

    // Annule tout et remet comme à l'origine
    // TODO enlever les valeurs présentes partout
    btnCancel.addListener("execute", function() {
      this.gpeLigne.setEnabled(true);
      this.gpeNameLigne.setVisibility("excluded");
      this.gpeTable.setVisibility("excluded");
      this.gpeAccessions.setVisibility("excluded");
      this.gpeNameMutant.setVisibility("excluded");
      this.cbSaveOtherVar.setVisibility("excluded");
      this.gpeDescription.setVisibility("excluded");
      this.cbSaveOtherUser.setVisibility("excluded");
      this.gpeDescAccess.setVisibility("excluded");
      this.cbSaveOtherAccess.setVisibility("excluded");
      this.gpeNotation.setVisibility("excluded");
      this.btnSaveAddLot.setVisibility("excluded");
      this.btnSaveAddAccess.setVisibility("excluded");
      this.btnSaveMutantAddAccess.setVisibility("excluded");
      this.resetGeneticBackground();
      this.resetWildVariety();
      this.resetMutantVariety();
      this.resetAccession();
      this.resetLot();
      this.resetNotation();
      var store = qx.bom.Storage.getLocal();
      store.removeItem("drafts");
    }, this);

    // Enregistre (test)
    btnDraft.addListener("execute", function() {
      this.saveDraft();
      btnDraft.setEnabled(false);
      validationLot.validate();
    }, this);

    // Valide les champs obligatoires pour l'enregistrement
    validationLot.addListener("complete", function() {
      btnDraft.setEnabled(true);
      if (validationLot.getValid()) {
        this.saveLot();
      }
    }, this);

    this.addListener("saveDraft", function() {
      this.fromDict(this.objLot, this.objAccession, this.objVariety);
      var store = qx.bom.Storage.getLocal();
      store.removeItem("drafts");
    }, this);

    // Ajoute un widget notation
    this.btnAdd.addListener("execute", function() {
      this.addNotationWidget();
    }, this);

    // Ajoute un widget NameVariety pour lignée sauvage
    this.btnAddNameUsuel.addListener("execute", function() {
      this.addNameWidget();
    }, this);

    // Ajoute un widget NameVariety pour mutant
    this.btnAddNameMutant.addListener("execute", function() {
      this.addNameMutantWidget();
    }, this);

    // Affiche la fenêtre info pour une ligne de tableau
    this.tableLot.addListener("displayInformations", function(e) {
      var tree = new prems.TreeVariete();
      tree.setCultivated(e.getData()["cultivated"]);
      tree.setGenus(e.getData()["genus"]);
      tree.setSpecie(e.getData()["specie"]);
      tree.setIdVariete(Number(e.getData()["variete"]));
      tree.setInitialFocus(e.getData()["ligneTable"]);
      containerInfo.removeAll();
      containerInfo.add(tree);
      window.setLayout(layoutVBox);
      window.add(containerInfo);
      window.center();
      window.open();
    }, this);

    this.getAllCriteres();
  },

  members: {
    /**
    * Display the widget.
    */
    __display : function() {
      this.addSearchLine();
      this.layout.add(this.gpeOrga);
      this.layout.add(this.gpeLigne);
      this.layout.add(this.gpeNameLigne);
      this.layout.add(this.gpeTable);
      this.layout.add(this.gpeAccessions);
      this.layout.add(this.gpeNameMutant);
      this.layout.add(this.gpeDescription);
      this.layout.add(this.gpeDescAccess);
      this.layout.add(this.gpeNotation);
      this.layout.add(this.cbSaveOtherVar);
      this.layout.add(this.cbSaveOtherAccess);
      this.layout.add(this.cbSaveOtherUser);
      this.layout.add(this.HboxButton);
      this.layout.add(this.btnSaveAddLot);
      this.layout.add(this.btnSaveAddAccess);
      this.layout.add(this.btnSaveMutantAddAccess);
    },
    /**
     * bindingLot
     *
     * @param obj {prems.data.plant.Lot} object lot.
     */
    bindingLot: function(obj) {
      var dateFormat = new qx.util.format.DateFormat("dd/MM/yyyy");
      this.ftNewLot.bind("value", obj, "name");
      obj.bind("name", this.ftNewLot, "value");
      this.bind("accessionId", obj, "accessionId");
      obj.bind("accessionId", this, "accessionId");
      this.bind("lotSourceId", obj, "lotSourceId");
      obj.bind("lotSourceId", this, "lotSourceId");
      obj.setCreationDate(dateFormat.format(this.getCreationDate()));
      this.dateRecolte.bind("value", this, "dateRecolte");
      this.bind("dateRecolte", this.dateRecolte, "value");
      obj.setHarvestingDate(dateFormat.format(this.getDateRecolte()));
      this.dateSemi.bind("value", this, "multiplicationDate");
      this.bind("multiplicationDate", this.dateSemi, "value");
      obj.setMultiplicationDate(dateFormat.format(this.getMultiplicationDate()));
      // this.tfNotation.bind("value", obj, "type");
      // this.taRemarque.bind("value", obj, "destructionDate");
      // this.taRemarque.bind("value", obj, "quantity");
      // this.taRemarque.bind("value", obj, "place");
    },
    /**
     * bindingAccession
     *
     * @param obj {prems.data.plant.Accession} object accession.
     */
    bindingAccession: function(obj) {
      // var dateFormat = new qx.util.format.DateFormat("dd/MM/yyyy");
      this.ftAccession.bind("value", obj, "introductionName");
      obj.bind("introductionName", this.ftAccession, "value");
      this.ftDescription.bind("value", obj, "comment");
      obj.bind("comment", this.ftDescription, "value");
      // obj.setVarietyId(this.fcbGenetique["__selection"].toArray()[0].getId());
      this.dateIntro.bind("value", this, "introductionDate");
      // this.bind("introductionDate", this.dateIntro, "introductionDate");
      // obj.setIntroductionDate(dateFormat.format(this.getIntroductionDate()));
      // this.tfNotation.bind("value", obj, "type");
      // this.ftNewLot.bind("value", obj, "introductionCloneId");
      // this.ftNewLot.bind("value", obj, "provider");
      // this.ftNewLot.bind("value", obj, "providerId");
      // this.ftNewLot.bind("value", obj, "collectionDate");
      // this.ftNewLot.bind("value", obj, "collectionSiteId");
    },
    /**
     * bindingVariety
     *
     * @param obj {prems.data.plant.Variety} object variety.
     */
    bindingVariety: function(obj) {
      // var dateFormat = new qx.util.format.DateFormat("dd/MM/yyyy");
      // this.ftAccession.bind("value", obj, "breeder");
      // this.ftAccession.bind("value", obj, "breederId");
      // this.ftAccession.bind("value", obj, "comment");
      // this.ftAccession.bind("value", obj, "taxonomyId");
      this.fcbGenre.bind("value", obj, "genus");
      obj.bind("genus", this.fcbGenre, "value");
      this.fcbEspece.bind("value", obj, "specie");
      obj.bind("specie", this.fcbEspece, "value");
      // this.ftAccession.bind("value", obj, "editor");
      // this.ftAccession.bind("value", obj, "editorId");
      obj.setListVarietyName(this.nameUsuelList);
    },
    /**
     * bindingVarietyMutant
     *
     * @param obj {prems.data.plant.Variety} object variety.
     */
    bindingVarietyMutant: function(obj) {
      // var dateFormat = new qx.util.format.DateFormat("dd/MM/yyyy");
      // this.ftAccession.bind("value", obj, "breeder");
      // this.ftAccession.bind("value", obj, "breederId");
      // this.ftAccession.bind("value", obj, "comment");
      // this.ftAccession.bind("value", obj, "taxonomyId");
      this.fcbGenre.bind("value", obj, "genus");
      obj.bind("genus", this.fcbGenre, "value");
      this.fcbEspece.bind("value", obj, "specie");
      obj.bind("specie", this.fcbEspece, "value");
      // this.ftAccession.bind("value", obj, "editor");
      // this.ftAccession.bind("value", obj, "editorId");
      obj.setListVarietyName(this.nameMutantList);
    },
    /**
     * addNameWidget
     *
     * Function which add a widget NameNewVariety and create an object
     * this object is pushed in the nameUsuelList
     */
    addNameWidget: function() {
      var name = new prems.ui.plant.NameNewVariety();
      this.nameVarietyContainer.add(name);
      var objectName = name.binding();
      var objectNameUsuel = null;
      this.nameUsuelList.push(objectName);

      name.addListener("delete", function(e) {
        this.nameVarietyContainer.remove(e.getData());
        name.deleteObjectList(objectName);
        name.deleteObjectList(objectNameUsuel);
      }, this);

      name.addListener("deleteList", function(e) {
        this.nameUsuelList.splice(this.nameUsuelList.indexOf(e.getData()), 1);
      }, this);

      name.cbUsuel.addListener("changeValue", function(e) {
        var loadData = e.getData();
        if (loadData == false) {
          name.deleteObjectList(objectNameUsuel);
        } else {
          objectNameUsuel = name.bindingUsuel();
          this.nameUsuelList.push(objectNameUsuel);
        }
      }, this);
    },
    /**
     * addNameMutantWidget
     *
     * Function which add a widget NameNewVariety and create an object
     * this object is pushed in the nameMutantList
     */
    addNameMutantWidget: function() {
      var nameMutant = new prems.ui.plant.NameNewVariety();
      this.nameVarietyMutantContainer.add(nameMutant);
      var objectNameMutant = nameMutant.binding();
      var objectNameUsuelMutant = null;
      this.nameMutantList.push(objectNameMutant);

      nameMutant.addListener("delete", function(e) {
        this.nameVarietyMutantContainer.remove(e.getData());
        nameMutant.deleteObjectList(objectNameMutant);
        nameMutant.deleteObjectList(objectNameUsuelMutant);
      }, this);

      nameMutant.addListener("deleteList", function(e) {
        this.nameMutantList.splice(this.nameMutantList.indexOf(e.getData()), 1);
      }, this);

      nameMutant.cbUsuel.addListener("changeValue", function(e) {
        var loadData = e.getData();
        if (loadData == false) {
          nameMutant.deleteObjectList(objectNameUsuelMutant);
        } else {
          objectNameUsuelMutant = nameMutant.bindingUsuel();
          this.nameMutantList.push(objectNameUsuelMutant);
        }
      }, this);
    },
    /**
     * addNotationWidget
     *
     * Function which add a widget WidgetNotation and create an object
     * this object is pushed in the notationList
     */
    addNotationWidget: function() {
      var notation = new prems.ui.notation.WidgetNotation();
      this.notationContainer.add(notation);
      var objectNotation = notation.binding();
      this.notationList.push(objectNotation);

      notation.addListener("delete", function(e) {
        this.notationContainer.remove(e.getData());
        notation.deleteObjectList(objectNotation);
      }, this);

      notation.addListener("deleteList", function(e) {
        this.notationList.splice(this.notationList.indexOf(e.getData()), 1);
      }, this);
    },
    /**
     * getAllCriteres
     *
     * Function which add a all criteres in a list which use
     * in addSearchLine function and use on a TableLot widget
     */
    getAllCriteres: function() {
      this.__listItemCriteres = [];
      this.__listItemCriteres.push({
        "label": "Nom lot",
        "type" : "name_lot",
        "id": null
      });
      this.__listItemCriteres.push({
        "label": "Nom concept plantes pérennes",
        "type" : "name_accession",
        "id": null
      });
      this.__listItemCriteres.push({
        "label": "Nom variété ou lignée",
        "type" : "name_variety",
        "id": null
      });
      //Affiche le widget une fois que les informations sont dans la liste
      this.__display();
      //tous les types de notations attachées à un groupe
      this.serviceNotation.getTypeNotationByGroup();
      this.serviceNotation.addListener("gotTypeNotationByGroup", function(e) {
        var result = e.getData();
        for (let n in result) {
          this.__listItemCriteres.push({
            "label": result[n]["name"],
            "type": "notation_type",
            "id": result[n]["id"]
          });
        }
        this.serviceLot.getAllPlaceNamesByGroup();
        this.serviceLot.addListener("gotAllPlaceNamesByGroup", function(e) {
          var result = e.getData();
          for (let n in result) {
            this.__listItemCriteres.push({
              "label": "Lieu : "+result[n]["name"],
              "type": "place_type",
              "id": result[n]["id"]
            });
          }
          this.setListeCritereRecherche(this.__listItemCriteres);
        }, this);
      }, this);
    },
    /**
     * addSearchLine
     *
     * Function which add a widget SearchCriteriaSelectorItem
     * this widget is pushed in the nameUsuelList
     */
    addSearchLine: function() {
      var searchWidget = new prems.ui.SearchCriteriaSelectorItem();
      searchWidget.buttonMoins.setVisibility("excluded");
      searchWidget.setListeCritereRecherche(this.__listItemCriteres);
      this.searchWidgetList.push(searchWidget);
      this.HboxSearch._add(searchWidget);
      this.HboxSearch._add(this.btnSearch);
    },
    /**
     * btnAccess
     *
     * Action on button accession.
     */
    btnAccess: function() {
      this.gpeLigne.setEnabled(false);
      this.gpeTable.setVisibility("visible");
      this.gpeDescAccess.setVisibility("visible");
      this.cbSaveOtherAccess.setVisibility("visible");
      this.gpeNameMutant.setVisibility("excluded");
      this.cbSaveOtherVar.setVisibility("excluded");
      this.gpeAccessions.setVisibility("excluded");
      this.btnSaveAddAccess.setVisibility("excluded");
      this.gpeNameLigne.setVisibility("excluded");
      this.gpeDescription.setVisibility("excluded");
      this.cbSaveOtherUser.setVisibility("excluded");
      this.btnSaveAddLot.setVisibility("visible");
      this.gpeNotation.setVisibility("excluded");
    },
    /**
     * saveVar
     */
    saveVar: function() {
      // console.log(this.objVariety);
      // TODO à modifier pour avoir l'id de la varieté et enregistrer avec
      this.gpeDescAccess.setVisibility("visible");
      this.cbSaveOtherAccess.setVisibility("visible");
      this.gpeNameMutant.setVisibility("excluded");
      this.cbSaveOtherVar.setVisibility("excluded");
      this.gpeNameLigne.setVisibility("excluded");
      this.gpeTable.setVisibility("excluded");
      this.btnSaveAddAccess.setVisibility("excluded");
      this.btnSaveMutantAddAccess.setVisibility("excluded");
      this.gpeDescription.setVisibility("excluded");
      this.cbSaveOtherUser.setVisibility("excluded");
      this.btnSaveAddLot.setVisibility("visible");
      this.gpeNotation.setVisibility("excluded");
    },
    /**
     * saveAccess
     */
    saveAccess: function() {
      // console.log(this.objAccession);
      // TODO à modifier pour avoir l'id de l'accession enregistrer avec
      this.gpeDescription.setVisibility("visible");
      this.cbSaveOtherUser.setVisibility("visible");
      this.gpeNotation.setVisibility("visible");
      this.gpeNameLigne.setVisibility("excluded");
      this.gpeNameMutant.setVisibility("excluded");
      this.cbSaveOtherVar.setVisibility("excluded");
      this.gpeDescAccess.setVisibility("excluded");
      this.cbSaveOtherAccess.setVisibility("excluded");
      this.btnSaveAddLot.setVisibility("excluded");
      this.btnSaveAddAccess.setVisibility("excluded");
    },
    /**
     * saveLot
     */
    saveLot: function() {
      // console.log(this.objLot);
      this.debug("Save lots");
    },
    /**
     * resetGeneticBackground
     */
    resetGeneticBackground: function() {
      this.fcbGenetique.resetValue();
      // la ligne de recherche à reset
    },
    /**
     * resetWildVariety
     */
    resetWildVariety: function() {
      this.nameVarietyContainer.removeAll();
      this.nameUsuelList = [];
      this.addNameWidget();
    },
    /**
     * resetMutantVariety
     */
    resetMutantVariety: function() {
      this.nameVarietyMutantContainer.removeAll();
      this.nameMutantList = [];
      this.addNameMutantWidget();
    },
    /**
     * resetAccession
     */
    resetAccession: function() {
      this.ftAccession.resetValue();
      this.dateIntro.setValue(new Date());
      this.ftDescription.resetValue();
    },
    /**
     * resetLot
     */
    resetLot: function() {
      this.dateSemi.resetValue();
      this.ftNewLot.resetValue();
      this.dateRecolte.set(new Date());
    },
    /**
     * resetNotation
     */
    resetNotation: function() {
      this.notationContainer.removeAll();
      this.notationList = [];
      this.addNotationWidget();
    },
    /**
     * First test save/print data
     */
    saveDraft : function() {
      this.fireDataEvent("saveDraft", this.toDict(this.objLot, this.objAccession, this.objVariety));
    },

    loadDraft : function() {
      this.fireDataEvent("loadDraft", this.fromDict(this.objLot, this.objAccession, this.objVariety));
    },
    /**
     * Renvoie un dictionnaire correspondant aux propriétés de l'objet
     *
     * @return {Map} Le dictionnaire
     */
    toDict : function(objectLot, objectAccess, objectVariety) {
      var m = new Map();
      // var format = new qx.util.format.DateFormat("dd/MM/yyyy");
      m["name"] = objectLot.getName();
      m["accessionId"] = objectLot.getAccessionId();
      m["lotSourceId"] = objectLot.getLotSourceId();
      m["creationDate"] = objectLot.getCreationDate();
      m["harvestingDate"] = objectLot.getHarvestingDate();
      m["multiplicationDate"] = objectLot.getMultiplicationDate();
      m["introductionName"] = objectAccess.getIntroductionName();
      m["comment"] = objectAccess.getComment();
      m["introductionDate"] = objectAccess.getIntroductionDate();
      m["genus"] = objectVariety.getGenus();
      m["specie"] = objectVariety.getSpecie();
      m["listVarietyName"] = objectVariety.getListVarietyName();
      // console.log(m);
      return (m);
    },

    /**
     *
     *
     */
    fromDict : function(objectLot, objectAccess, objectVariety) {
      var store = qx.bom.Storage.getLocal();
      var drafts = store.getItem("drafts");
      if (drafts !== null) {
        objectLot.setName(drafts["name"]);
        objectLot.setAccessionId(drafts["accessionId"]);
        objectLot.setLotSourceId(drafts["lotSourceId"]);
        objectLot.setCreationDate(drafts["creationDate"]);
        objectLot.setHarvestingDate(drafts["harvestingDate"]);
        objectLot.setMultiplicationDate(drafts["multiplicationDate"]);
        objectAccess.setIntroductionName(drafts["introductionName"]);
        objectAccess.setComment(drafts["comment"]);
        objectAccess.setIntroductionDate(drafts["introductionDate"]);
        objectVariety.setGenus(drafts["genus"]);
        objectVariety.setSpecie(drafts["specie"]);
        objectVariety.setListVarietyName(drafts["listVarietyName"]);
      }
      return (drafts);
    }
  }
});
