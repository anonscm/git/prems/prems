/* ************************************************************************

  Copyright: 2018 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/LicenceCeCILLV2-en.html
    See the LICENCE file in the project's top-level directory for details.

  Authors:
    * Lysiane Hauguel, bioinfo team, IRHS

************************************************************************ */

/**
 * This is the main widget for add new taking.
 * Authors : Lysiane Hauguel,
 * May 2019.
 * @asset(glams/*)
 */
qx.Class.define("prems.ui.plant.NameNewVariety", {
  extend: qx.ui.core.Widget,

  events: {
    "delete": "qx.event.type.Event",
    "deleteList": "qx.event.type.Event"
  },

  properties: {
    date : {
      init : null,
      nullable : true,
      event : "changeDate"
    },
    taxonomy : {
      init : null,
      nullable : true,
      event : "changeTaxonomy"
    }
  },

  construct: function() {
    this.base(arguments);
    /*
    *****************************************************************************
     INIT
    *****************************************************************************
    */
    //Services
    this.serviceLot = prems.service.plant.ServiceLot.getInstance();

    //Containers
    this.container = new qx.ui.container.Composite(new qx.ui.layout.HBox(10));
    this._setLayout(new qx.ui.layout.Grow());
    this.lbName = new qx.ui.basic.Label().set({
      value: "Nom : ",
      rich: true });
    this.cbUsuel = new qx.ui.form.CheckBox();
    this.tfName = new qx.ui.form.TextField();
    this.fcbTypeName = new qxelvis.ui.FilteredComboBox();
    this.fcbTypeName.setPlaceholder("Type de nom");
    this.cbAllType = new qx.ui.form.CheckBox();
    var popup = new qx.ui.popup.Popup(new qx.ui.layout.VBox());
    var labelpopup = new qx.ui.basic.Label().set({
      value: "Affiche tous les types de noms.",
      rich : true,
      width: 150
    });
    popup.add(labelpopup);
    this.date = new qx.ui.form.DateField();
    this.dateFormat = new qx.util.format.DateFormat("dd/MM/yyyy");
    this.date.setDateFormat(this.dateFormat);
    this.date.setValue(new Date());
    this.btnDel = new qx.ui.form.Button("-");
    this.btnDel.setAllowStretchX(false);
    this.btnDel.setAlignX("right");
    this.container.add(this.cbUsuel);
    this.container.add(this.lbName);
    this.container.add(this.tfName, {flex: 1});
    this.container.add(this.fcbTypeName);
    this.container.add(this.cbAllType);
    this.container.add(this.date);
    this.container.add(this.btnDel);
    this._add(this.container);

    // LISTENNERS

    this.serviceLot.addListener("gotVarietyNameType", function(e) {
      var result = e.getData();
      var listTypeVar = [];
      this.listTypeUsuel = [];
      for (let n in result) {
        if (result[n]["value"] != "nom usuel") {  
          listTypeVar.push({
          "label": result[n]["value"],
          "id": result[n]["id"]
          });
        } else {
          this.listTypeUsuel.push({
          "label": result[n]["value"],
          "id": result[n]["id"]
          });
        }
      }
      this.fcbTypeName.setListModel(listTypeVar);
    }, this);
    this.serviceLot.getVarietyNameType([[453]]);

    this.cbUsuel.addListener("changeValue", function(e) {
      var loadData = e.getData();
      if (loadData == false) {
        this.lbName.setValue("Nom : ");
      } else {
        this.lbName.setValue("Nom usuel : ");
      }
    }, this);

    this.cbAllType.addListener("changeValue", function(e) {
      var loadData = e.getData();
      var tabTaxonomy = [[453]];
      if (loadData == false) {
        this.serviceLot.getVarietyNameType(tabTaxonomy);
        this.serviceLot.addListener("gotVarietyNameType", function(e) {
          var result = e.getData();
          var listTypeVar = [];
          for (let n in result) {
            if (result[n]["value"] != "nom usuel") {  
              listTypeVar.push({
              "label": result[n]["value"],
              "id": result[n]["id"]
              });
            }
          }
          this.fcbTypeName.setListModel(listTypeVar);
        }, this);
      } else {
        this.serviceLot.getVarietyNameType();
        this.serviceLot.addListener("gotVarietyNameType", function(e) {
          var result = e.getData();
          var listTypeVar = [];
          for (let n in result) {
            if (result[n]["value"] != "nom usuel") {  
              listTypeVar.push({
              "label": result[n]["value"],
              "id": result[n]["id"]
              });
            }
          }
          this.fcbTypeName.setListModel(listTypeVar);
        }, this);
      }
    }, this);

    this.btnDel.addListener("execute", function() {
      this.fireDataEvent("delete", this);
    }, this);

    this.cbAllType.addListener("mouseover", function(e) { 
      popup.placeToPointer(e);
      popup.show();
    }, this);

    this.cbAllType.addListener("mouseout", function(e) {
      if (popup !== null) {
        popup.setVisibility("excluded");
      }
    }, this);
  },  

  members : {

    binding : function() {
      var object = new prems.data.plant.VarietyName();
      this.tfName.bind("value", object, "value");
      // object.bind("value", this.tfName, "value");
      this.fcbTypeName.bind("value", object, "type");
      // object.bind("value", this.fcbTypeName, "type");
      this.date.bind("value", this, "date");
      object.setDate(this.dateFormat.format(this.getDate()));
      this.fcbTypeName.addListener("changeSelection", function(e) {
        var loadData = e.getData();
        object.setVarietyNameTypeId(loadData.getId());
      }, this);
      return object;
    },

    bindingUsuel : function() {
      var object = new prems.data.plant.VarietyName();
      this.tfName.bind("value", object, "value");
      // object.bind("value", this.tfName, "value");
      object.setType(this.listTypeUsuel[0]["label"]);
      object.setVarietyNameTypeId(this.listTypeUsuel[0]["id"]);
      this.date.bind("value", this, "date");
      object.setDate(this.dateFormat.format(this.getDate()));
      return object;
    },

    deleteObjectList : function(object) {
      this.fireDataEvent("deleteList", object);
    }
  }
});
