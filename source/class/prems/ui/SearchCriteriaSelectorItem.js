/**
 * Search widget
 * Widget de recherche qui correspond à la ligne type de recherche, opérateur, valeur
 * février 2019
 */
qx.Class.define("prems.ui.SearchCriteriaSelectorItem",
{
  extend : qx.ui.core.Widget,
  events : {
    /**
     * Fired when the widget is going to be deleted.
     */
    "delete" : "qx.event.type.Data"
  },
  properties : {
    listeCritereRecherche : {
      init : null,
      nullable : true,
      check : "Array",
      event : "changeListeCritereRecherche"
    },
    criterion : {
      init : null,
      nullable : true,
      event : "changeCriterion"
    },
    operator : {
      init : null,
      nullable : true,
      event : "changeOperator"
    },
    value : {
      init : null,
      nullable : true,
      event : "changeValue"
    }
  },

  construct : function() {
    this.base(arguments);

    /*-- UI drawing --*/

    //Layouts
    let container = new qx.ui.container.Composite(new qx.ui.layout.HBox(5));
    this._setLayout(new qx.ui.layout.Grow());
    this._add(container);

    // Critère de recherche
    this.__critere = new qxelvis.ui.FilteredComboBox();
    this.__critere.setMinWidth(200);
    // To be used for binding
    // let critereList = this.__critere.getChildControl("dropdown").getChildControl("list");

    // Operateur
    this.__operator = new qx.ui.form.SelectBox();
    this.__operator.add(new qx.ui.form.ListItem("égal"));
    this.__operator.add(new qx.ui.form.ListItem("notations absente"));
    this.__operator.add(new qx.ui.form.ListItem("dans"));
    this.__itemNomProche = new qx.ui.form.ListItem("nom proche");
    this.__itemNomProche.hide();
    this.__operator.add(this.__itemNomProche);

    this.__operator.addListener("changeSelection", function(e) {
      if (e.getData()[0].getLabel() == "dans") {
        this.__valueList.show();
        this.__value.exclude();
      } else {
        this.__value.show();
        this.__valueList.exclude();
      }
    }, this);

    // Valeur
    this.__valueList = new qx.ui.form.TextArea();
    // this.__valueList.setWidth(200);
    this.__value = new qxelvis.ui.FilteredComboBox();
    // this.__value.setWidth(200);
    container.add(this.__critere);
    container.add(this.__operator);
    container.add(this.__value, {flex: 1});
    container.add(this.__valueList, {flex: 1});
    this.__valueList.exclude();

    // Bouton moins
    this.buttonMoins = new qx.ui.form.Button(this.tr("-"));
    this.buttonMoins.setAlignX("right");
    container.add(this.buttonMoins);

    // Loading gif
    this.__loading = new qx.ui.basic.Image("prems/loading.gif");
    this.__loading.setAlignX("center");
    container.add(this.__loading);
    this.__loading.exclude();

    /* -- Listeners --*/

    //liste des criteres de recheche
    this.addListener("changeListeCritereRecherche", function() {
      if (this.getListeCritereRecherche()) {
        this.__critere.setListModel(this.getListeCritereRecherche());
      }
    }, this);

    //Listener bouton -
    this.buttonMoins.addListener("execute", function() {
      this.fireDataEvent("delete", this);
    }, this);

    // listener de la FilteredComboBox critere
    this.__critere.addListener("changeSelection", function(e) {
      this.setCriterion({"label" : this.__critere.getValue(), "type" : e.getData().getType(), "id" : e.getData().getId()});
      this.setModelFilteredCBValue();

      if (this.__critere.getValue()=="Nom variété ou lignée") {
        this.__itemNomProche.show();
      } else {
        this.__itemNomProche.hide();
      }
    }, this);

    // listener de la FilteredComboBox value
    this.__value.addListener("changeSelection", function(e) {
      this.setValue(this.__value.getValue());
      this.setModelFilteredCBValue();
    }, this);
  },

  members : {
    /**
     * get operateur
     * @return {String} =,>,< ~* ...
     */
    getOperator : function() {
      let operator = this.__operator.getSelection()[0].getLabel();
      if (operator == "égal") {
        return "=";
      }
      if (operator == "dans") {
        return "in";
      }
      if (operator == "notations absente") {
        return "abs";
      }
      if (operator == "nom proche") {
        return "near";
      }
      this.error("Unrecognized operator [" + operator + "]");
      return "";
    },

    /**
     * get Value
     *
     */
    getValue : function() {
      if (this.__valueList.isVisible()) {
        if (this.__valueList.getValue()!== null) {
          var list = this.__valueList.getValue().split("\n");
          var value = list.join("|");
          return value;
        }
        return null;
      }
      return this.__value.getValue();
    },

    /**
     * get Model of filteredComBox value
     */
    setModelFilteredCBValue : function() {
     //charge les usergroups et idsession
      var userGroups = [];
      var session = qxelvis.session.service.Session.getInstance();
      var sessionId = session.getSessionId();
      if (session.getUserGroupsInfo() !== null) {
        userGroups = session.getUserGroupsInfo().getReadGroupIds();
      }
      //charge les idtypeNotation
      if (this.getCriterion()["type"] == "notation_type") {
        this.__loading.show();
        this.__value.setPlaceholder(this.tr("Valeur de notation"));
        let serviceValuesNot = prems.ServiceNotation.getInstance().query("getValuesNotationsByTypeNotationAndGroup", [sessionId, userGroups, [this.getCriterion()["id"]]]);
        serviceValuesNot.addListener("changeResponseModel", function(e) {
          var listItemValue = [];
          for (let val in e.getData()["result"]) {
            listItemValue.push({"label": e.getData()["result"][val]["value"]});
          }
          this.__value.setListModel(listItemValue);
          this.__loading.exclude();
        }, this);
      }
      if (
        this.getCriterion()["type"] == "name_variety" ||
        this.getCriterion()["type"] == "name_accession" ||
        this.getCriterion()["type"] == "name_lot"
      ) {
        this.__value.setPlaceholder(this.tr("Filter Name ( 3 carracters minimum)"));
        this.__value.addListener("changeSelectedString", function(e) {
          let loadData = e.getData();
          if (loadData.length > 2) {
          this.__loading.show();
            let serviceNamesLot = prems.ServicePlant.getInstance().query("getAllNamesByGroup", [sessionId, userGroups, this.getCriterion()["type"], e.getData()]);
            serviceNamesLot.addListener("changeResponseModel", function(e) {
              let listItemValue = [];
                for (let val in e.getData()["result"]) {
                  listItemValue.push({"label": e.getData()["result"][val]["value"]});
                }
              this.__value.setListModel(listItemValue);
              this.__loading.exclude();
            }, this);
          }
        }, this);
      }
      if (this.getCriterion()["type"] == "place_type") {
        this.__loading.show();
        this.__value.setPlaceholder(this.tr("Valeur de notation"));
        let serviceValuesNot = prems.ServicePlant.getInstance().query("getValuesPlacesByTypeAndGroup", [sessionId, userGroups, [this.getCriterion()["id"]]]);
        serviceValuesNot.addListener("changeResponseModel", function(e) {
          let listItemValue = [];
          for (let val in e.getData()["result"]) {
            listItemValue.push({"label": e.getData()["result"][val]["value"]});
          }
          this.__value.setListModel(listItemValue);
          this.__loading.exclude();
        }, this);
      }
    }
  }
});
