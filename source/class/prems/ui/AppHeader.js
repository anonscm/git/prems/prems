/**
 * An Application Header
 */
qx.Class.define("prems.ui.AppHeader", {
  extend : qx.ui.core.Widget,

  /**
   * Constructor
   *
   * @param name {String} The name to be displayed
   * @param version {String} The version number to be displayed
   */
  construct : function(name, version) {
    this.base(arguments);
    var logo = new qx.ui.basic.Image("prems/crb_logo.png");
    var nameLbl = new qx.ui.basic.Label(name);
    var versionLbl = new qx.ui.basic.Label("v." + version);

    nameLbl.setFont(new qx.bom.Font(20, ["sans"]));

    this._setLayout(new qx.ui.layout.Canvas());
    this._add(logo, {top : 10, left : 10, bottom : 10});
    this._add(nameLbl, {left : 215, bottom : 22});
    this._add(versionLbl, {right : 10, bottom : 0});
  }
});
