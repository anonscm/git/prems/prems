/**
 * Main menu of prems application
 *
 * This menu is a widget that contains a qx.ui.menubar.MenuBar
 * It can only fire events when user click on menu buttons.
 */
qx.Class.define("prems.ui.Menu", {
  extend : qx.ui.core.Widget,

  events : {
    /**
     * Fired when a login action is wanted
     */
    "wantLogin" : "qx.event.type.Event",

    /**
     * Fired when user account details is wanted
     */
    "wantUserAccount" : "qx.event.type.Event",

    /**
     * Fired when user want to add a new variety
     */
    "newVariety" : "qx.event.type.Event",

    /**
     * Fired when user want to add a new accession
     */
    "newAccession" : "qx.event.type.Event",

    /**
     * Fired when user want to add a new tree lot
     */
    "newTreeLot" : "qx.event.type.Event",

    /**
     * Fired when user want to add a new seed lot
     */
    "newSeedLot" : "qx.event.type.Event",

    /**
     * Fired when user want to add a new notation on lot
     */
    "newLotNotation" : "qx.event.type.Event",

    /**
     * Fired when user want to import plant material
     */
    "importMaterial" : "qx.event.type.Event",

    /**
     * Fired when user want to import notations
     */
    "importNotations" : "qx.event.type.Event",

    /**
     * Fired when user want to search material
     */
    "searchMaterial" : "qx.event.type.Event",

    /**
     * Fired when user want to view the summary
     */
    "viewSummary" : "qx.event.type.Event",

    /**
     * Fired when user want to export some descriptors
     */
    "exportDescriptors" : "qx.event.type.Event",

    /**
     * Fired when user want to view contacts
     */
    "viewContacts" : "qx.event.type.Event",

    /**
     * Fired when user want to view name types
     */
    "viewNameTypes" : "qx.event.type.Event",

    /**
     * Fired when user want to view place types
     */
    "viewPlaceTypes" : "qx.event.type.Event",

    /**
     * Fired when user want to view the taxonomy
     */
    "viewTaxonomy" : "qx.event.type.Event",

    /**
     * Fired when user want to view the collections
     */
    "viewCollections" : "qx.event.type.Event",

    /**
     * Fired when user want to view the notations types
     */
    "viewNotationTypes" : "qx.event.type.Event",

    /**
     * Fired when user want to view the list of sites
     */
    "viewSites" : "qx.event.type.Event"
  },

  construct : function(desktop, toolbar) {
    this.base(arguments);

    this.__menu = new qx.ui.menubar.MenuBar();
    var menu = this.__menu;

    this._setLayout(new qx.ui.layout.Grow());
    this._add(menu);

    // Main menu creation
    var addMenuBtn = new qx.ui.menubar.Button(this.tr("Ajouter"), "prems/add.png", this.getNewMenu());
    var searchMenuBtn = new qx.ui.menubar.Button(this.tr("Rechercher"), "prems/search.png", this.getSearchMenu());
    var visuMenuBtn = new qx.ui.menubar.Button(this.tr("Visualiser"), "prems/bilan.png", this.getVisualisationMenu());
    var helpMenuBtn = new qx.ui.menubar.Button(this.tr("Aide"), "prems/help.png");
    var loginBtn = new qxelvis.ui.menubar.UserButton("prems/user.png");

    helpMenuBtn.setEnabled(false);

    loginBtn.addListener("wantLogin", function() {
      this.fireEvent("wantLogin");
    }, this);

    loginBtn.addListener("wantUserAccount", function() {
      this.fireEvent("wantUserAccount");
    }, this);

    // Build menu
    menu.add(addMenuBtn);
    menu.add(searchMenuBtn);
    menu.add(visuMenuBtn);
    menu.addSpacer();
    menu.add(helpMenuBtn);
    menu.add(loginBtn);
  },

  members : {
    /**
     * Get the menu for the 'new' entry
     *
     * @return {qx.ui.menu.Menu} The menu for the 'new' entry
     */
    getNewMenu : function() {
      var menu = new qx.ui.menu.Menu();
      var newAccession = new qx.ui.menu.Button(this.tr("Accession"));
      var newLot = new qx.ui.menu.Button(this.tr("Lot"), null, null, this.getLotMenu());
      var newVariete = new qx.ui.menu.Button(this.tr("Variété"));
      var newNotation = new qx.ui.menu.Button(this.tr("Notation"));
      var importBtn = new qx.ui.menu.Button(this.tr("Importation"), null, null, this.getImportMenu());

      //Nouvelle accession
      newAccession.addListener("execute", function() {
        this.fireEvent("newAccession");
      }, this);

      //Nouvelle variété
      newVariete.addListener("execute", function() {
        this.fireEvent("newVariety");
      }, this);

      //Notation
      newNotation.addListener("execute", function() {
        this.fireEvent("newLotNotation");
      }, this);

      menu.add(newVariete);
      menu.add(newAccession);
      menu.add(newLot);
      menu.add(new qx.ui.menu.Separator());
      menu.add(newNotation);
      menu.add(new qx.ui.menu.Separator());
      menu.add(importBtn);

      return menu;
    },

    /**
     * Get the 'descriptor' menu
     *
     * @return {qx.ui.menu.Menu} The 'descriptor' menu
     */
    getDescriptorMenu : function() {
      var menu = new qx.ui.menu.Menu();
      var newCollection = new qx.ui.menu.Button(this.tr("Collection"));
      var newPepinieriste = new qx.ui.menu.Button(this.tr("Contact"));
      var newSite = new qx.ui.menu.Button(this.tr("Site"));
      var newTaxinomie = new qx.ui.menu.Button(this.tr("Taxinomie"));
      var newTypeLieu = new qx.ui.menu.Button(this.tr("Type lieu"));
      var newTypeNom = new qx.ui.menu.Button(this.tr("Type nom"));
      var newTypeNotation = new qx.ui.menu.Button(this.tr("Type notation"));
      var exportDescripteur = new qx.ui.menu.Button(this.tr("Export"));

      //Export descriptors
      exportDescripteur.addListener("execute", function() {
        this.fireEvent("exportDescriptors");
      }, this);

      //Nouveau contact
      newPepinieriste.addListener("execute", function() {
        this.fireEvent("viewContacts");
      }, this);

      //Nouveau type nom
      newTypeNom.addListener("execute", function() {
        this.fireEvent("viewNameTypes");
      }, this);

      //Nouveau type lieu
      newTypeLieu.addListener("execute", function() {
        this.fireEvent("viewPlaceTypes");
      }, this);

      //Nouvelle taxinomie
      newTaxinomie.addListener("execute", function() {
        this.fireEvent("viewTaxonomy");
      }, this);

      //Nouvelle collection
      newCollection.addListener("execute", function() {
        this.fireEvent("viewCollections");
      }, this);

      //Nouveau type notation
      newTypeNotation.addListener("execute", function() {
        this.fireEvent("viewNotationTypes");
      }, this);

      //Sites
      newSite.addListener("execute", function() {
        this.fireEvent("viewSites");
      }, this);

      menu.add(newCollection);
      menu.add(newPepinieriste);
      menu.add(newSite);
      menu.add(newTaxinomie);
      menu.add(newTypeLieu);
      menu.add(newTypeNom);
      menu.add(newTypeNotation);
      menu.add(new qx.ui.menu.Separator());
      menu.add(exportDescripteur);

      return menu;
    },

    /**
     * Get the 'search' menu
     *
     * @return {qx.ui.menu.Menu} The 'search' menu
     */
    getSearchMenu : function() {
      var menu = new qx.ui.menu.Menu();
      var searchMateriel = new qx.ui.menu.Button(this.tr("Matériel Végétal"));

      //Recherche matériel végétal
      searchMateriel.addListener("execute", function() {
        this.fireEvent("searchMaterial");
      }, this);

      menu.add(searchMateriel);

      return menu;
    },

    /**
     * Get the 'visualisation' menu
     *
     * @return {qx.ui.menu.Menu} The 'visualisation' menu
     */
    getVisualisationMenu : function() {
      var menu = new qx.ui.menu.Menu();
      var bilanBtn = new qx.ui.menu.Button(this.tr("Bilan"));
      var descriptorBtn = new qx.ui.menu.Button(this.tr("Descripteurs"), null, null, this.getDescriptorMenu());

      bilanBtn.addListener("execute", function() {
        this.fireEvent("viewSummary");
      }, this);

      menu.add(bilanBtn);
      menu.add(descriptorBtn);

      return menu;
    },

    /**
     * Get the 'import' menu
     *
     * @return {qx.ui.menu.Menu} The 'import' menu
     */
    getImportMenu : function() {
      var menu = new qx.ui.menu.Menu();
      var inputMaterielButton = new qx.ui.menu.Button(this.tr("Matériel végétal"));
      var inputNotationButton = new qx.ui.menu.Button(this.tr("Notations"));

      //Matériel végétal
      inputMaterielButton.addListener("execute", function() {
        this.fireEvent("importMaterial");
      }, this);

      //Notations
      inputNotationButton.addListener("execute", function() {
        this.fireEvent("importNotations");
      }, this);

      menu.add(inputMaterielButton);
      menu.add(inputNotationButton);

      return menu;
    },

    /**
     * Get the 'Lot' Menu
     *
     * @return {qx.ui.menu.Menu} The 'Lot' menu
     */
    getLotMenu : function() {
      var menu = new qx.ui.menu.Menu();
      var arbreButton = new qx.ui.menu.Button(this.tr("Arbre"));
      var grainesButton = new qx.ui.menu.Button(this.tr("Graines"));

      arbreButton.addListener("execute", function() {
        this.fireEvent("newTreeLot");
      }, this);
      grainesButton.addListener("execute", function() {
        this.fireEvent("newSeedLot");
      }, this);

      menu.add(arbreButton);
      menu.add(grainesButton);

      return menu;
    }
  }
});
