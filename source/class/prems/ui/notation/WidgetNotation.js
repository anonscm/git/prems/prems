/* ************************************************************************

  Copyright: 2018 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/LicenceCeCILLV2-en.html
    See the LICENCE file in the project's top-level directory for details.

  Authors:
    * Lysiane Hauguel, bioinfo team, IRHS

************************************************************************ */

/**
 * This is the main widget for add new taking.
 * Authors : Lysiane Hauguel,
 * April 2019.
 * @asset(glams/*)
 */
qx.Class.define("prems.ui.notation.WidgetNotation", {
  extend: qx.ui.core.Widget,

  events: {
    delete : "qx.event.type.Event",
    deleteList : "qx.event.type.Event"
  },

  properties: {
    date : {
      init : null,
      nullable : true,
      event : "changeDate"
    }
  },

  construct: function() {
    this.base(arguments);

    /*
    *****************************************************************************
     INIT
    *****************************************************************************
    */
    //Containers
    this.notationContainer = new qx.ui.container.Composite(new qx.ui.layout.VBox(20));
    this._setLayout(new qx.ui.layout.Grow());
    var hBoxNotation = new qx.ui.container.Composite(new qx.ui.layout.HBox(20));
    var btnContext = new qx.ui.form.Button("C");
    btnContext.setVisibility("excluded");
    this.fcbTypeNotation = new qxelvis.ui.FilteredComboBox();
    this.fcbListType = new qxelvis.ui.FilteredComboBox();
    this.fcbListType.setVisibility("excluded");
    this.tfNotation = new qx.ui.form.TextField();
    var lbMore = new qx.ui.basic.Label("More");
    var lbLess = new qx.ui.basic.Label("Less");
    lbLess.setVisibility("excluded");
    this.btnDel = new qx.ui.form.Button("-");
    this.btnDel.setAllowStretchX(false);
    this.btnDel.setAlignX("right");

    hBoxNotation._add(btnContext);
    hBoxNotation._add(this.fcbTypeNotation, {flex: 1});
    hBoxNotation._add(this.fcbListType, {flex: 1});
    hBoxNotation._add(this.tfNotation, {flex: 1});
    hBoxNotation._add(lbMore);
    hBoxNotation._add(lbLess);
    hBoxNotation._add(this.btnDel);

    var gpeMore = new qx.ui.groupbox.GroupBox();
    gpeMore.setVisibility("excluded");
    var layout = new qx.ui.layout.Grid().setSpacing(10);
    layout.setColumnFlex(0, 1);
    layout.setColumnFlex(1, 1);
    gpeMore.setLayout(layout);
    this.date = new qx.ui.form.DateField();
    this.dateFormat = new qx.util.format.DateFormat("dd/MM/yyyy");
    this.date.setDateFormat(this.dateFormat);
    this.date.setValue(new Date());
    this.taRemarque = new qx.ui.form.TextArea();
    var fcbSite = new qxelvis.ui.FilteredComboBox();
    fcbSite.setEnabled(false);

    gpeMore.add(new qx.ui.basic.Label().set({
      value: "Date :",
      rich: true }), {
      row: 0,
      column: 0
    });
    gpeMore.add(this.date, {
      row: 0,
      column : 1
    });
    gpeMore.add(new qx.ui.basic.Label().set({
      value: "Remarques :",
      rich: true }), {
      row: 0,
      column: 3
    });

    gpeMore.add(new qx.ui.basic.Label().set({
      value: "Notateurs :",
      rich: true }), {
      row: 1,
      column: 0
    });
    gpeMore.add(new qx.ui.basic.Label().set({
      value: "Widget à venir !!!",
      rich: true }), {
      row: 1,
      column: 1,
      rowSpan : 1
    });
    gpeMore.add(this.taRemarque, {
      row: 1,
      column : 3,
      colSpan : 1,
      rowSpan : 4
    });

    gpeMore.add(new qx.ui.basic.Label().set({
      value: "Site :",
      rich: true }), {
      row: 3,
      column: 0
    });
    gpeMore.add(fcbSite, {
      row: 3,
      column : 1
    });

    gpeMore.add(new qx.ui.basic.Label().set({
      value: "Protocole :",
      rich: true }), {
      row: 4,
      column: 0
    });
    gpeMore.add(new qx.ui.basic.Label().set({
      value: "lien à ajouter",
      rich: true }), {
      row: 4,
      column: 1
    });

    this.notationContainer.add(hBoxNotation);
    this.notationContainer.add(gpeMore);

    this._add(this.notationContainer);

    // LISTENNERS

    lbMore.addListener("click", function() {
      gpeMore.setVisibility("visible");
      lbMore.setVisibility("excluded");
      lbLess.setVisibility("visible");
    }, this);

    lbLess.addListener("click", function() {
      gpeMore.setVisibility("excluded");
      lbMore.setVisibility("visible");
      lbLess.setVisibility("excluded");
    }, this);

    this.btnDel.addListener("execute", function() {
      this.fireDataEvent("delete", this);
      }, this);

    var testContext = prems.service.notation.ServiceNotation.getInstance();

    testContext.addListener("gotContext", function (e) {
      // TODO le contexte choisi selon la page, ici j'ai mis 1 pour faire des tests
      // console.log(e.getData());
      var loadData = e.getData()[1]["typeNotList"];
      var listTypeNot = [];
      for (var i in loadData) {
        listTypeNot.push({
          "label" : loadData[i]["name"],
          "id" : loadData[i]["id"],
          "type" : loadData[i]["type"],
          "liste" : loadData[i]["list"]
        });
      }
      this.fcbTypeNotation.setListModel(listTypeNot);
    }, this);

    this.fcbTypeNotation.addListener("changeSelection", function(e) {
      var loadData = e.getData();
      if (loadData.getType() == "list") {
        this.fcbListType.setVisibility("visible");
        this.tfNotation.setVisibility("excluded");
        var listNot = [];
        for (var i in loadData.getListe().toArray()) {
          listNot.push({
            "label" : loadData.getListe().toArray()[i].getValue(),
            "code" : loadData.getListe().toArray()[i].getCode()
          });
        }
        this.fcbListType.setListModel(listNot);
      } else {
        this.fcbListType.setVisibility("excluded");
        this.tfNotation.setVisibility("visible");
      }
    }, this);
    testContext.getContext();
  },

  members : {

    binding : function() {
      var object = new prems.data.notation.Notation();
      this.fcbTypeNotation.bind("value", object, "type");
      // object.bind("value", this.fcbTypeNotation, "type");
      this.tfNotation.bind("value", object, "value");
      // object.bind("value", this.tfNotation, "value");
      this.date.bind("value", this, "date");
      object.setDate(this.dateFormat.format(this.getDate()));
      this.taRemarque.bind("value", object, "comment");
      // object.bind("value", this.taRemarque, "comment");
      return object;
    },

    deleteObjectList : function(object) {
      this.fireDataEvent("deleteList", object);
    }
  }
});
