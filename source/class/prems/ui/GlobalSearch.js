/**
* Recherche variétés, lots, accessions
* mars 2019
* Fabrice Dupuis
*TODO  passer le container __searchWidgetContainer en widget contenant aussi le bouton +
* avec les methodes associées.
* Ajouter aux properties tous les choix utilisateurs.
* Utiliser un minimum de membres privés.
*/
qx.Class.define("prems.ui.GlobalSearch", {
  extend : qx.ui.core.Widget,
  events : {
    /**
    * Fired to display information on a plant
    */
    "displayInformations" : "qx.event.type.Data",

    /**
    * Fired to export information on a plant
    */
    "tableToCSV" : "qx.event.type.Event",

    /**
    * Fired to display information on a plant
    */
    "displayLot" : "qx.event.type.Event",

    /**
    * Fired to display information on a plant
    */
    "displayAccession" : "qx.event.type.Event",

    /**
    * Fired to display information on a plant
    */
    "displayVariete" : "qx.event.type.Event",

    /**
    * Fired to display information on a plant
    */
    "displayLotPlace" : "qx.event.type.Event",

    /**
    * Fired when user request some help
    */
    "wantsHelp" : "qx.event.type.Event"
  },

  properties : {
    //liste de criteres de recherche
    listeCritereRecherche : {
      init : null,
      nullable : true,
      check : "Array",
      event : "changeListeCritereRecherche"
    },
    // choix de l'affichage : perennes ou annuelles
    cultivatedPlant : {
      init : true,
      nullable : true,
      check : "Boolean",
      event : "changeCultivatedPlant"
    },
    //chekBox des emplacements
    displayPlaces : {
      init : true,
      nullable : true,
      check : "Boolean",
      event : "changeDisplayPlaces"
    }
  },

  construct : function() {
    this.base(arguments);

    //service
    let serviceLot = prems.service.plant.ServiceLot.getInstance();

    /* -- Data Management --*/

    //liste des items de la virtualCombobox des criteres
    this.__listItemCriteres = [];

    /*-- UI drawing --*/

    //Container + layout
    this.__container = new qx.ui.container.Composite(new qx.ui.layout.VBox(5));
    this._setLayout(new qx.ui.layout.Grow());
    this._add(this.__container);
    //container de recherche
    this.__searchContainer = new qx.ui.container.Composite(new qx.ui.layout.VBox(5));
    let title0 = new qx.ui.basic.Label("Recherche de matériel végétal selon des critères de noms ou de notations");
    let title1 = new qx.ui.basic.Label("les valeurs numériques peuvent être précédées des opérateurs < > <= >=");
    let title2 = new qx.ui.basic.Label("les recherches de texte peuvent utiliser * pour remplacer tout carractère");
    this.__searchContainer.add(title0);
    this.__searchContainer.add(title1);
    this.__searchContainer.add(title2);

    //Lignes de recherche
    this.__searchWidgetContainer = new qx.ui.container.Composite(new qx.ui.layout.VBox(5));

    //liste de prems.ui.SearchCriteriaSelectorItem
    this.__searchWidgetList = [];

    //Bouton ajouter ligne (Le bouton - est contenu dans le widget)
    let buttonPlus = new qx.ui.form.Button("+");
    buttonPlus.setAllowStretchX(false);
    buttonPlus.setAlignX("right");

    //Bouton pour lancer la recherche
    let buttonSearch = new qx.ui.form.Button(this.tr("Recherche"));
    buttonSearch.setAllowStretchX(false);
    buttonSearch.setAlignX("right");

    this.__searchContainer.add(this.__searchWidgetContainer);
    this.__searchContainer.add(buttonPlus);

    let title3 = new qx.ui.basic.Label("Options d'affichage des résultats");
    this.__searchContainer.add(title3);

    //Choix recherche variété, accession, lot
    this.__typeSearchContainer = new qx.ui.container.Composite(new qx.ui.layout.HBox(5));
    let variete = new qx.ui.form.RadioButton("Variété ou lignée");
    let accession = new qx.ui.form.RadioButton("Accession");
    let lot = new qx.ui.form.RadioButton("Lot");
    let placeLot = new qx.ui.form.RadioButton("Lot avec emplacement");
    this.__typeSearch = new qx.ui.form.RadioGroup();
    this.__typeSearch.add(lot, accession, variete, placeLot);
    this.__typeSearchContainer.add(placeLot);
    this.__typeSearchContainer.add(lot);
    this.__typeSearchContainer.add(accession);
    this.__typeSearchContainer.add(variete);

    let buttonHelp = new qx.ui.form.Button("?");
    this.__typeSearchContainer.add(buttonHelp);
    this.__searchContainer.add(this.__typeSearchContainer);

    //checkbox de recherche avec emplacement
    this.__cbPlaces = new qx.ui.form.CheckBox("Recherche avec emplacement");
    this.__cbPlaces.setValue(true);
    this.__cbPlaces.bind("value", this, "displayPlaces");
    //this.bind("displayPlaces", this.__cbPlaces,"value");
    this.__searchContainer.add(this.__cbPlaces);
    this.__searchContainer.add(buttonSearch);

    //Bouton Export csv
    this.__exportButton = new qx.ui.form.Button(this.tr("Export csv"));
    this.__exportButton.setAllowStretchX(false);
    this.__exportButton.setAlignX("right");

    //Loading gif
    this.__loading = new qx.ui.basic.Image("prems/loading.gif");
    this.__loading.setAlignX("center");
    this.__container.add(this.__loading);
    //this.__loading.exclude();

    /* -- Listeners --*/

    this.addListener("changeDisplayPlaces", function () {
      if (this.getDisplayPlaces() == true) {
        placeLot.setEnabled(true);
        //this.__typeSearch.setSelection(placeLot);
      } else {
        placeLot.setEnabled(false);
        //this.__typeSearch.setSelection(lot);
      }
    }, this);

    buttonHelp.addListener("execute", function() {
      this.fireEvent("wantsHelp");
    }, this);


    //ajout 1 ligne de recherche
    buttonPlus.addListener("execute", function() {
      this.addSearchLine();
    }, this);

    //Listener lorque la recherche est lancée
    buttonSearch.addListener("execute", function() {
      this.searchExecute();
    }, this);

    // choix d'affichage variete/accession/lot
    this.__typeSearch.addListener("changeSelection", function(e) {
      switch (this.__typeSearch.getSelection()[0].getLabel()) {
        case "Lot":
        this.fireDataEvent("displayLot", this);
        break;
        case "Accession":
        this.fireDataEvent("displayAccession", this);
        break;
        case "Variété ou lignée":
        this.fireDataEvent("displayVariete", this);
        break;
        case "Lot avec emplacement":
        this.fireDataEvent("displayLotPlace", this);
        break;
      }
    }, this);

    serviceLot.addListener("gotVarieteByLot", function(e) {
      let info = e.getData()[0];
      // let id = info["variete"];
      // let cultivated = true;
      // let genus = info["genus"];
      // let specie = info["specie"];
      info["ligneTable"] = this.ligneTable;
      this.fireDataEvent("displayInformations", info);
    }, this);

    //Listener export csv
    this.__exportButton.addListener("execute", function() {
      this.fireDataEvent("tableToCSV", this);
    }, this);

    // Actions
    this.getAllCriteres();
  },

  members : {
    /**
    * Affichage des composants de la fenêtre graphique
    */
    __display : function() {
      this.__loading.exclude();
      this.addSearchLine();
      this.__container.add(this.__searchContainer);
      this.__container.add(new qx.ui.table.Table(), {flex : 1});
      this.__container.add(this.__exportButton);
    },
    /**
    * instructions after execute button search
    */
    searchExecute : function() {
      //On affiche loading gif
      this.__loading.show();

      //3 listes des critères de recherche notations nom et place
      let listeNotation = [];
      let listeNames = [];
      let listePlaces = [];
      for (let s in this.__searchWidgetList) {
        //recherche sur name_lot
        if (this.__searchWidgetList[s].getCriterion()["type"] == "name_lot") {
          listeNames.push({
            "name_type": this.__searchWidgetList[s].getCriterion()["type"],
            "value_name": this.__searchWidgetList[s].getValue(),
            "operator": this.__searchWidgetList[s].getOperator()
          });
        }
        //recherche sur name_accession
        if (this.__searchWidgetList[s].getCriterion()["type"] == "name_accession") {
          listeNames.push({"name_type": this.__searchWidgetList[s].getCriterion()["type"], "value_name": this.__searchWidgetList[s].getValue(), "operator": this.__searchWidgetList[s].getOperator()});
        }
        //recherche sur name_variete
        if (this.__searchWidgetList[s].getCriterion()["type"] == "name_variety") {
          listeNames.push({"name_type": this.__searchWidgetList[s].getCriterion()["type"], "value_name": this.__searchWidgetList[s].getValue(), "operator": this.__searchWidgetList[s].getOperator()});
        }
        //recherche avec type_notation
        if (this.__searchWidgetList[s].getCriterion()["type"] == "notation_type") {
          listeNotation.push({"id_notation_type": this.__searchWidgetList[s].getCriterion()["id"], "value_notation": this.__searchWidgetList[s].getValue(), "operator": this.__searchWidgetList[s].getOperator()});
        }
        //recherche sur place
        if (this.__searchWidgetList[s].getCriterion()["type"] == "place_type") {
          listePlaces.push({"id_place_type": this.__searchWidgetList[s].getCriterion()["id"], "value_place": this.__searchWidgetList[s].getValue(), "operator": this.__searchWidgetList[s].getOperator()});
        }
      }
      //effectue la recherche des plantes avec ces critères
      this.getNamesPlants(listeNames, listeNotation, listePlaces);
    },
    /**
    * charge toutes les valeurs de la liste des criteres.
    */
    getAllCriteres : function() {
      this.__listItemCriteres = [];
      this.__listItemCriteres.push({
        "label": "Nom lot",
        "type" : "name_lot",
        "id": null
      });
      this.__listItemCriteres.push({
        "label": "Nom accession plantes pérennes",
        "type" : "name_accession",
        "id": null
      });
      this.__listItemCriteres.push({
        "label": "Nom variété ou lignée",
        "type" : "name_variety",
        "id": null
      });
      this.__display();
      //tous les types de notations attachées à un groupe
      //charge les usergroups et idsession
      let userGroups = [];
      let session = qxelvis.session.service.Session.getInstance();
      let sessionId = session.getSessionId();
      if (session.getUserGroupsInfo() !== null) {
        userGroups = session.getUserGroupsInfo().getReadGroupIds();
      }

      let serviceNot = prems.ServiceNotation.getInstance();
      let serviceNotGetTypeNot = serviceNot.query("getTypeNotationByGroup", [sessionId, userGroups]);
      serviceNotGetTypeNot.addListener("changeResponseModel", function(e) {
        let result = e.getData()["result"];
        for (let n in result) {
          this.__listItemCriteres.push({
            "label": result[n]["name"],
            "type" : "notation_type",
            "id": result[n]["id"]
          });
        }
        let serviceNotGetTypePlace = prems.ServicePlant.getInstance().query("getAllPlaceNamesByGroup", [sessionId, userGroups]);
        serviceNotGetTypePlace.addListener("changeResponseModel", function(e) {
          let result = e.getData()["result"];
          for (let n in result) {
            this.__listItemCriteres.push({
              "label": "Lieu : "+result[n]["name"],
              "type" : "place_type",
              "id": result[n]["id"]
            });
          }
          this.setListeCritereRecherche(this.__listItemCriteres);
        }, this);
      }, this);
    },
    /**
    * Add a search widget.
    */
    addSearchLine : function() {
      let searchWidget = new prems.ui.SearchCriteriaSelectorItem();
      this.bind("listeCritereRecherche", searchWidget, "listeCritereRecherche");
      // searchWidget.setListeCritereRecherche(this.__listItemCriteres);
      this.__searchWidgetContainer.add(searchWidget);
      this.__searchWidgetList.push(searchWidget);

      searchWidget.addListener("delete", function(e) {
        let widget = e.getData();
        this.debug(widget);
        this.__searchWidgetContainer.remove(widget);
        this.__searchWidgetList.splice(this.__searchWidgetList.indexOf(widget), 1);
        // widget.dispose();
      }, this);
    },
    /**
    * Search plants with listeNames and listeNotations and listePlaces
    */
    getNamesPlants : function(listeNames, listeNotations, listePlaces) {
      //charge les usergroups et idsession
      let userGroups = [];
      let session = qxelvis.session.service.Session.getInstance();
      let sessionId = session.getSessionId();
      let servicePlant = prems.ServicePlant.getInstance();
      if (session.getUserGroupsInfo() !== null) {
        userGroups = session.getUserGroupsInfo().getReadGroupIds();
      }
      let lnot = [];
      let lnam = [];
      let lplace = [];
      let lnamIn = [];
      let lnotIn = [];
      let lplaceIn = [];
      let lnamNear = [];
      let lnotAbs = [];
      for (let n in listeNotations) {
        if (listeNotations[n]["operator"]== "=") {
          lnot.push({"id_notation_type": listeNotations[n]["id_notation_type"], "value_notation": listeNotations[n]["value_notation"] });
        }
        if (listeNotations[n]["operator"]== "in") {
          lnotIn.push({"id_notation_type": listeNotations[n]["id_notation_type"], "value_notation": listeNotations[n]["value_notation"] });
        }
        if (listeNotations[n]["operator"]== "abs") {
          lnotAbs.push({"id_notation_type": listeNotations[n]["id_notation_type"] });
        }
      }
      for (let n in listeNames) {
        if (listeNames[n]["operator"]== "=") {
          lnam.push({"name_type": listeNames[n]["name_type"], "value_name": listeNames[n]["value_name"] });
        }
        if (listeNames[n]["operator"]== "in") {
          lnamIn.push({"name_type": listeNames[n]["name_type"], "value_name": listeNames[n]["value_name"] });
        }
        if (listeNames[n]["operator"]== "near") {
          lnamNear.push({"name_type": listeNames[n]["name_type"], "value_name": listeNames[n]["value_name"] });
        }
      }
      for (let n in listePlaces) {
        if (listePlaces[n]["operator"]== "=") {
          lplace.push({"id_place_type": listePlaces[n]["id_place_type"], "value_place": listePlaces[n]["value_place"] });
        }
        if (listePlaces[n]["operator"]== "in") {
          lplaceIn.push({"id_place_type": listePlaces[n]["id_place_type"], "value_place": listePlaces[n]["value_place"] });
        }
      }
      //lance la recherche sur les notations noms et places
      let serviceSP = servicePlant.query("getNamesLotsByNamesOrNotationsOrPlaces", [sessionId, userGroups, lnam, lnot, lplace, lnamIn, lnotIn, lplaceIn, lnamNear, lnotAbs]);
      serviceSP.addListener("changeResponseModel", function(e) {
        let r = e.getData()["result"];
        let allData = [];
        let listIdLot = [];
        let listeNewKeys = ["id", "lot_name", "introduction_name", "variety_name"];
        for (let l in r) {
          allData.push({"lot_name":r[l]["lot_name"], "variety_name":r[l]["variety_name"], "introduction_name":r[l]["introduction_name"], "id":r[l]["id"].toString()});
          listIdLot.push(r[l]["id"]);
        }
        //ajoute les emplacements des lots
        if (this.getDisplayPlaces() == true) {
          let servicePP = servicePlant.query("getAllPlacesByIdLots", [sessionId, userGroups, listIdLot]);
          servicePP.addListener("changeResponseModel", function(e) {
            let r = e.getData()["result"];
            for (let j in r) {
              for (let k in allData) {
                if (r[j]["id"].toString() == allData[k]["id"]) {
                  allData[k][r[j]["type"]] = r[j]["value"];
                  if (listeNewKeys.indexOf(r[j]["type"]) == -1) {
                    listeNewKeys.push(r[j]["type"]);
                  }
                  break;
                }
              }
            }
            //complète de tableau remplaces données abscentes par des données nulles
            /*
            for (let j in allData) {
              for (let k in listeNewKeys) {
              }
            }
            */
            //this.__loading.exclude();
            this.displayResult(allData, listeNewKeys);
            this.__loading.exclude();
          }, this);
        } else {
          this.displayResult(allData, listeNewKeys);
          this.__loading.exclude();
        }
      }, this);
    },
    /**
    * Displays the results in the table.
    * @param result {Array} result to diplay
    */
    displayResult : function(result, listeKeys) {
      //cree une nouvelle table
      let tableResult = new qx.ui.table.Table();
      tableResult.setMinHeight(50);
      /*
      tableResult.setHeight(100);
      tableResult.setAllowStretchX(true, true);
      tableResult.setAllowStretchY(true, true);
      */

      let tableModel = new qx.ui.table.model.Filtered();
      tableModel.setColumns(listeKeys);
      tableModel.setDataAsMapArray(result);
      tableResult.setTableModel(tableModel);
      let tcm = tableResult.getTableColumnModel();
      let tm = tableResult.getTableModel();
      for (let i = 0; i < tm.getColumnCount(); i++) {
        tcm.setColumnVisible(i, true);
      }
      tableResult = this.orderTable(tableResult);
      this.__container.removeAt(2);
      // let oldTable = this.__container.removeAt(2);
      // oldTable.dispose(); // FIXME Needs to be disposed or better do not recreate a Table for each result
      this.__container.addAt(tableResult, 2, {flex : 1});
      /* listeners */
      tableResult.addListener("cellDbltap", function(e) {
        let serviceLot = prems.service.plant.ServiceLot.getInstance();
        //Récupère l'id dans la colonne id du tableau
        let id = Number(tableResult.getTableModel().getRowDataAsMap(e.getRow())["id"]);
        this.ligneTable = tableResult.getTableModel().getRowDataAsMap(e.getRow());
        serviceLot.getVarieteByLot(id);
      }, this);
      this.addListener("displayLot", function() {
        tableModel.setDataAsMapArray(result);
        tableResult.setTableModel(tableModel);
        let tcm = tableResult.getTableColumnModel();
        let tm = tableResult.getTableModel();
        for (let i = 0; i < tm.getColumnCount(); i++) {
          tcm.setColumnVisible(i, false);
          //Met en premier la colonne 'Nom lot'
          if (tm.getColumnName(i)=="lot_name") {
            tcm.moveColumn(i, 0);
            tm.sortByColumn(i, true);
            tcm.setColumnVisible(i, true);
          }
          //Met en second la colonne 'introduction_name'
          if (tm.getColumnName(i)=="introduction_name") {
            tcm.moveColumn(i, 1);
            tcm.setColumnVisible(i, true);
          }
          //Met en troisième la colonne 'variety_name'
          if (tm.getColumnName(i)=="variety_name") {
            tcm.moveColumn(i, 2);
            tcm.setColumnVisible(i, true);
          }
        }
      }, this);
      this.addListener("displayAccession", function() {
        let unique = [];
        let resultAcc = [];
        for (let l in result) {
          if (unique.indexOf(result[l]["introduction_name"]) < 0) {
            unique.push(result[l]["introduction_name"]);
            resultAcc.push({"introduction_name": result[l]["introduction_name"], "id": result[l]["id"], "variety_name": result[l]["variety_name"]});
          }
        }
        tableModel.setDataAsMapArray(resultAcc);
        tableResult.setTableModel(tableModel);
        let tcm = tableResult.getTableColumnModel();
        let tm = tableResult.getTableModel();
        for (let i = 0; i < tm.getColumnCount(); i++) {
          tcm.setColumnVisible(i, false);
          //Met en second la colonne 'introduction_name'
          if (tm.getColumnName(i)=="introduction_name") {
            tcm.moveColumn(i, 0);
            tcm.setColumnVisible(i, true);
          }
          //Met en troisième la colonne 'variety_name'
          if (tm.getColumnName(i)=="variety_name") {
            tcm.moveColumn(i, 1);
            tcm.setColumnVisible(i, true);
          }
        }
      }, this);
      this.addListener("displayVariete", function() {
        let unique = [];
        let resultAcc = [];
        for (let l in result) {
          if (unique.indexOf(result[l]["variety_name"]) < 0) {
            unique.push(result[l]["variety_name"]);
            resultAcc.push({"variety_name": result[l]["variety_name"], "id": result[l]["id"]});
          }
        }
        tableModel.setDataAsMapArray(resultAcc);
        tableResult.setTableModel(tableModel);
        let tcm = tableResult.getTableColumnModel();
        let tm = tableResult.getTableModel();
        for (let i = 0; i < tm.getColumnCount(); i++) {
          tcm.setColumnVisible(i, false);
          //Met en premier la colonne 'variety_name'
          if (tm.getColumnName(i)=="variety_name") {
            tcm.moveColumn(i, 0);
            tcm.setColumnVisible(i, true);
          }
        }
      }, this);
      this.addListener("displayLotPlace", function() {
        tableModel.setDataAsMapArray(result);
        tableResult.setTableModel(tableModel);
        let tcm = tableResult.getTableColumnModel();
        let tm = tableResult.getTableModel();
        for (let i = 0; i < tm.getColumnCount(); i++) {
          tcm.setColumnVisible(i, true);
        }
        tableResult = this.orderTable(tableResult);
      }, this);
    },

    /**
    * Modifie la tableModel
    * Rend invisibles les colonnes des id et ordonne les colonnes
    */
    orderTable : function(table) {
      let tcm = table.getTableColumnModel();
      let tm = table.getTableModel();
      //parcours les noms des colonnes
      for (let i = 0; i < tm.getColumnCount(); i++) {
        tcm.setColumnVisible(i, true);
        //supprime la visibilité des colones des id
        if (tm.getColumnName(i)=="id") {
          tcm.setColumnVisible(i, false);
          //tcm.setDataCellRenderer(i,new qx.ui.table.cellrenderer.Number());
        }
        // Met en premier la colonne 'Nom lot'
        if (tm.getColumnName(i) == "lot_name") {
          tcm.moveColumn(i, 0);
          tm.sortByColumn(i, true);
        }
        // Met en second la colonne 'introduction_name'
        if (tm.getColumnName(i) == "introduction_name") {
          tcm.moveColumn(i, 1);
        }
      }
      this.addListener("tableToCSV", function() {
        let fileWriter = new qxfileio.FileWriter();
        fileWriter.saveTextAsFile(this.__getTableContentAsCsv(table), "exportCRB.csv");
      }, this);
      return table;
    },

    /**
    * Transform tableModel en data.csv
    */
    __getTableContentAsCsv : function(table) {
      let tableModel = table.getTableModel();
      let numberOfRows = tableModel.getRowCount();
      let numberOfColumns = tableModel.getColumnCount();
      let data = "";
      //Colonnes
      for (let i = 0; i < numberOfColumns; i++) {
        if (table.getTableColumnModel().isColumnVisible(i)) {
          data += table.getTableModel().getColumnName(i);
          if (i != numberOfColumns - 1) {
            data += "\t";
          }
        }
      }
      data += "\n";
      //Valeurs
      for (let i = 0; i < numberOfRows; i++) {
        for (let j = 0; j < numberOfColumns; j++) {
          if (table.getTableColumnModel().isColumnVisible(j)) {
            if (tableModel.getValue(j, i)) {
              data += tableModel.getValue(j, i);
            }
            if (j != numberOfColumns - 1) {
              data += "\t";
            }
          }
        }
        data += "\n";
      }
      return data;
    }
  }
});
