/**
* Introduction lot object.
* Stores all the informations of the Emplacement table.
*/
qx.Class.define("prems.Emplacement", {
  extend : qx.core.Object,
  events : {
    /**
    * Fired when the emplacement is saved in the database.
    */
    "saved" : "qx.event.type.Event",

    /**
    * Fired when the emplacement is retrieved from the database.
    */
    "getEnd" : "qx.event.type.Event"
  },
  properties : {
    /**
    * id
    */
    id : {
      init : null,
      nullable : true,
      check : "Integer",
      event : "changeId"
    },

    /**
    * site
    */
    site : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeSite"
    },

    /**
    * lot
    */
    lot : {
      init : null,
      nullable : true,
      check : "Integer",
      event : "changeLot"
    },

    /**
    * Lieux
    */
    lieux : {
      init : new qx.data.Array(),
      nullable : true,
      check : "Array",
      event : "changeLieux"
    }
  },

  /**
  * constructor
  * @param idEmplacement {Integer} id of the emplacement in the database.
  */
  construct : function(idEmplacement) {
    this.base(arguments);

    //Service
    this.__service = prems.service.Service.getInstance();
    this.setLieux(new qx.data.Array());

    //Sets the variete id
    if (idEmplacement) {
      this.setId(idEmplacement);
      this.get();
    }
  },
  members : {
    /**
    * Saves (update or create)
    */
    save : function() {
      let serviceSite = this.__service.query("getSite", [this.getSite()]);
      let idSite = null;
      serviceSite.addListener("changeResponseModel", function() {
        if (serviceSite.getResult().length > 0) {
          idSite = serviceSite.getResult()[0]["id_site"];
        }
        if (this.getId() === null) {
          this.create(idSite);
        } else {
          this.update(idSite);
        }
      }, this);
    },

    /**
    * Get
    */
    get : function() {
      let service = this.__service.query("getEmplacement", [this.getId()]);
      service.addListener("changeResponseModel", function() {
        let idSite = service.getResult()[0]["site"];
        let serviceSite = this.__service.query("getSiteById", [idSite]);
        serviceSite.addListener("changeResponseModel", function() {
          if (serviceSite.getResult().length > 0) {
            this.setSite(serviceSite.getResult()[0]["nom_site"]);
          }
          let serviceLieux = this.__service.query("getLieuxByEmplacement", [this.getId()]);
          serviceLieux.addListener("changeResponseModel", function() {
            let cptLieu = 0;
            let lieuxArray = new qx.data.Array();
            let lieux = serviceLieux.getResult();
            if (lieux.length == 0) {
              this.fireEvent("getEnd");
            } else {
              for (let i in lieux) {
                let lieu = new prems.Lieu(lieux[i]["id_lieu"]);
                lieuxArray.push(lieu);
                lieu.addListener("getEnd", function() {
                  cptLieu++;
                  if (cptLieu == lieux.length) {
                    this.setLieux(lieuxArray);
                    this.fireEvent("getEnd");
                  }
                }, this);
              }
            }
          }, this);
        }, this);
      }, this);
    },

    /**
    * Create
    * @param idSite {Integer} id site
    */
    create : function(idSite) {
      let service = this.__service.query("createEmplacement", [this.getLot(), idSite]);
      service.addListener("changeResponseModel", function() {
        this.setId(service.getResult()[0]["id_emplacement"]);
        this.saveLieux();
      }, this);
    },

    /**
    * Update
    * @param idSite {Integer} id site
    */
    update : function(idSite) {
      let service = this.__service.query("updateEmplacement", [this.getId(), idSite]);
      service.addListener("changeResponseModel", function() {
        this.saveLieux();
      }, this);
    },

    /**
    * Add lieu
    * @param lieu {prems.Lieu} lieu
    */
    addLieu : function(lieu) {
      let lieux = this.getLieux();
      lieux.push(lieu);
      this.setLieux(lieux);
      this.fireDataEvent("changeLieux");
    },

    /**
    * Remove lieu
    * @param lieu {prems.Lieu} lieu
    */
    removeLieu : function(lieu) {
      let lieux = this.getLieux();
      lieux.remove(lieu);
      this.setLieux(lieux);
      this.fireDataEvent("changeLieux");
    },

    /**
    * Save lieux
    */
    saveLieux : function() {
      let cpt = 0;
      if (this.getLieux().length == 0) {
        this.fireEvent("saved");
      }
      this.getLieux().forEach(function(lieu) {
        lieu.save(this.getId());
        lieu.addListener("saved", function() {
          cpt++;
          if (cpt == this.getLieux().getLength()) {
            this.fireEvent("saved");
          }
        }, this);
      }, this);
    },

    /**
    * to CSV
    * @return {Object} object containing all the infos of the emplacement.
    */
    toCsv : function() {
      let data = {};
      data["Site"] = this.getSite();
      this.getLieux().forEach(function(lieu) {
        let colName = "Lieu:" + lieu.getType();
        data[colName] = lieu.getValeur();
      }, this);
      return data;
    }
  }
});
