/**
* Introduction lot object.
* Stores all the informations of the "Lot" table (Arbre and Graines).
*/
qx.Class.define("prems.Lot", {
  extend : qx.core.Object,
  events : {
    /**
    * Fired when the lot is saved in the database.
    */
    "saved" : "qx.event.type.Event",

    /**
    * Fired when the lot is retrieved from the database.
    */
    "getEnd" : "qx.event.type.Data"
  },
  properties : {
    /**
    * id
    */
    id : {
      init : null,
      nullable : true,
      check : "Integer",
      event : "changeId"
    },

    /**
    * Type
    */
    type : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeType"
    },

    /**
    * Numéro
    */
    numero : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeNumero"
    },

    /**
    * Numéro auto
    */
    numeroAuto : {
      init : null,
      nullable : true,
      check : "Boolean",
      event : "changeNumeroAuto"
    },

    /**
    * Clone
    */
    clone : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeClone"
    },

    /**
    * idClone
    */
    idClone : {
      init : null,
      nullable : true,
      check : "Integer",
      event : "changeIdClone"
    },

    /**
    * idGroupe
    */
    idGroupe : {
      init : null,
      nullable : true,
      check : "Integer",
      event : "changeIdGroupe"
    },

    /**
    * Quantité
    */
    quantite : {
      init : null,
      nullable : true,
      //check : "Double",
      event : "changeQuantite"
    },

    /**
    * Rang evaluation
    */
    rangEvaluation : {
      init : null,
      nullable : true,
      check : "Integer",
      event : "changeRangEvaluation"
    },

    /**
    * Equilibre
    */
    equilibre : {
      init : null,
      nullable : true,
      check : "Boolean",
      event : "changeEquilibre"
    },

    /**
    * Date récolte
    */
    dateRecolte : {
      init : null,
      nullable : true,
      check : "Date",
      event : "changeDateRecolte"
    },

    /**
    * Emplacement
    */
    emplacement : {
      init : new prems.Emplacement(),
      check : "prems.Emplacement",
      event : "changeEmplacement"
    },

    /**
    * Multiplication
    */
    multiplication : {
      init : null,
      nullable : true,
      check : "prems.Multiplication",
      event : "changeMultiplication"
    },

    /**
    * notations
    */
    notations : {
      init : new qx.data.Array(),
      check : "Array",
      event : "changeNotations"
    },

    /**
    * clone object
    */
    cloneObj : {
      init : new prems.Clone(),
      check : "prems.Clone",
      event : "changeCloneObj"
    }
  },

  /**
  * constructor
  * @param idLot {Integer} id of the lot if it exists in the database.
  * @param type {String} type lot : Arbre, Graines
  */
  construct : function(idLot, type) {
    this.base(arguments);
    this.setEmplacement(new prems.Emplacement());
    this.setNotations(new qx.data.Array());

    //Service
    this.__service = prems.service.Service.getInstance();
    try {
      this.setType(type);
    } catch (error) {
    }

    //Sets the variete id
    if (idLot) {
      this.setId(idLot);
      this.get();
    }
  },
  members :
  {
    /**
    * Saves (update or create)
    */
    save : function() {
      if (this.getId() === null) {
        this.getNumeroLot();
      } else {
        this.update();
      }
    },

    /**
    * Get numero lot
    */
    getNumeroLot : function() {
      var session = qxelvis.session.service.Session.getInstance();
      var userGroups = session.getUserGroupsInfo().getReadGroupIds();

      var service = this.__service.query("getLotByNumeroClone", [this.getClone(), "=", userGroups]);
      service.addListenerOnce("changeResponseModel", function() {
        var numeroLot = this.getClone() + "-" + parseInt(service.getResult().length + 1);
        if (this.getNumeroAuto()) {
          this.setNumero(numeroLot);
        }
        this.create();
      }, this);
    },

    /**
    * Get
    */
    get : function() {
      let service = null;
      if (this.getType() == "arbre") {
        service = this.__service.query("getArbre", [this.getId()]);
      }
      if (this.getType() == "graines") {
        service = this.__service.query("getGraines", [this.getId()]);
      }
      service.addListenerOnce("changeResponseModel", function() {
        this.setId(service.getResult()[0]["id_lot"]);
        if (this.getType() == "graines") {
          this.setEquilibre(service.getResult()[0]["equilibre"]);
          this.setDateRecolte(service.getResult()[0]["date_recolte"]);
          this.setQuantite(service.getResult()[0]["quantite"]);
          this.setRangEvaluation(service.getResult()[0]["rang_evaluation"]);
        }
        this.setNumero(service.getResult()[0]["numero_lot"]);
        var serviceGroupe = this.__service.query("getGroupeByIdLot", [this.getId()]);
        serviceGroupe.addListenerOnce("changeResponseModel", function() {
          this.setIdGroupe(serviceGroupe.getResult()[0]["id_group"]);
          this.getCloneInfos(service.getResult()[0]["clone"]);
        }, this);
      }, this);
    },

    /**
    * gets the multiplication informations
    */
    getMultiplicationInfos : function() {
      var service = this.__service.query("getMultiplicationByLotDesc", [this.getId()]);
      service.addListenerOnce("changeResponseModel", function() {
        if (service.getResult().length > 0) {
          var multiplication = new prems.Multiplication(service.getResult()[0]["id"]);
          multiplication.addListenerOnce("getEnd", function() {
            this.setMultiplication(multiplication);
            this.getNotationsInfos();
          }, this);
        } else {
          this.getNotationsInfos();
        }
      }, this);
    },

    /**
    * gets the clone informations
    * @param idClone {Integer} id clone
    */
    getCloneInfos : function(idClone) {
      var service = this.__service.query("getCloneById", [idClone]);
      service.addListenerOnce("changeResponseModel", function() {
        this.setClone(service.getResult()[0]["numero"]);
        this.setIdClone(service.getResult()[0]["id_clone"]);
        this.getEmplacementInfos();
      }, this);
    },

    /**
    * Gets the emplacement
    */
    getEmplacementInfos : function() {
      //Emplacement
      var serviceEmpl = this.__service.query("getEmplacementByLot", [this.getId()]);
      serviceEmpl.addListenerOnce("changeResponseModel", function() {
        try {
          var idEmpl = serviceEmpl.getResult()[0]["id_emplacement"];
          var emplacement = new prems.Emplacement(idEmpl);
          emplacement.addListenerOnce("getEnd", function() {
            this.setEmplacement(emplacement);
            this.getMultiplicationInfos();
          }, this);
        } catch (e) {
          this.getMultiplicationInfos();
        }
      }, this);
    },

    /**
    * Return the notations of the lot.
    */
    getNotationsInfos : function() {
      //Notations
      var serviceNotation = this.__service.query("getNotationsByLot", [this.getId()]);
      serviceNotation.addListenerOnce("changeResponseModel", function() {
        var result = serviceNotation.getResult();
        if (result.length == 0) {
          this.getCloneObject();
        }
        var notationArray = new qx.data.Array();
        var cpt = 0;
        for (var i in result) {
          notationArray.push(new prems.Notation(result[i]["id_notation"]));
          notationArray.getItem(i).addListenerOnce("getEnd", function() {
            cpt++;
            if (cpt == result.length) {
              this.setNotations(notationArray);
              this.getCloneObject();
            }
          }, this);
        }
      }, this);
    },

    /**
    * Create
    */
    create : function() {
      var serviceClone = this.__service.query("getCloneByNumero3", [this.getClone()]);
      serviceClone.addListenerOnce("changeResponseModel", function() {
        var idClone = serviceClone.getResult()[0]["id_clone"];
        let service = null;
        if (this.getType() == "arbre") {
          service = this.__service.query("createArbre", [idClone, this.getNumero()]);
        }
        if (this.getType() == "graines") {
          service = this.__service.query("createGraines", [idClone, this.getNumero(), this.getEquilibre(), this.getDateRecolte(), this.getQuantite(), this.getRangEvaluation()]);
        }
        service.addListenerOnce("changeResponseModel", function() {
          this.setId(service.getResult()[0]["id_lot"]);
          this.getEmplacement().setLot(this.getId());
          this.getEmplacement().save();
          this.getEmplacement().addListenerOnce("saved", function() {
            this.createGroupe();
          }, this);
        }, this);
      }, this);
    },

    /**
    * Update
    */
    update : function() {
      var serviceClone = this.__service.query("getCloneByNumero3", [this.getClone()]);
      serviceClone.addListenerOnce("changeResponseModel", function() {
        var idClone = serviceClone.getResult()[0]["id_clone"];
        let service = null;
        if (this.getType() == "arbre") {
          service = this.__service.query("updateArbre", [this.getId(), idClone, this.getNumero()]);
        }
        if (this.getType() == "graines") {
          service = this.__service.query("updateGraines", [this.getId(), idClone, this.getNumero(), this.getEquilibre(), this.getDateRecolte(), this.getQuantite(), this.getRangEvaluation()]);
        }
        service.addListenerOnce("changeResponseModel", function() {
          this.getEmplacement().save();
          this.getEmplacement().addListenerOnce("saved", function() {
            this.saveMultiplication();
          }, this);
        }, this);
      }, this);
    },

    /**
    * Creates groups and ghgroup link
    */
    createGroupe : function() {
      var session = qxelvis.session.service.Session.getInstance();
      var writeGroups = session.getUserGroupsInfo().getWriteGroupIds();
      var serviceGroup = this.__service.query("createGroupe", [this.getId(), "idGhGroup=" + qx.lang.Json.stringify(writeGroups)]);
      serviceGroup.addListenerOnce("changeResponseModel", function() {
        var idGroup = serviceGroup.getResult()[0]["id_group"];
        this.setIdGroupe(idGroup);
        var session = qxelvis.session.service.Session.getInstance();
        var listeIdGhGroup = session.getUserGroupsInfo().getWriteGroupIds();
        var serviceGhGroup = this.__service.query("setGhGroup", [listeIdGhGroup, idGroup]);
        serviceGhGroup.addListenerOnce("changeResponseModel", function() {
          this.saveMultiplication();
        }, this);
      }, this);
    },

    /**
    * Save multiplication
    */
    saveMultiplication : function() {
      if (this.getMultiplication() !== null) {
        this.getMultiplication().setLotDescendant(this.getNumero());
        this.getMultiplication().save();
        this.getMultiplication().addListenerOnce("saved", function() {
          this.saveNotations();
        }, this);
      } else {
        var service = this.__service.query("deleteMultiplicationByLotDesc", [this.getId()]);
        service.addListener("changeResponseModel", function() {
          this.saveNotations();
        }, this);
      }
    },

    /**
    * Save notations
    */
    saveNotations : function() {
      var cpt = 0;
      if (this.getNotations().getLength() != 0) {
        this.getNotations().forEach(function(notation) {
          notation.setIdGroup(this.getIdGroupe());
          notation.save();
          notation.addListenerOnce("saved", function() {
            cpt++;
            if (cpt == this.getNotations().length) {
              this.fireEvent("saved");
            }
          }, this);
        }, this);
      } else {
        this.fireEvent("saved");
      }
    },

    /**
    * Add a lieu
    * @param lieu {prems.Lieu} lieu
    */
    addLieu : function(lieu) {
      this.getEmplacement().addLieu(lieu);
    },

    /**
    * Return the object to be shown in a table.
    * @return {Object} row to show in the tables.
    */
    getRow : function() {
      return {
        "id" : this.getId(),
        "type" : this.getType(),
        "numero" : this.getNumero(),
        "hash" : this.toHashCode()
      };
    },

    /**
    * Add a notation.
    * @param notation {prems.Notation} notation
    */
    addNotation : function(notation) {
      var notations = this.getNotations();
      notations.push(notation);
      this.setNotations(notations);
      this.fireDataEvent("changeNotations");
    },

    /**
    * Get the clone obj.
    */
    getCloneObject : function() {
      var clone = new prems.Clone(this.getIdClone());
      this.setCloneObj(clone);
      clone.addListener("getEnd", function() {
        this.fireDataEvent("getEnd", this);
      }, this);
    },

    /**
    * Return the object
    * @return {Object} object to write in the files
    */
    toCsv : function() {
      var data = this.getCloneObj().toCsv();
      data["id lot"] = this.getId();
      data["Numéro lot"] = this.getNumero();
      data["Type lot"] = this.getType();
      if (this.getType() == "graines") {
        data["Quantité"] = this.getQuantite();
        data["Date récolte"] = this.getDateRecolte();
        data["Rang évaluation"] = this.getRangEvaluation();
        data["Equilibre"] = this.getEquilibre();
      }
      var dataEmplacement = this.getEmplacement().toCsv();
      for (let key in dataEmplacement) {
        data[key] = dataEmplacement[key];
      }
      this.getNotations().forEach(function(notation) {
        var colName = "Notation:" + notation.getTypeNotation();
        data[colName] = notation.getValeur();
      }, this);
      return data;
    }
  }
});
