/**
* Insert notations.
*/
qx.Class.define("prems.InsertNotations", {
  extend : qx.ui.core.Widget,
  events : {
    /**
    * End of validation.
    */
    "validateEnd" : "qx.event.type.Data",

    /**
    * Fired when the insertion is complete.
    */
    "end" : "qx.event.type.Data",

    /**
    * Fired when a line is saved.
    */
    "lineSaved" : "qx.event.type.Data",

    /**
    * Fired when all the datas are retrieved from the database.
    */
    "getDataEnd" : "qx.event.type.Data"
  },

  //constructeur
  construct : function() {
    this.base(arguments);

    this.__typeNotations = {};
    this.__numeroLots = [];
    this.__cols = [];
    this.__lines = [];

    //Service
    this.__service = prems.service.Service.getInstance();

    //Containers
    this.__container = new qx.ui.container.Composite(new qx.ui.layout.VBox(5));
    this._setLayout(new qx.ui.layout.Grow());
    this._add(this.__container);

    //Fenêtre choix du fichier
    var fileChooser = new qxfileio.FileChooser();

    //Button ouvrir fichier
    this.__fileButton = new qx.ui.form.Button("Ouvrir fichier...");
    this.__container.add(this.__fileButton);

    //Affichage nom du fichier
    var labelFile = new qx.ui.basic.Label("");
    this.__container.add(labelFile);

    //Bouton insérer
    this.__insertButton = new qx.ui.form.Button("Insérer");
    this.__container.add(this.__insertButton);

    //Boite affichage des messages d'erreurs
    this.__textArea = new qx.ui.form.TextArea();
    this.__container.add(this.__textArea);
    this.__textArea.setHeight(300);
    this.__textArea.setWidth(300);
    this.__textArea.setReadOnly(true);

    //Loading gif
    var loading = new qx.ui.basic.Image("prems/loading.gif");
    loading.setAlignX("center");
    this.__container.add(loading);
    loading.exclude();

    //Bouton pour télécharger le fichier exemple
    var helpButton = new qx.ui.form.Button(this.tr("Aide"));
    this.__container.add(helpButton);
    helpButton.addListener("execute", function() {
      var testFile = new prems.ImportFileExample();
      var fileWriter = new qxfileio.FileWriter();
      fileWriter.saveTextAsFile(testFile.getNotationFile(), "Import_format_plantDb.csv");
    }, this);

    //Fin de l'import du fichier'
    this.addListener("end", function(e) {
      this.__fileButton.setEnabled(true);
      this.__insertButton.setEnabled(true);
      loading.exclude();
      this.__textArea.show();
      this.__textArea.setValue(e.getData());
    }, this);
    var fr = new qxfileio.FileReader();

    //Ouverture de la fenêtre de choix de fichier
    fileChooser.addListener("filesChange", function(e) {
      if (e.getData().length > 0) {
        this.__file = e.getData()[0];
        labelFile.setValue(e.getData()[0]["name"]);
      }
    }, this);

    //Bouton insérer
    this.__insertButton.addListener("execute", function() {
      this.__fileButton.setEnabled(false);
      this.__insertButton.setEnabled(false);
      fr.loadAsText(this.__file);
    }, this);

    //Bouton choix de fichier
    this.__fileButton.addListener("execute", function() {
      fileChooser.open();
    }, this);

    //FileReader
    fr.addListener("load", function(e) {
      loading.show();
      this.__textArea.exclude();

      //code de fin de ligne pour les fichiers unix "\n" ou les fichiers windows "\r"
      if (e.getData().indexOf("\r")==-1) {
        var tabFile = e.getData().split("\n");
      } else {
        tabFile = e.getData().split("\r\n");
      }

      //Noms de colonnes
      var cols = tabFile[0].split("\t");

      //Données
      var lines = [];
      for (var i = 1; i < tabFile.length; i++) {
        var line = tabFile[i].split("\t");
        lines.push(line);
      }
      this.__lines = lines;
      this.__cols = cols;
      this.__getDbData();

      //this.__validate(cols, lines);
    }, this);
    this.addListener("getDataEnd", function() {
      this.__validate(this.__cols, this.__lines);
    }, this);
    this.addListener("validateEnd", function(e) {
      if (e.getData() == "") {
        this.__insertToDb(this.__cols, this.__lines);
      } else {
        this.fireDataEvent("end", e.getData());
      }
    });
  },
  members :
  {
    __typeNotations : null,
    __numeroLots : null,
    __cols : null,
    __lines : null,

    /**
    * Validate the file.
    * @param cols {Array} columns
    * @param lines {Array} lines
    */
    __validate : function(cols, lines) {
      var errorMsg = "";
      var listColumn = ["Numéro lot", "Remarque notation", "Date notation", "Notateur"];
      var numeroLotRequired = false;
      for (let i in cols) {
        //Test des notations
        if (cols[i].indexOf("Notation") > -1) {
          var typeNotation = cols[i].split(":")[1];
          if (Object.keys(this.__typeNotations).indexOf(typeNotation) < 0) {
            errorMsg += "Erreur type notation : " + typeNotation + "\n";
          }
        }
        if (cols[i] == "Numéro lot") {
          numeroLotRequired = true;
        } else if (cols[i].indexOf("Notation:") < 0) {
          if (listColumn.indexOf(cols[i]) < 0) {
            errorMsg += "Nom de colonne \"" + cols[i] + "\"non reconnu.\n";
          }
        }
      }
      if (numeroLotRequired == false) {
        errorMsg += "Colonne \"Numéro lot\" obligatoire.";
      }
      for (let i in lines) {
        for (let j in lines[i]) {
          //Valeur de la cellule
          var cellValue = lines[i][j];
          if (cellValue != "") {
            //Test des numeros de lots
            if (cols[j] == "Numéro lot") {
              if (this.__numeroLots.indexOf(cellValue) < 0) {
                errorMsg += ("Erreur numéro lot : " + cellValue + "\n");
              }
            }

            //Test des dates
            if ((cols[j] == "Date notation")) {
              if (!prems.DataTypeValidator.isDate(cellValue)) {
                errorMsg += ("Mauvais format de la date :" + cellValue + "\n");
              }
            }

            //Test des valeurs des notations
            if (cols[j].indexOf("Notation") > -1) {
              var type_notation = cols[j].split(":")[1];
              var type_data_notation = this.__typeNotations[type_notation];
              var valeur = cellValue;
              if (type_data_notation == "double") {
                //Si ce n'est pas un double
                if (!prems.DataTypeValidator.isFloat(valeur)) {
                  errorMsg += ("Mauvais format de la valeur :" + valeur + " (Nombre attendu).\n");
                }
              }
              if (type_data_notation == "date") {
                if (!prems.DataTypeValidator.isDate(cellValue)) {
                  errorMsg += ("Mauvais format de la valeur :" + valeur + "(Date attendue (dd/MM/YYYY)).\n");
                }
              }
              if (type_data_notation == "integer") {
                if (!prems.DataTypeValidator.isInteger(valeur)) {
                  errorMsg += ("Mauvais format de la valeur :" + valeur + " (Nombre entier attendu).\n");
                }
              }
            }
          }
        }
      }
      this.fireDataEvent("validateEnd", errorMsg);
    },

    /**
    * Validate the file.
    * @param cols {Array} columns
    * @param lines {Array} lines
    */
    __insertToDb : function(cols, lines) {
      this.__notations = [];

      //Lignes
      for (let i in lines) {
        //Colonnes
        for (let j in lines[i]) {
          let cellValue = lines[i][j];

          //Numéro lot
          let numeroLot = null;
          if (cols[j] == "Numéro lot") {
            numeroLot = cellValue;
          }

          //Notations
          if (cols[j].indexOf("Notation:") != -1) {
            var notation = new prems.Notation();
            notation.setLot(numeroLot);
            var typeNotation = cols[j].split(":")[1];
            var valeur = cellValue;
            notation.setTypeNotation(typeNotation);
            notation.setValeur(valeur);
            this.__notations.push(notation);
          }
          if (cols[j] == "Date notation") {
            var parts = cellValue.split("/");
            var dt = new Date(parseInt(parts[2], 10), parseInt(parts[1], 10) - 1, parseInt(parts[0], 10)+1);
            notation.setDate(dt);
          }
          if (cols[j] == "Notateur") {
            notation.setNotateur(cellValue);
          }
          if (cols[j] == "Remarque notation") {
            notation.setRemarque(cellValue);
          }
        }
      }

      //Barre de progression de l'import.
      this.__pb = new qx.ui.indicator.ProgressBar(0, this.__notations.length);
      this.__container.addAt(this.__pb, 3);
      this.__pb.addListener("complete", function(e) {
        this.__pb.exclude();
      }, this);
      for (var i in this.__notations) {
        this.__saveToDb(this.__notations[i]);
      }
      this.addListener("lineSaved", function() {
        this.__pb.setValue(parseInt(this.__pb.getValue()) + 1);
      }, this);
      this.__pb.addListener("complete", function() {
        this.fireDataEvent("end", null);
        this.__textArea.setValue("Import effectué");
      }, this);
    },

    /**
    * Get database data (variétés, lots, accessions)
    */
    __getDbData : function() {
      var session = qxelvis.session.service.Session.getInstance();
      var userGroups = session.getUserGroupsInfo().getReadGroupIds();

      //Récupère tous les numéros de lots
      var serviceLot = this.__service.query("getAllLot", [userGroups]);
      serviceLot.addListener("changeResponseModel", function() {
        for (var i in serviceLot.getResult()) {
          this.__numeroLots.push(serviceLot.getResult()[i]["numero_lot"]);
        }

        //Récupère tous les types de notations

        //Type notations
        var serviceTypeNotations = this.__service.query("getAllTypeNotation", []);
        serviceTypeNotations.addListener("changeResponseModel", function() {
          for (var i in serviceTypeNotations.getResult()) {
            this.__typeNotations[serviceTypeNotations.getResult()[i]["nom"]] = serviceTypeNotations.getResult()[i]["type"];
          }
          this.fireDataEvent("getDataEnd", this);
        }, this);
      }, this);
    },

    /**
    * Save to database.
    *@param notation {prems.Notation} notation.
    */
    __saveToDb : function(notation) {
      if (notation.getValeur().length > 0) {
        notation.save();
        notation.addListener("saved", function() {
          this.fireDataEvent("lineSaved", this);
        }, this);
      } else {
        this.fireDataEvent("lineSaved", this);
      }
    }
  }
});
