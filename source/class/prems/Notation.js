/**
* Notation object
*/
qx.Class.define("prems.Notation", {
  extend : qx.core.Object,
  events :
  {
    /**
    * Fired when the notation is saved in the database
    */
    "saved" : "qx.event.type.Event",

    /**
    * Fired when the notation is retrieved from the database.
    */
    "getEnd" : "qx.event.type.Event"
  },
  properties : {
    /**
    * id
    */
    id : {
      init : null,
      nullable : true,
      check : "Integer",
      event : "changeId"
    },

    /**
    * lot
    */
    lot : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeLot"
    },

    /**
    * date
    */
    date : {
      init : null,
      nullable : true,
      event : "changeDate"
    },

    /**
    * remarque
    */
    remarque : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeRemarque"
    },

    /**
    * site
    */
    site : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeSite"
    },

    /**
    * type notation
    */
    typeNotation : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeTypeNotation"
    },

    /**
    * type
    */
    type : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeType"
    },

    /**
    * notateur
    */
    notateur : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeNotateur"
    },

    /**
    * valeur
    */
    valeur : {
      init : null,
      nullable : true,
      event : "changeValeur"
    },

    /**
    * id groupe
    */
    idGroupe : {
      init : null,
      nullable : true,
      check : "Integer",
      event : "changeIdGroupe"
    },

    /**
    * id variete
    */
    idVariete : {
      init : null,
      nullable : true,
      check : "Integer",
      event : "changeIdVariete"
    },

    /**
    * id group
    */
    idGroup : {
      init : null,
      nullable : true,
      check : "Integer",
      event : "changeIdGroup"
    },

    /**
    * toDelete
    */
    toDelete : {
      init : false,
      check : "Boolean",
      event : "changeToDelete"
    }
  },
  construct : function(idNotation) {
    this.base(arguments);

    //Service
    this.__service = prems.service.Service.getInstance();

    //Sets the variete id
    if (idNotation) {
      this.setId(idNotation);
      this.get();
    }
  },

  members : {
    /**
    * Saves (update or create)
    */
    save : function() {
      if (this.getToDelete() == true) {
        this.remove();
      } else {
        var serviceSite = this.__service.query("getSite", [this.getSite()]);
        serviceSite.addListener("changeResponseModel", function() {
          try {
            var idSite = serviceSite.getResult()[0]["id_site"];
          } catch (e) {
            idSite = null;
          }
          if (this.getId() === null) {
            this.create(idSite);
          } else {
            this.update(idSite);
          }
        }, this);
      }
    },

    /**
    * Get
    */
    get : function() {
      var service = this.__service.query("getNotationById", [this.getId()]);
      service.addListener("changeResponseModel", function() {
        var result = service.getResult();
        this.setTypeNotation(result[0]["type_notation"]);
        this.setType(result[0]["type"]);
        this.setDate(result[0]["date_notation"]);
        this.setNotateur(result[0]["notateur"]);
        this.setSite(result[0]["nom_site"]);
        this.setValeur(result[0]["valeur"]);
        this.fireEvent("getEnd");
      }, this);
    },

    /**
    * Create
    * @param idSite {Integer} id site
    */
    create : function(idSite) {
      var service = this.__service.query("createNotation", [this.getDate(), this.getRemarque(), idSite, this.getTypeNotation(), this.getValeur(), this.getNotateur()]);
      service.addListener("changeResponseModel", function() {
        this.setId(service.getResult()[0]["id_notation"]);
        this.createGroupANotation();
      }, this);
    },

    /**
    * Remove
    */
    remove : function() {
      var service = this.__service.query("deleteNotation", [this.getId()]);
      service.addListener("changeResponseModel", function() {
        this.fireEvent("saved");
      }, this);
    },

    /**
    * Update
    * @param idSite {Integer} id site
    */
    update : function(idSite) {
      var service = this.__service.query("updateNotation", [this.getId(), this.getDate(), this.getRemarque(), idSite, this.getTypeNotation(), this.getValeur(), this.getNotateur()]);
      service.addListener("changeResponseModel", function() {
        this.fireEvent("saved");
      }, this);
    },

    /**
    * Create group a notation
    */
    createGroupANotation : function() {
      if (this.getLot() === null) {
        var service = this.__service.query("setGroupeANotation", [this.getIdGroup(), this.getId()]);
        service.addListener("changeResponseModel", function() {
          this.fireEvent("saved");
        }, this);
      } else {
        var serviceGroupe = this.__service.query("getGroupeByLot", [this.getLot()]);
        serviceGroupe.addListener("changeResponseModel", function(e) {
          this.setIdGroup(serviceGroupe.getResult()[0]["id_group"]);
          var service = this.__service.query("setGroupeANotation", [this.getIdGroup(), this.getId()]);
          service.addListener("changeResponseModel", function() {
            this.fireEvent("saved");
          }, this);
        }, this);
      }
    },

    /**
    * Create group a notation
    * @return {Object}  object to show in the tables.
    */
    getRow : function() {
      return {
        "id" : this.getId(),
        "type_notation" : this.getTypeNotation(),
        "valeur" : this.getValeur(),
        "date" : this.getDate(),
        "notateur" : this.getNotateur(),
        "site" : this.getSite(),
        "hash" : this.toHashCode()
      };
    }
  }
});
