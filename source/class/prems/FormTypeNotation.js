/**
 * Type notation form
*/
qx.Class.define("prems.FormTypeNotation",
{
  extend : qx.ui.core.Widget,
  events : {
    /**
        * Fired when the type notation is saved.
        */
    "saved" : "qx.event.type.Event"
  },
  construct : function() {
    this.base(arguments);

    //Layouts
    this.__container = new qx.ui.container.Composite(new qx.ui.layout.VBox(20));
    this._setLayout(new qx.ui.layout.Grow());
    this._add(this.__container);

    //Service
    this.__service = prems.service.Service.getInstance();
    var form = new qx.ui.form.Form();
    this.__typeTf = new qx.ui.form.TextField();
    form.add(this.__typeTf, this.tr("Nom"), null, "nom");
    this.__typeSb = new qx.ui.form.SelectBox();
    this.__typeSb.add(new qx.ui.form.ListItem("texte"));
    this.__typeSb.add(new qx.ui.form.ListItem("integer"));
    this.__typeSb.add(new qx.ui.form.ListItem("double"));
    this.__typeSb.add(new qx.ui.form.ListItem("date"));
    form.add(this.__typeSb, this.tr("Type notation"), null, "type");
    var saveButton = new qx.ui.form.Button(this.tr("Enregistrer"));
    form.addButton(saveButton);
    saveButton.addListener("execute", function() {
      if (this.__validate()) {
        this.__save();
      }
    }, this);
    this.__container.add(new qx.ui.form.renderer.Single(form));
  },
  members :
  {
    /**
       * Check if a field is empty.
       *@param str {String} string to check.
       *@return {Boolean} whether the string is empty or not.
       */
    isEmpty : function(str) {
      return (!str || str.length === 0);
    },

    /**
    * Validate.
    *@return {Boolean} wether the form is valid or not.
    */
    __validate : function() {
      var returnValue = true;
      this.__typeTf.setValid(true);
      if (this.isEmpty(this.__typeTf.getValue())) {
        this.__typeTf.setValid(false);
        returnValue = false;
      }
      return returnValue;
    },

    /**
           * Save.
           */
    __save : function() {
      let nom = this.__typeTf.getValue();
      let type_notation = this.__typeSb.getSelection()[0].getLabel();
      let service = null;
      if (!this.__idTypeNotation) {
        service = this.__service.query("createTypeNotation", [nom, type_notation]);
      } else {
        service = this.__service.query("updateTypeNotation", [nom, this.__idTypeNotation]);
      }
      service.addListener("changeResponseModel", function() {
        if (service.noError()) {
          this.fireEvent("saved");
          this.__service.fireEvent("dbChange");
        } else {
          new prems.Dialog("Erreur"); // TODO To be replaced by event
        }
      }, this);
    },

    /**
           * Set id type notation.
           *@param idTypeNotation {Integer} id type notation.
           */
    setId : function(idTypeNotation) {
      this.__idTypeNotation = idTypeNotation;
      if (this.__idTypeNotation !== null) {
        var service = this.__service.query("getTypeNotationById", [this.__idTypeNotation]);
        service.addListener("changeResponseModel", function() {
          this.__typeTf.setValue(service.getResult()[0]["nom"]);
          this.__typeSb.exclude();
        }, this);
      }
    }
  }
});
