/**
 * Dialog to display messages
*/
qx.Class.define("prems.Dialog",
{
  extend : qx.ui.core.Widget,

  /**
  * Display a dialog with a message
  * @param message {String} Message to display.
  */
  construct : function(message) {
    this.base(arguments);

    //Window
    var dialog = new qx.ui.window.Window();
    dialog.setLayout(new qx.ui.layout.VBox(20));

    //Label
    var label = new qx.ui.basic.Label(message);
    dialog.add(label);

    //Validate button
    var buttonValidation = new qx.ui.form.Button(this.tr("ok"));
    buttonValidation.setAllowStretchX(false);
    buttonValidation.setAlignX("right");
    dialog.add(buttonValidation);
    dialog.open();
    dialog.center();

    //Listener validation
    buttonValidation.addListener("execute", function() {
      dialog.close();
    });
  }
});
