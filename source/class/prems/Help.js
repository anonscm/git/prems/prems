/**
 * help page
*/
qx.Class.define("prems.Help",
{
  extend : qx.ui.core.Widget,
  construct : function() {
    this.base(arguments);
    var scrollContainer = new qx.ui.container.Scroll();
    scrollContainer.setWidth(750);
    scrollContainer.setHeight(500);
    this._setLayout(new qx.ui.layout.Grow());
    this._add(scrollContainer);

    var mainContainer = new qx.ui.container.Composite(new qx.ui.layout.VBox(5));
    scrollContainer.add(mainContainer);

    //Introduction
    var texte1 = this.tr("Cette fenêtre permet d'effectuer des recherches multi-critères sur la base de données. Les paramètres de recherche sont les suivants :");
    this.__addText(texte1, mainContainer);

    //
    var titre1 = this.tr("<b>Choix des critères de recherche</b>");
    this.__addTitle(titre1, mainContainer);
    var texte2 = this.tr("<b>Nom de Variété</b> : la sélection se fait sur les noms des variétés ou lignées.");
    this.__addText(texte2, mainContainer);
    var texte3 = this.tr("<b>Nom d'Accession pour les plantes pérennes</b> : la sélection se fait sur les noms d'accession renseignés pour les plantes pérennes.");
    this.__addText(texte3, mainContainer);
    var texte4 = this.tr("<b>Nom du Lot</b> : la sélection se fait sur le nom du lot");
    this.__addText(texte4, mainContainer);
    var texte5 = this.tr("<b>Notation sur le lot</b> : la sélection est réalisée sur les valeurs de notations effectuées sur les lots.");
    this.__addText(texte5, mainContainer);
    var texte6 = this.tr("<b>Emplacement du lot</b> : la sélection est réalisée sur les valeurs des emplacements du lot.");
    this.__addText(texte6, mainContainer);

    var titre3 = this.tr("<b>Opérateurs de recherche</b>");
    this.__addTitle(titre3, mainContainer);
    var texte7 = this.tr("<b>égal</b> : la sélection est réalisée sur la valeur saisie.");
    this.__addText(texte7, mainContainer);
    var texte12 = this.tr("les valeurs numériques peuvent être précédées des opérateurs < > <= >=.");
    this.__addText(texte12, mainContainer);
    var texte13 = this.tr("les recherches de texte peuvent utiliser * pour remplacer tout carractère.");
    this.__addText(texte13, mainContainer);
    var texte9 = this.tr("<b>notation absente</b> : la sélection est réalisée pour les lots pour lesquels un type de notation n'est pas renseigné dans la base de données.");
    this.__addText(texte9, mainContainer);
    var texte10 = this.tr("<b>dans</b> : permet de rechercher dans une liste. Les valeurs doivent être séparées par un retour à la ligne.");
    this.__addText(texte10, mainContainer);
    var texte11 = this.tr("<b>nom proche</b> : la sélection est réalisée sur le nom de variété ou lignée, les noms approchés sont sélectionnés avec la méthode de Levenstein.");
    this.__addText(texte11, mainContainer);
    var titre4 = this.tr("<b>Format des dates</b>");
    this.__addTitle(titre4, mainContainer);
    var texte14 = this.tr("<b>date</b> : les dates doivent être sous le format dd/mm/yyyy par exemple 30/08/2015");
    this.__addText(texte14, mainContainer);
    var titre6 = this.tr("<b>Recherche avec plusieurs critères</b>");
    this.__addTitle(titre6, mainContainer);
    var texte18 = this.tr("Les recherches sont effectuées de façon indépendantes et agrégées avec le critère ET . Seules les recherches effectuées avec l'opérateur DANS sont effectuées avec le critère OU.");
    this.__addText(texte18, mainContainer);
    var titre5 = this.tr("<b>Recherche avec emplacement</b>");
    this.__addTitle(titre5, mainContainer);
    var texte15 = this.tr("Si la case Recherche avec emplacement est activée, les recherches sont effectues avec l'emplacemebnt du lot.");
    this.__addText(texte15, mainContainer);
    var texte16 = this.tr("Dans le cas de recherche renseignant un grand nombre de lots cette option peut être lente et ne doit pas être sélectionnée.");
    this.__addText(texte16, mainContainer);
    var titre7 = this.tr("<b>Options d'affichage des resultats</b>");
    this.__addTitle(titre7, mainContainer);
    var texte17 = this.tr("La selection d'un type d'affichage modifie uniquement les valeurs affichées et ne relance pas une recherche.");
    this.__addText(texte17, mainContainer);
    },
  members :
  {
    /**
           * Add title.
           *@param text {String} text
           *@param container {qx.ui.container.Scroll} container
           */
    __addTitle : function(text, container) {
      var textLbl = new qx.ui.basic.Label(text).set(
      {
        font : new qx.bom.Font(15, ["Verdana", "sans-serif"]),
        rich : true
      });
      container.add(textLbl);
    },

    /**
           * Add text.
           *@param text {String} text
           *@param container {qx.ui.container.Scroll} container
           */
    __addText : function(text, container) {
      var textLbl = new qx.ui.basic.Label(text).set(
      {
        font : new qx.bom.Font(12, ["Verdana", "sans-serif"]),
        rich : true
      });
      container.add(textLbl);
    }
  }
});
