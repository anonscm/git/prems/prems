/**
 * Autocomplete Box
*/
qx.Class.define("prems.AutoCompleteBox",
{
  extend : qx.ui.form.VirtualComboBox,
  events : {

  },

  /**
  * Autocomplete Box
  * @param array {Array} Array containing the values to show and filter.
  */
  construct : function(array) {
    this.base(arguments);

    var delegate = {
        configureItem : function(item) {
          item.setRich(true);
        }
      };

    this.setDelegate(delegate);

    //Meta jour le modèle
    if (array) {
      this.setListModel(array);
    }

    //Recherche listener quand quelque chose est entré dans la zone de texte.
    this.getChildControl("textfield").addListener("input", function(e) {
      //Contient les valeurs qui matchent avec le texte entré.
      var filteredData = new qx.data.Array();
      this.__data.forEach(function(i) {
        if (i !== null) {
          //Ne pas prendre en compte la casse
          //Peut ajouter la non prise en compte des accents ?
          var index = i.toLowerCase().indexOf(e.getData().toLowerCase());
          if (index > -1) {
            filteredData.push(i.replace(e.getData(), "<font color=\"red\">"+e.getData()+"</font>"));
          }
        }
      }, this);

      //Met à jour le nouveau modèle
      var modelNew = qx.data.marshal.Json.createModel(filteredData);
      this.setModel(modelNew);

      //Règle la taille de la liste
      var dropdown = this.getChildControl("dropdown");
      var list = dropdown.getChildControl("list");
      list.setHeight(null);
      list.setMaxHeight(100);
      list.setMinHeight(0);

      //Si au moins une valeur matche on ouvre la liste déroulante
      if (filteredData.length > 0) {
        this.open();
      }
    }, this);

    //lorque l'on met le focus sur un autre widget

    //On ferme la liste déroulante
    this.addListener("focusout", function() {
      this.close();
    }, this);

    var dropdown = this.getChildControl("dropdown");

    //Recherche listener quand quelque chose est entré dans la zone de texte.
    dropdown.addListener("changeVisibility", function(e) {
      var textField = this.getChildControl("textfield");
      try {
        textField.setValue(textField.getValue().replace("<font color=\"red\">", ""));
        textField.setValue(textField.getValue().replace("</font>", ""));
        textField.setValue(textField.getValue().replace("<b>", ""));
        textField.setValue(textField.getValue().replace("</b>", ""));
      } catch (error) {

      }
    }, this);
  },
  members :
  {
    /**
    * Sets the model of the AutoCompleteBox.
    * @param array {Array} Array containing the values to show and filter.
    */
    setListModel : function(array) {
      this.__data = array;
      var model = qx.data.marshal.Json.createModel(array);
      this.setModel(model);
    },

    /**
    * Validates the AutoCompleteBox.
    * @param arg {Boolean} true if the field is required.
    * @param msg {String} Message to show if the field is invalid.
    * @return {Boolean} true if the value is valid.
    */
    validate : function(arg, msg) {
      //Teste si la valeur est correcte et accepte un champs vide.
      if (!arg) {
        if (this.__data.indexOf(this.getValue()) > -1 || this.getValue() === null || this.getValue() == "") {
          this.setValid(true);
          this.setInvalidMessage(msg);
          return true;
        }
      } else //Test si la valeur est correcte mais n'accepte pas un champs vide.
      if (this.__data.indexOf(this.getValue()) > -1) {
        this.setValid(true);
        return true;
      }

      //Si la valeur n'est pas correcte ou si le champs est vide et que arg=true

      //La validation retourne false.
      this.setValid(false);
      this.setInvalidMessage(msg);
      return false;
    }
  }
});
