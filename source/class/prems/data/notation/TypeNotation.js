/**
 * The prems.data.notation.TypeNotation
 * 14 Mai 2019
 * Fabrice Dupuis
 */
qx.Class.define("prems.data.notation.TypeNotation", {
  extend : qx.core.Object,
  implement : prems.data.notation.ITypeNotation,
  include: [prems.data.notation.MTypeNotation],
  construct : function(name = null, type = null, constraint = null, list = null) {
    this.base(arguments);
    this.setName(name);
    this.setType(type);
    this.setConstraint(constraint);
    this.setList(list);
  }
});
