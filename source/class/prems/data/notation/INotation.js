/**
 * The prems.data.notation.INotation interface
 */
qx.Interface.define("prems.data.notation.INotation", {
  properties : {
    id : {},
    value : {},
    date : {},
    comment : {},
    siteId : {},
    type : {},
    observer : {},
    idUserList : {},
    idProtocolList : {}
  }
});
