/**
 * The prems.data.notation.MContextNotation mixin
 */
qx.Mixin.define("prems.data.notation.MContextNotation", {
  properties : {
    name : {
      nullable : true,
      event : "changeName"
    },
    comment : {
      nullable : true,
      event : "changeConstraint"
    },

    listUserGroup : {
      nullable : true,
      event : "changeType"
    },
    listTypeNotation : {
      nullable : true,
      event : "changeList"
    }
  }
});
