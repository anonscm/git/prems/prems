/**
 * The prems.data.notation.ITypeNotation interface
 */
qx.Interface.define("prems.data.notation.ITypeNotation", {
  properties : {
    name : {},
    type : {},
    constraint : {},
    list : []
  }
});
