/**
 * The prems.data.notation.IContextNotation interface
 */
qx.Interface.define("prems.data.notation.IContextNotation", {
  properties : {
    name : {},
    comment : {},
    listTypeNotation : {}
  }
});
