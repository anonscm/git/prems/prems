/**
 * The prems.data.notation.ContextNotation
 * 14 Mai 2019
 * Fabrice Dupuis
 */
qx.Class.define("prems.data.notation.ContextNotation", {
  extend : qx.core.Object,
  implement : prems.data.notation.IContextNotation,
  include: [prems.data.notation.MContextNotation],
  construct : function(name = null, comment = null, listUserGroup = null, listTypeNotation = null) {
    this.base(arguments);
    this.setName(name);
    this.setComment(comment);
    this.setListUserGroup(listUserGroup);
    this.setListTypeNotation(listTypeNotation);
  }
});
