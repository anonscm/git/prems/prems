/**
 * The prems.data.notation.MTypeNotation mixin
 */
qx.Mixin.define("prems.data.notation.MTypeNotation", {
  properties : {
    name : {
      nullable : true,
      event : "changeName"
    },
    constraint : {
      nullable : true,
      event : "changeConstraint"
    },
    type : {
      nullable : true,
      event : "changeType"
    },
    list : {
      nullable : true,
      event : "changeList"
    }
  }
});
