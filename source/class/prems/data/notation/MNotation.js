/**
 * The prems.data.notation.MNotation mixin
 */
qx.Mixin.define("prems.data.notation.MNotation", {
  properties : {
    id : {
      nullable : true,
      event : "changeId"
    },
    value : {
      nullable : true,
      event : "changeValue"
    },
    date : {
      nullable : true,
      event : "changeDate"
    },
    comment : {
      nullable : true,
      event : "changeComment"
    },
    siteId : {
      nullable : true,
      event : "changeSiteId"
    },
    type : {
      nullable : true,
      event : "changeType"
    },
    observer : {
      nullable : true,
      event : "changeObserver"
    },
    idUserList : {
      nullable : true,
      event : "changeIdUserList"
    },
    idProtocolList : {
      nullable : true,
      event : "changeIdProtocolList"
    }

  }
});
