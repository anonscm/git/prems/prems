/**
 * The prems.data.notation.Notation
 */
qx.Class.define("prems.data.notation.Notation", {
  extend : qx.core.Object,
  implement : prems.data.notation.INotation,
  include: [prems.data.notation.MNotation],
  construct : function(
      value = null,
      type = null,
      date = null,
      id = null,
      observer = null,
      comment = null,
      siteId = null,
      idUserList = [],
      idProtocolList = []
    ) {
    this.base(arguments);
    this.setValue(value);
    this.setType(type);
    this.setDate(date);
    this.setId(id);
    this.setObserver(observer);
    this.setComment(comment);
    this.setSiteId(siteId);
    this.setIdUserList(idUserList);
    this.setIdProtocolList(idProtocolList);
  }
});
