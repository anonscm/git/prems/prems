/**
 * The prems.data.plant.MLot mixin
 */
qx.Mixin.define("prems.data.plant.MLot", {
  properties : {
    id : {
      nullable : true,
      event : "changeId"
    },
    type : {
      nullable : true,
      event : "changeType"
    },
    name : {
      nullable : true,
      event : "changeName"
    },
    accessionId : {
      nullable : true,
      event : "changeAccessionId"
    },
    lotSourceId : {
      nullable : true,
      event : "changeLotSourceId"
    },
    multiplicationDate : {
      nullable : true,
      event : "changeMultiplicationDate"
    },
    harvestingDate : {
      nullable : true,
      event : "changeHarvestingDate"
    },
    creationDate : {
      nullable : true,
      event : "changeCreationDate"
    },
    destructionDate : {
      nullable : true,
      event : "changeDestructionDate"
    },
    quantity : {
      nullable : true,
      event : "changeQuantity"
    },
    place : {
      nullable : true,
      event : "changePlace"
    }
  }
});
