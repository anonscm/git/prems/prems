/**
 * The prems.data.plant.Accession
 */
qx.Class.define("prems.data.plant.Taxon", {
  extend : qx.core.Object,
  implement : prems.data.plant.ITaxon,
  include: [prems.data.plant.MTaxon],
  construct : function(
      id = null,
      genus = null,
      specie = null,
      type = null,
      idType = null
    ) {
    this.base(arguments);
    this.setId(id);
    this.setGenus(genus);
    this.setSpecie(specie);
    this.setType(type);
    this.setIdType(idType);
  }
});
