/**
 * The prems.data.plant.Accession
 */
qx.Class.define("prems.data.plant.Accession", {
  extend : qx.core.Object,
  implement : prems.data.plant.IAccession,
  include: [prems.data.plant.MAccession],
  construct : function(
      id = null,
      introductionName = null,
      varietyId = null,
      comment = null,
      introductionCloneId = null,
      provider = null,
      providerId = null,
      introductionDate = null,
      collectionDate = null,
      collectionSiteId = null
    ) {
    this.base(arguments);
    this.setId(id);
    this.setComment(comment);
    this.setIntroductionName(introductionName);
    this.setVarietyId(varietyId);
    this.setIntroductionCloneId(introductionCloneId);
    this.setProviderId(providerId);
    this.setProvider(provider);
    this.setIntroductionDate(introductionDate);
    this.setCollectionDate(collectionDate);
    this.setCollectionSiteId(collectionSiteId);
  }
});
