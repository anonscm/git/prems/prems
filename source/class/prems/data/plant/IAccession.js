/**
 * The prems.data.plant.IAccession interface
 */
qx.Interface.define("prems.data.plant.IAccession", {
  properties : {
    id : {},
    introductionName : {},
    varietyId : {},
    comment : {},
    introductionCloneId : {},
    provider : {},
    providerId : {},
    introductionDate : {},
    collectionDate : {},
    collectionSiteId : {}
  }
});
