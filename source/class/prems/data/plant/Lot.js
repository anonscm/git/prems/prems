/**
 * The prems.data.plant.Lot
 */
qx.Class.define("prems.data.plant.Lot", {
  extend : qx.core.Object,
  implement : prems.data.plant.ILot,
  include: [prems.data.plant.MLot],
  construct : function(
      id = null,
      type = null,
      name = null,
      accessionId = null,
      lotSourceId = null,
      multiplicationDate = null,
      harvestingDate = null,
      creationDate = null,
      destructionDate = null,
      quantity = null,
      place = null
    ) {
    this.base(arguments);
    this.setId(id);
    this.setType(type);
    this.setName(name);
    this.setAccessionId(accessionId);
    this.setLotSourceId(lotSourceId);
    this.setMultiplicationDate(multiplicationDate);
    this.setHarvestingDate(harvestingDate);
    this.setCreationDate(creationDate);
    this.setDestructionDate(destructionDate);
    this.setQuantity(multiplicationDate);
    this.setPlace(place);    
  }

});
