/**
 * The prems.data.plant.IVarietyName interface
 */
qx.Interface.define("prems.data.plant.IVarietyName", {
  properties : {
    id : {},
    varietyId : {},
    value : {},
    date : {},
    varietyNameTypeId : {},
    type : {}
  }
});
