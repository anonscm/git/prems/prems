/**
 * The prems.data.plant.Place
 */
qx.Class.define("prems.data.plant.Place", {
  extend : qx.core.Object,
  implement : prems.data.plant.IPlace,
  include: [prems.data.plant.MPlace],
  construct : function(
      id = null,
      lotId = null,
      siteId = null,
      plantationDate = null,
      removingDate = null,
      locationList = []
    ) {
    this.base(arguments);
    this.setId(id);
    this.setLotId(lotId);
    this.setSiteId(siteId);
    this.setPlantationDate(plantationDate);
    this.setRemovingDate(removingDate);
    this.setLocationList(locationList);
  }
});
