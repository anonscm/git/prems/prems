/**
 * The prems.data.plant.VarietyName
 */
qx.Class.define("prems.data.plant.VarietyName", {
  extend : qx.core.Object,
  implement : prems.data.plant.IVarietyName,
  include: [prems.data.plant.MVarietyName],
  construct : function(
      id = null,
      varietyId = null,
      value = null,
      date = null,
      varietyNameTypeId = null,
      type = null
    ) {
    this.base(arguments);
    this.setId(id);
    this.setVarietyId(varietyId);
    this.setValue(value);
    this.setDate(date);
    this.setVarietyNameTypeId(varietyNameTypeId);
    this.setType(type);
    this.addListener("changeType", function(e) {
      var typeVariete = e.getData();
      //TODO trouver idType by type
    }, this);
  }
});
