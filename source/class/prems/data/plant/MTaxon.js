/**
 * The prems.data.plant.MAccession mixin
 */
qx.Mixin.define("prems.data.plant.MTaxon", {
  properties : {
    id : {
      nullable : true,
      event : "changeId"
    },
    genus : {
      nullable : true,
      event : "changeGenus"
    },
    specie : {
      nullable : true,
      event : "changeSpecie"
    },
    type : {
      nullable : true,
      event : "changeType"
    },
    idType : {
      nullable : true,
      event : "changeIdType"
    }
  }
});
