/**
 * The prems.data.plant.ILot interface
 */
qx.Interface.define("prems.data.plant.ILot", {
  properties : {
    id : {},
    type : {},
    name : {},
    accessionId : {},
    lotSourceId : {},
    multiplicationDate : {},
    harvestingDate : {},
    creationDate : {},
    destructionDate : {},
    quantity : {},
    place : {}
  }
});
