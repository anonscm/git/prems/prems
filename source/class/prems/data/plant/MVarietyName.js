/**
 * The prems.data.plant.MVarietyName mixin
 */
qx.Mixin.define("prems.data.plant.MVarietyName", {
  properties : {
    id : {
      nullable : true,
      event : "changeId"
    },
    varietyId : {
      nullable : true,
      event : "changeVarietyId"
    },
    value : {
      nullable : true,
      event : "changeValue"
    },
    date : {
      nullable : true,
      event : "changeDate"
    },
    varietyNameTypeId : {
      nullable : true,
      event : "changeVarietyNameTypeId"
    },
    type : {
      nullable : true,
      event : "changeType"
    }
  }
});
