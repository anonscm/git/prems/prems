/**
 * The prems.data.plant.IVariety interface
 */
qx.Interface.define("prems.data.plant.IVariety", {
  properties : {
    id : {},
    breeder : {},
    breederId : {},
    comment : {},
    taxonomyId : {},
    genus : {},
    specie : {},
    editor : {},
    editorId : {},
    listVarietyName : []
  }
});
