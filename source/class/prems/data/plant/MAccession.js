/**
 * The prems.data.plant.MAccession mixin
 */
qx.Mixin.define("prems.data.plant.MAccession", {
  properties : {
    id : {
      nullable : true,
      event : "changeId"
    },
    comment : {
      nullable : true,
      event : "changeComment"
    },
    introductionName : {
      nullable : true,
      event : "changeIntroductionName"
    },
    varietyId : {
      nullable : true,
      event : "changeVarietyId"
    },
    introductionCloneId : {
      nullable : true,
      event : "changeIntroductionCloneId"
    },
    provider : {
      nullable : true,
      event : "changeProvider"
    },
    providerId : {
      nullable : true,
      event : "changeProviderId"
    },
    introductionDate : {
      nullable : true,
      event : "changeIntroductionDate"
    },
    collectionDate : {
      nullable : true,
      event : "changeCollectionDate"
    },
    collectionSiteId : {
      nullable : true,
      event : "changeCollectionSiteId"
    }
  }
});
