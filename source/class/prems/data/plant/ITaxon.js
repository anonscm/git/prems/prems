/**
 * The prems.data.plant.IAccession interface
 */
qx.Interface.define("prems.data.plant.ITaxon", {
  properties : {
    id : {},
    genus : {},
    specie : {},
    type : {},
    idType : {}
  }
});
