/**
 * The prems.data.plant.MPlace mixin
 */
qx.Mixin.define("prems.data.plant.MPlace", {
  properties : {
    id : {
      nullable : true,
      event : "changeId"
    },
    lotId : {
      nullable : true,
      event : "changeLotId"
    },
    siteId : {
      nullable : true,
      event : "changeSiteId"
    },
    removingDate : {
      nullable : true,
      event : "changeRemovingDate"
    },
    plantationDate : {
      nullable : true,
      event : "changePlantationDate"
    },
    locationList : {
      nullable : true,
      event : "changeLocationList"
    }
  },
  members : {
    addLocation : function(type, value) {
      this.locationList.append({"type":type, "value":value});
    }
  }
});
