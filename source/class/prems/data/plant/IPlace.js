/**
 * The prems.data.plant.IPlace interface
 */
qx.Interface.define("prems.data.plant.IPlace", {
  properties : {
    id : {},
    lotId : {},
    siteId : {},
    plantationDate : {},
    removingDate : {},
    locationList : []
  }
});
