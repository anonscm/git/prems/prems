/**
 * The prems.data.plant.Variety
 */
qx.Class.define("prems.data.plant.Variety", {
  extend : qx.core.Object,
  implement : prems.data.plant.IVariety,
  include: [prems.data.plant.MVariety],
  construct : function(
    id = null,
    breeder = null,
    breederId = null,
    comment = null,
    taxonomyId = null,
    genus = null,
    specie = null,
    editor = null,
    editorId = null,
    listVarietyName = []
  ) {
    this.base(arguments);
    this.setId(id);
    this.setBreederId(breederId);
    this.setBreeder(breeder);
    this.setComment(comment);
    this.setTaxonomyId(taxonomyId);
    this.setGenus(genus);
    this.setSpecie(specie);
    this.setEditor(editor);
    this.setEditorId(editorId);
    this.setListVarietyName(listVarietyName);
  }
});
