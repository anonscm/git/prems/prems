/**
 * The prems.data.plant.MVariety mixin
 */
qx.Mixin.define("prems.data.plant.MVariety", {
  properties : {
    id : {
      nullable : true,
      event : "changeId"
    },
    breeder : {
      nullable : true,
      event : "changeBreeder"
    },
    breederId : {
      nullable : true,
      event : "changeBreederId"
    },
    comment : {
      nullable : true,
      event : "changeComment"
    },
    taxonomyId : {
      nullable : true,
      event : "changeTaxonomyId"
    },
    genus : {
      nullable : true,
      event : "changeGenus"
    },
    specie : {
      nullable : true,
      event : "changeSpecie"
    },
    editor : {
      nullable : true,
      event : "changeEditor"
    },
    editorId : {
      nullable : true,
      event : "changeEditorId"
    },
    listVarietyName : {
      nullable : true,
      init : [],
      event : "changeListVarietyName"
    }
  },
  members : {
    addNom : function(nom) {
      this.getListVarietyName().push(nom);
    }
  }
});
