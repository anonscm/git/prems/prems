/**
 * The prems.data.plant.MPerson mixin
 */
qx.Mixin.define("prems.data.addressBook.MPerson", {
  properties : {
    id : {
      nullable : true,
      event : "changeId"
    },
    firstname : {
      nullable : true,
      event : "changeFirstname"
    },
    lastname : {
      nullable : true,
      event : "changeLastname"
    },
    address1 : {
      nullable : true,
      event : "changeAddress1"
    },
    address2 : {
      nullable : true,
      event : "changeAddress2"
    },
    city : {
      nullable : true,
      event : "changeCity"
    },
    state : {
      nullable : true,
      event : "changeState"
    },
    postalcode : {
      nullable : true,
      event : "changePostalcode"
    },
    contryId : {
      nullable : true,
      event : "changeContryId"
    },
    email : {
      nullable : true,
      event : "changeEmail"
    },
    phone : {
      nullable : true,
      event : "changePhone"
    },
    fax : {
      nullable : true,
      event : "changeFax"
    }
  }
});
