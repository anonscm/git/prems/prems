/**
 * The prems.data.plant.IPerson interface
 */
qx.Interface.define("prems.data.addressBook.IPerson", {
  properties : {
    id : {},
    firstname : {},
    lastname : {},
    address1 : {},
    address2 : {},
    city : {},
    state : {},
    postalcode : {},
    contryId : {},
    email : {},
    phone : {},
    fax : {}
  }
});
