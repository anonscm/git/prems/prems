/**
 * The prems.data.Person
 */
qx.Class.define("prems.data.addressBook.Person", {
  extend : qx.core.Object,
  implement : prems.data.addressBook.IPerson,
  include: [prems.data.addressBook.MPerson],
  construct : function(
      id = null,
      firstname = null,
      lastname = null,
      address1 = null,
      address2 = null,
      city = null,
      state = null,
      postalcode = null,
      contryId = null,
      email = null,
      phone = null,
      fax = null
    ) {
    this.base(arguments);
    this.setId(id);
    this.setFirstname(firstname);
    this.setLastname(lastname);
    this.setAddress1(address1);
    this.setAddress2(address2);
    this.setCity(city);
    this.setState(state);
    this.setPostalcode(postalcode);
    this.setContryId(contryId);
    this.setEmail(email);
    this.setPhone(phone);
    this.setFax(fax);
  }
});
