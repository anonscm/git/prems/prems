/**
 * Native Object dumpable interface
 *
 * Class inheriting from this interface must proviede a fromNativeObject static method
 * returning a new instance of this Class initialized from a native JavaScript Object
 * with corresponding properties.
 */
qx.Interface.define("prems.data.IObjectDumpable", {
  statics : {
    fromNativeObject : function() {}
  }
});
