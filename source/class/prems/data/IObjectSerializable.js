/**
 * Native Object serializabe interface
 *
 * Class inheriting from this interface must proviede a toNativeObject method
 * returning a native JavaScript Object corresponding to the current Object instance.
 */
qx.Interface.define("prems.data.IObjectSerializable", {
  members : {
    toNativeObject : function() {}
  }
});
