/**
 * Experiment interface
 *
 * @author Sylvain Gaillard
 * @date 5 juillet 2019
 */
qx.Interface.define("prems.data.experiment.IExperiment", {
  properties : {
    id : {},
    name : {},
    creationDate : {},
    description : {},
    title : {},
    active : {},
    dateStart : {},
    dateEnd : {},
    code : {},
    idUser : {},
    idExperimentType : {}
  }
});
