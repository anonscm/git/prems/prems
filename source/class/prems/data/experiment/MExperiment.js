/**
 * Experiment mixin
 *
 * @author Sylvain Gaillard
 * @date 5 juillet 2019
 */
qx.Mixin.define("prems.data.experiment.MExperiment", {
  properties : {
    id : {
      nullable : true,
      check : "Integer",
      event : "changeId"
    },
    name : {
      nullable : true,
      check : "String",
      event : "changeName"
    },
    creationDate : {
      nullable : true,
      check : "Date",
      event : "changeCreationDate"
    },
    description : {
      nullable : true,
      check : "String",
      event : "changeDescription"
    },
    title : {
      nullable : true,
      check : "String",
      event : "changeTitle"
    },
    active : {
      nullable : true,
      check : "Boolean",
      event : "changeActive"
    },
    dateStart : {
      nullable : true,
      check : "Date",
      event : "changeDateStart"
    },
    dateEnd : {
      nullable : true,
      check : "Date",
      event : "changeDateEnd"
    },
    code : {
      nullable : true,
      check : "String",
      event : "changeCode"
    },
    idUser : {
      nullable : true,
      check : "Integer",
      event : "changeIdUser"
    },
    idExperimentType : {
      nullable : true,
      check : "Integer",
      event : "changeIdExperiement"
    }
  }
});
