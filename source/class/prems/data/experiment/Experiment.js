/**
 * Experiment
 *
 * @author Sylvain Gaillard
 * @date 5 juillet 2019
 */
qx.Class.define("prems.data.experiment.Experiment", {
  extend : qx.core.Object,
  implement : [
    prems.data.experiment.IExperiment,
    prems.data.IObjectSerializable
    // prems.data.IObjectDumpable
  ],
  include: [prems.data.experiment.MExperiment],
  construct : function(
      id = null,
      name = null,
      creationDate = null,
      description = null,
      title = null,
      active = null,
      dateStart = null,
      dateEnd = null,
      code = null,
      idUser = null,
      idExperimentType = null
    ) {
    this.base(arguments);
    this.setId(id);
    this.setName(name);
    this.setCreationDate(creationDate);
    this.setDescription(description);
    this.setTitle(title);
    this.setActive(active);
    this.setDateStart(dateStart);
    this.setDateEnd(dateEnd);
    this.setCode(code);
    this.setIdUser(idUser);
    this.setIdExperimentType(idExperimentType);
  },

  statics : {
    fromNativeObject : function(obj) {
      let exp = new prems.data.experiment.Experiment();
      let o = qx.lang.Object.clone(obj, true);
      if (o.id) {
        exp.setId(o.id);
      }
      if (o.name) {
        exp.setName(o.name);
      }
      if (o.creationDate) {
        exp.setCreationDate(o.creationDate);
      }
      if (o.description) {
        exp.setDescription(o.description);
      }
      if (o.title) {
        exp.setTitle(o.title);
      }
      if (o.active) {
        exp.setActive(o.active);
      }
      if (o.dateStart) {
        exp.setDateStart(o.dateStart);
      }
      if (o.dateEnd) {
        exp.setDateEnd(o.dateEnd);
      }
      if (o.code) {
        exp.setCode(o.code);
      }
      if (o.idUser) {
        exp.setIdUser(o.idUser);
      }
      if (o.idExperimentType) {
        exp.setIdExperimentType(o.idExperimentType);
      }
      return exp;
    }
  },

  members : {
    toNativeObject : function() {
      let obj = qx.util.Serializer.toNativeObject(this);
      return obj;
    }
  }
});
