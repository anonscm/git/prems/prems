/**
* Présentation des informationd d'un lot
* fabrice mars 2019
*/
qx.Class.define("prems.PresentationLot", {
  extend : qx.ui.core.Widget,
  properties : {
    id : {
      init : null,
      nullable : true,
      check : "Integer",
      event : "changeId"
    },
    cultivated : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeCultivated"
    },
    name : {
      init : null,
      check : "new qx.ui.basic.Label()",
      nullable : true,
      event : "changeName"
    },
    nameLotSource : {
      init : null,
      check : "new qx.ui.basic.Label()",
      nullable : true,
      event : "changeNameLotSource"
    },
    allPlaces : {
      init : null,
      check : "new qx.ui.basic.Label()",
      nullable : true,
      event : "changeAllPlaces"
    },
    dateMultiplication : {
      init : null,
      check : "new qx.ui.basic.Label()",
      nullable : true,
      event : "changeDateMultiplication"
    },
    dateRecolte : {
      init : null,
      check : "new qx.ui.basic.Label()",
      nullable : true,
      event : "changeDateRecolte"
    },
    quantite : {
      init : null,
      check : "new qx.ui.basic.Label()",
      nullable : true,
      event : "changeQuantite"
    },
    anneePremPousse : {
      init : null,
      check : "new qx.ui.basic.Label()",
      nullable : true,
      event : "changeAnneePremPousse"
    },
    porteGreffe : {
      init : null,
      check : "new qx.ui.basic.Label()",
      nullable : true,
      event : "changePorteGreffe"
    },
    dateArrachage : {
      init : null,
      check : "new qx.ui.basic.Label()",
      nullable : true,
      event : "changeDateArrachage"
    }

  },
  construct : function() {
    this.base(arguments);

    //service
    this.__service = prems.service.Service.getInstance();
    this.__servicePlant = prems.ServicePlant.getInstance();
    this.__serviceNot = prems.ServiceNotation.getInstance();

    /* -- Data Management --*/
    this.setName(new qx.ui.basic.Label());
    this.setDateMultiplication(new qx.ui.basic.Label());
    this.setDateRecolte(new qx.ui.basic.Label());
    this.setQuantite(new qx.ui.basic.Label());
    this.setAnneePremPousse(new qx.ui.basic.Label());
    this.setPorteGreffe(new qx.ui.basic.Label());
    this.setDateArrachage(new qx.ui.basic.Label());
    this.setAllPlaces(new qx.ui.basic.Label());

    /*-- UI drawing --*/
    this._setLayout(new qx.ui.layout.VBox());
    this.__container = new qx.ui.container.Composite(new qx.ui.layout.VBox(10));
    this.__container.setAllowStretchX(true, true);
    this.__container.setAllowStretchY(true, true);
    this._add(this.__container, {flex : 1});

    this.getAllPlaces().setAllowStretchX(true, true);
    this.getAllPlaces().setAllowStretchY(true, true);
    this.__container.add(this.getName());
    this.__container.add(this.getDateMultiplication());
    this.__container.add(this.getAnneePremPousse());
    this.__container.add(this.getPorteGreffe());
    this.__container.add(this.getDateArrachage());
    this.__container.add(this.getQuantite());
    this.getName().setVisibility("excluded");
    this.getDateMultiplication().setVisibility("excluded");
    this.getAnneePremPousse().setVisibility("excluded");
    this.getPorteGreffe().setVisibility("excluded");
    this.getDateArrachage().setVisibility("excluded");
    this.getQuantite().setVisibility("excluded");


    /* -- Listeners --*/
    this.addListener("changeId", function() {
      var userGroups = [];
      var session = qxelvis.session.service.Session.getInstance();
      var sessionId = session.getSessionId();
      if (session.getUserGroupsInfo() !== null) {
        userGroups = session.getUserGroupsInfo().getReadGroupIds();
      }
      //valeurs des informations
      var servicePlantInfo = this.__servicePlant.query("getAllInformationsByIdLot", [sessionId, userGroups, this.getId()]);
      servicePlantInfo.addListener("changeResponseModel", function(e) {
        var info = e.getData()["result"];
        if (info["name"] !== null) {
          this.getName().setValue("Nom du Lot : "+ info["name"]);
          this.getName().setVisibility("visible");
        }
        if (info["multiplication_date"] !== null) {
          this.getDateMultiplication().setValue("Date multiplication : "+ info["multiplication_date"]);
          this.getDateMultiplication().setVisibility("visible");
        }
        if (info["first_shoot_year"] !== null) {
          this.getAnneePremPousse().setValue("Année de première pousse : "+ info["first_shoot_year"]);
          this.getAnneePremPousse().setVisibility("visible");
        }
        if (info["rootstock"] !== null) {
          this.getPorteGreffe().setValue("Porte greffe : "+ info["rootstock"]);
          this.getPorteGreffe().setVisibility("visible");
        }
        if (info["death_date"] !== null) {
          this.getDateArrachage().setValue("Date arrachage : "+ info["death_date"]);
          this.getDateArrachage().setVisibility("visible");
        }
        if (info["quantity"] !== null) {
          this.getQuantite().setValue("Quantité : "+ info["quantity"]);
          this.getQuantite().setVisibility("visible");
        }
        //valeurs de emplacement
        var servicePlantPlace = this.__servicePlant.query("getAllPlacesByIdLot", [sessionId, userGroups, this.getId()]);
        servicePlantPlace.addListener("changeResponseModel", function(e) {
          var str = "Emplacement : ";
          if (e.getData()["result"].length > 0) {
            var r = e.getData()["result"];
            for (let l in r) {
              str = str + r[l]["type"].toString() + " : " + r[l]["value"].toString() + " ; ";
            }
          }
          this.getAllPlaces().setValue(str);
          if (this.getAllPlaces().getValue().length > 15) {
            this.__container.add(this.getAllPlaces());
          }
          var serviceNotNot = this.__serviceNot.query("getNotationIds", [sessionId, null, this.getId()]);
          //valeurs de notations
          serviceNotNot.addListener("changeResponseModel", function(e) {
            var result=[];
            if (e.getData()["result"].length > 0) {
              var r = e.getData()["result"];
              for (let l in r) {
                result.push({
                  "type_notation":r[l]["name"],
                  "value":r[l]["value"].toString(),
                  "experimentator":r[l]["experimentator"],
                  "date":r[l]["date"]
                });
              }
              //cree une nouvelle table
              var tableResult = new qx.ui.table.Table();
              tableResult.setHeight(100);
              tableResult.setAllowStretchX(true, true);
              tableResult.setAllowStretchY(true, true);

              var tableModel = new qx.ui.table.model.Filtered();
              tableModel.setColumns(Object.keys(result[0]));
              tableModel.setDataAsMapArray(result);
              tableResult.setTableModel(tableModel);
              var tcm = tableResult.getTableColumnModel();
              var tm = tableResult.getTableModel();
              for (let i = 0; i < tm.getColumnCount(); i++) {
                tcm.setColumnVisible(i, true);
              }
              this.__container.add(new qx.ui.basic.Label(this.tr("Notations sur Lot : ")));
              this.__container.add(tableResult, {flex : 1});
            }
          }, this);
        }, this);
      }, this);
    }, this);
  }
});
