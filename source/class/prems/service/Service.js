/**
* Service
*/
qx.Class.define("prems.service.Service", {
  type : "singleton",
  extend : qx.core.Object,
  events : {
    /**
    * Fired when there is a change in the database.
    */
    "dbChange" : "qx.event.type.Event"
  },

  /**
  * Constructor
  */
  construct : function() {
    this.base(arguments);
    prems.service.Service.SERVICE_BASE_URL = qxelvis.session.service.Session.SERVICE_BASE_URL;
    this._serviceUrl = prems.service.Service.SERVICE_BASE_URL + "ServiceCrb.py";
    this._serviceName = "ServiceCrb";
  },

  /**
  * Static constants
  */
  statics : {
    SERVICE_BASE_URL : "not_set"
  },

  /**
  * Method definitions.
  */
  members :
  {
    _serviceUrl : "not_set",
    _serviceName : "not_set",

    /**
    * synchronous query
    *@param serviceMethod {String} method to call
    *@param args {Array} arguments
    *@return {Array} result
    */
    syncQuery : function(serviceMethod, args) {
      this.warn("Don't use synchronus query! It's EVIL!");
      var service = new qx.io.remote.Rpc(prems.service.Service.SERVICE_BASE_URL + "ServiceCrb.py", this._serviceName);
      service.setTimeout(1000000);
      var locArgs = [serviceMethod];
      if (args !== null) {
        locArgs = locArgs.concat(args);
      }
      return service.callSync.apply(service, locArgs);
    },

    /**
    * asynchronous query
    *@param serviceMethod {String} method to call
    *@param args {Array} arguments
    *@return {Array} result
    */
    query : function(serviceMethod, args) {
      return new qxelvis.io.RpcService(prems.service.Service.SERVICE_BASE_URL + "ServiceCrb.py", this._serviceName, serviceMethod, args);
    },

    /**
    * get autocomplete model
    *@param method {String} method to call
    *@param args {Array} arguments
    *@param label {String} label
    *@param widget {qx.ui.Widget} widget
    */
    getAutoCompleteModel : function(method, args, label, widget) {
      var service = this.query(method, args);
      service.addListener("changeResponseModel", function() {
        var data = [];
        for (var i in service.getResult()) {
          if (service.getResult()[i][label] != "" && service.getResult()[i][label] !== null) {
            data.push(service.getResult()[i][label]);
          }
        }
        widget.setListModel(data);
      }, this);
    }
  }
});
