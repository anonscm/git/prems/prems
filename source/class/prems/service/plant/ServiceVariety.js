/**
 * ServiceVariety
 * mai 2019
 * Author : Fabrice dupuis
 */
qx.Class.define("prems.service.plant.ServiceVariety",
{
  type : "singleton",
  extend : qx.core.Object,

  events : {
    "failed" : "qx.event.type.Data",
    "completed" : "qx.event.type.Data",
    /**
    * Fired when the getListTaxonomy is completed
    * The data associed is a list of taxonomies
    */
    "gotListTaxonomy" : "qx.event.type.Data",
    /**
    * Fired when the getConformityVarietyList is completed
    * The data associed is a list of varieties with the differents ids when exists
    */
    "gotConformityVarietyList" : "qx.event.type.Data",
    /**
    * Fired when the saveVariety is completed
    * The data associed is a list of id varieties
    */
    "varietiesListSaved" : "qx.event.type.Data"
  },

  /**
  * Constructor
  */
  construct : function() {
    this.base(arguments);
    prems.service.plant.ServiceVariety.SERVICE_BASE_URL = qxelvis.session.service.Session.SERVICE_BASE_URL;
    this._serviceUrl = qxelvis.session.service.Session.SERVICE_BASE_URL + "/ServicePlant.py";
    this._serviceName = "ServicePlant";
  },

  statics : {
    SERVICE_BASE_URL : "SERVICE_BASE_URL_not_defined"
  },

  /**
  * Method definitions.
  */
  members :
  {
    _serviceUrl : "URL_NOT_SET",
    _serviceName : "SERVICE_NOT_SET",

    /**
    * getConformityVarietyList
    * @param varieteList {} list of objects Variety
    */

    getConformityVarietyList : function(varieteList) {
      var outputData = [];
      for (var k = 0; k < varieteList.length; k++) {
       outputData[k] = qx.util.Serializer.toNativeObject(varieteList[k]);
      }
      var session = qxelvis.session.service.Session.getInstance();
      var rpc = new qx.io.remote.Rpc(this._serviceUrl, this._serviceName);
      rpc.addListener("failed", function(e) {
        this.debug("Failed getConformityVarietyList" + e.getData());
      }, this);
      rpc.addListener("completed", function(e) {
        this.fireDataEvent("gotConformityVarietyList", e.getData());
      }, this);
      rpc.callAsyncListeners(
        true,
        "getConformityVarietyList",
        session.getSessionId(),
        session.getUserGroupsInfo().getWriteGroupIds(),
        outputData
      );
      return true;
    },

    /**
    * saveVarieties
    * @param varieteList {} list of objects Variety
    */

    saveVarieties : function(varieteList) {
      var outputData = [];
      for (var k = 0; k < varieteList.length; k++) {
       outputData[k] = qx.util.Serializer.toNativeObject(varieteList[k]);
      }
      var session = qxelvis.session.service.Session.getInstance();
      var rpc = new qx.io.remote.Rpc(this._serviceUrl, this._serviceName);
      rpc.addListener("failed", function(e) {
        this.debug("Failed saveVariety" + e.getData());
      }, this);
      rpc.addListener("completed", function(e) {
        this.fireDataEvent("varietiesListSaved", e.getData());
      }, this);
      rpc.callAsyncListeners(
        true,
        "saveVarieties",
        session.getSessionId(),
        session.getUserGroupsInfo().getWriteGroupIds(),
        outputData
      );
      return true;
    },
    getTaxonomiesFromListId : function(taxonomyListId, taxonomyTypeListId) {
      var session = qxelvis.session.service.Session.getInstance();
      var rpc = new qx.io.remote.Rpc(this._serviceUrl, this._serviceName);
      rpc.addListener("failed", function(e) {
        this.fireDataEvent("failed", e.getData());
        this.debug(e.getData());
        this.error("Failed to get taxons");
      }, this);
      rpc.addListener("completed", function(e) {
        var resp = e.getData();
        this.fireDataEvent("gotListTaxonomy", resp);
      }, this);
      rpc.callAsyncListeners(
        true,
        "getTaxonomiesFromListId",
        session.getSessionId(),
        taxonomyListId,
        taxonomyTypeListId
      );
      return true;
    }
  }
});
