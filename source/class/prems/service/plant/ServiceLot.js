/**
 * ServiceLot
 *  mai 2019
 * Author : Fabrice dupuis
 */
qx.Class.define("prems.service.plant.ServiceLot",
{
  type : "singleton",
  extend : qx.core.Object,

  events : {
    /**
    * Fired when a request fail
    */
    "failed": "qx.event.type.Data",

    "completed": "qx.event.type.Data",
    /**
    * Fired when getNamesForIdLot is completed
    */
    "gotNamesForIdLot": "qx.event.type.Data",
    /**
    * Fired when getVarieteByLot is completed
    */
    "gotVarieteByLot": "qx.event.type.Data",
    /**
    * Fired when getVarietyNameType is completed
    */
    "gotVarietyNameType": "qx.event.type.Data",
    /**
    * Fired when getAllPlaceNamesByGroup is completed
    */
    "gotAllPlaceNamesByGroup": "qx.event.type.Data",
    /**
    * Fired when getNamesLotsByNamesOrNotationsOrPlaces is completed
    */
    "gotNamesLotsByNamesOrNotationsOrPlaces": "qx.event.type.Data",
    /**
    * Fired when getGeneticBackgroundList is completed
    */
    "gotGeneticBackgroundList": "qx.event.type.Data",
    /**
    * Fired when getLotIdsForGeneticBackgroundIds is completed
    */
    "gotLotIdsForGeneticBackgroundIds": "qx.event.type.Data",
    
    "gotConformityLotList": "qx.event.type.Data",

    /**
    * Fired when lot ids have been retrieved from lot names
    *
    * Data is a dictionnary with keys = lot names, values = array of lot ids
    */
    "gotLotIdsFromLotNames" : "qx.event.type.Data"
  },

  /**
  * Constructor
  */
  construct : function() {
    this.base(arguments);
    prems.service.plant.ServiceLot.SERVICE_BASE_URL = qxelvis.session.service.Session.SERVICE_BASE_URL;
    this._serviceUrl = qxelvis.session.service.Session.SERVICE_BASE_URL + "/ServicePlant.py";
    this._serviceName = "ServicePlant";
  },

  statics : {
    SERVICE_BASE_URL : "SERVICE_BASE_URL_not_defined"
  },

  /**
  * Method definitions.
  */
  members :
  {
    _serviceUrl : "URL_NOT_SET",
    _serviceName : "SERVICE_NOT_SET",

    /**
    * getConformityLotList
    *
    * @param lotList {} list of objects lots
    */
    getConformityLotList : function(lotList) {
      var outputData = [];
      for (var k = 0; k < lotList.length; k++) {
       outputData[k] = qx.util.Serializer.toNativeObject(lotList[k]);
      }
      var session = qxelvis.session.service.Session.getInstance();
      var rpc = new qx.io.remote.Rpc(this._serviceUrl, this._serviceName);
      rpc.addListener("failed", function(e) {
        this.debug("Failed to getConformityLotList" + e.getData());
      }, this);
      rpc.addListener("completed", function(e) {
        var resp = e.getData();
        this.fireDataEvent("gotConformityLotList", resp);
      }, this);
      rpc.callAsyncListeners(
        true,
        "getConformityLotList",
        // [
        session.getSessionId(),
        session.getUserGroupsInfo().getWriteGroupIds(),
        outputData
        // ]
      );
      return true;
    },

    /**
    * Request lot ids given a list of lot names
    *
    * Lot names are strings or motifs using '*' as wildchar
    * This method fires the gotLotIdsFromLotNames DataEvent when data are
    * retrieved.
    * @param lotNames {Array} a list of lot names
    * @returns {qx.io.remote.Rpc} a ref to the Rpc for cancelation if needed
    */
    getLotIdsFromLotNames : function(lotNames) {
      let session = qxelvis.session.service.Session.getInstance();
      let rpc = new qx.io.remote.Rpc(this._serviceUrl, this._serviceName);
      rpc.setTimeout(1000000);
      rpc.addListener("failed", function(e) {
        this.debug("Failed to retrieve lot ids");
        this.fireDataEvent("failed", e.getData());
      }, this);
      rpc.addListener("completed", function(e) {
        let res = e.getData()["result"];
        this.fireDataEvent("gotLotIdsFromLotNames", res);
      }, this);
      rpc.callAsyncListeners(
        true,
        "getLotIdsFromLotNames",
        session.getSessionId(),
        session.getUserGroupsInfo().getReadGroupIds(),
        lotNames
      );
      return rpc;
    },

    getNamesForIdLot: function(lotIds) {
      var session = qxelvis.session.service.Session.getInstance();
      var rpc = new qx.io.remote.Rpc(this._serviceUrl, this._serviceName);
      rpc.addListener("failed", function(e) {
        this.debug("Failed to getNamesForIdLot" + e.getData());
      }, this);
      rpc.addListener("completed", function(e) {
        this.fireDataEvent("gotNamesForIdLot", e.getData()["result"]);
      }, this);
      rpc.callAsyncListeners(
        true,
        "getNamesForIdLot",
        session.getSessionId(),
        lotIds
      );
    },

    getVarieteByLot: function(lotIds) {
      var session = qxelvis.session.service.Session.getInstance();
      var rpc = new qx.io.remote.Rpc(this._serviceUrl, this._serviceName);
      rpc.addListener("failed", function(e) {
        this.debug("Failed to getVarieteByLot" + e.getData());
      }, this);
      rpc.addListener("completed", function(e) {
        this.fireDataEvent("gotVarieteByLot", e.getData()["result"]);
      }, this);
      rpc.callAsyncListeners(
        true,
        "getVarieteByLot",
        session.getSessionId(),
        lotIds
      );
    },

    getVarietyNameType: function(taxonomyId) {
      var session = qxelvis.session.service.Session.getInstance();
      var rpc = new qx.io.remote.Rpc(this._serviceUrl, this._serviceName);
      rpc.addListener("failed", function(e) {
        this.debug("Failed to getVarietyNameType" + e.getData());
      }, this);
      rpc.addListener("completed", function(e) {
        this.fireDataEvent("gotVarietyNameType", e.getData()["result"]);
      }, this);
      rpc.callAsyncListeners(
        true,
        "getVarietyNameType",
        session.getSessionId(),
        taxonomyId
      );
    },

    getAllPlaceNamesByGroup: function() {
      var session = qxelvis.session.service.Session.getInstance();
      var rpc = new qx.io.remote.Rpc(this._serviceUrl, this._serviceName);
      rpc.addListener("failed", function(e) {
        this.debug("Failed to getAllPlaceNamesByGroup" + e.getData());
      }, this);
      rpc.addListener("completed", function(e) {
        this.fireDataEvent("gotAllPlaceNamesByGroup", e.getData()["result"]);
      }, this);
      rpc.callAsyncListeners(
        true,
        "getAllPlaceNamesByGroup",
        session.getSessionId(),
        session.getUserGroupsInfo().getReadGroupIds()
      );
    },

    getNamesLotsByNamesOrNotationsOrPlaces: function(
      lnam,
      lnot,
      lplace,
      lnamIn,
      lnotIn,
      lplaceIn,
      lnamNear,
      lnotAbs) {
      var session = qxelvis.session.service.Session.getInstance();
      var rpc = new qx.io.remote.Rpc(this._serviceUrl, this._serviceName);
      rpc.addListener("failed", function(e) {
        this.debug("Failed to getNamesLotsByNamesOrNotationsOrPlaces" + e.getData());
      }, this);
      rpc.addListener("completed", function(e) {
        this.fireDataEvent(
          "gotNamesLotsByNamesOrNotationsOrPlaces", e.getData()["result"]
        );
      }, this);
      rpc.callAsyncListeners(
        true,
        "getNamesLotsByNamesOrNotationsOrPlaces",
        session.getSessionId(),
        session.getUserGroupsInfo().getReadGroupIds(),
        lnam,
        lnot,
        lplace,
        lnamIn,
        lnotIn,
        lplaceIn,
        lnamNear,
        lnotAbs
      );
    },

    getGeneticBackgroundList: function(
      taxonomyId) {
      var session = qxelvis.session.service.Session.getInstance();
      var rpc = new qx.io.remote.Rpc(this._serviceUrl, this._serviceName);
      rpc.addListener("failed", function(e) {
        this.debug("Failed to getGeneticBackgroundList" + e.getData());
      }, this);
      rpc.addListener("completed", function(e) {
        this.fireDataEvent(
          "gotGeneticBackgroundList", e.getData()["result"]
        );
      }, this);
      rpc.callAsyncListeners(
        true,
        "getGeneticBackgroundList",
        session.getSessionId(),
        taxonomyId
      );
    },

    getLotIdsForGeneticBackgroundIds: function(geneticId, taxonomyId) {
      var session = qxelvis.session.service.Session.getInstance();
      var rpc = new qx.io.remote.Rpc(this._serviceUrl, this._serviceName);
      rpc.addListener("failed", function(e) {
        this.debug("Failed to getLotIdsForGeneticBackgroundIds" + e.getData());
      }, this);
      rpc.addListener("completed", function(e) {
        this.fireDataEvent(
          "gotLotIdsForGeneticBackgroundIds", e.getData()["result"]
        );
      }, this);
      rpc.callAsyncListeners(
        true,
        "getLotIdsForGeneticBackgroundIds",
        session.getSessionId(),
        geneticId,
        taxonomyId,
        session.getUserGroupsInfo().getReadGroupIds()
      );
    },
    /**
    * saveLots
    * @param varList {} list of objects variety
    * @param accList {} list of objects accession
    * @param lotList {} list of objects lots
    */
    saveLots : function(varList, accList, lotList) {
      var outputData = [];
      for (var k = 0; k < lotList.length; k++) {
       outputData[k] = qx.util.Serializer.toNativeObject(lotList[k]);
      }
      var session = qxelvis.session.service.Session.getInstance();
      var rpc = new qx.io.remote.Rpc(this._serviceUrl, this._serviceName);
      rpc.addListener("failed", function(e) {
        this.fireDataEvent("failed", e.getData());
      }, this);
      rpc.addListener("completed", function(e) {
        var resp = e.getData();
        this.fireDataEvent("savedLots", resp);
      }, this);
      rpc.callAsyncListeners(
        true,
        "saveLots",
        session.getSessionId(),
        session.getUserGroupsInfo().getWriteGroupIds(),
        outputData
      );
      return true;
    }
  }
});
