/**
 * ServiceAccession
 *  mai 2019
 * Author : Fabrice dupuis
 */
qx.Class.define("prems.service.plant.ServiceAccession",
{
  type : "singleton",
  extend : qx.core.Object,

  events : {
    "failed" : "qx.event.type.Data",
    "completed" : "qx.event.type.Data",
    /**
    * Fired when the getConformityAccessionList is completed
    * The data associed is a list of accessions with the differents ids when exists
    */
    "gotConformityAccessionList" : "qx.event.type.Data"
  },

  /**
  * Constructor
  */
  construct : function() {
    this.base(arguments);
    prems.service.plant.ServiceAccession.SERVICE_BASE_URL = qxelvis.session.service.Session.SERVICE_BASE_URL;
    this._serviceUrl = qxelvis.session.service.Session.SERVICE_BASE_URL + "/ServicePlant.py";
    this._serviceName = "ServicePlant";
  },

  statics : {
    SERVICE_BASE_URL : "SERVICE_BASE_URL_not_defined"
  },

  /**
  * Method definitions.
  */
  members :
  {
    _serviceUrl : "URL_NOT_SET",
    _serviceName : "SERVICE_NOT_SET",

    /**
    * getConformityAccessionList
    * @param accessionList {} list of objects accession

    */

    getConformityAccessionList : function(accessionList) {
      var outputData = [];
      for (var k = 0; k < accessionList.length; k++) {
       outputData[k] = qx.util.Serializer.toNativeObject(accessionList[k]);
      }
      var session = qxelvis.session.service.Session.getInstance();
      var rpc = new qx.io.remote.Rpc(this._serviceUrl, this._serviceName);
      rpc.addListener("failed", function(e) {
        this.debug("Failed getConformityAccessionList" + e.getData());
      }, this);
      rpc.addListener("completed", function(e) {
        var resp = e.getData();
        this.fireDataEvent("gotConformityAccessionList", resp);
      }, this);
      rpc.callAsyncListeners(
        true,
        "getConformityAccessionList",
        session.getSessionId(),
        session.getUserGroupsInfo().getWriteGroupIds(),
        outputData
      );
      return true;
    }
  }
});
