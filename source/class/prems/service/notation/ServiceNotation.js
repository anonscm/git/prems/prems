/**
 * ServiceNotation
 * 14 mai 2019
 * Author: Fabrice dupuis
 */
qx.Class.define("prems.service.notation.ServiceNotation",
{
  type : "singleton",
  extend : qx.core.Object,

  events : {
    "failed" : "qx.event.type.Data",
    "completed" : "qx.event.type.Data",
    "gotContext" : "qx.event.type.Data",
    "savedListNotations" : "qx.event.type.Data",
    "gotTypeNotationByGroup": "qx.event.type.Data"
  },

  /**
  * Constructor
  */
  construct : function() {
    this.base(arguments);
    prems.service.plant.ServiceVariety.SERVICE_BASE_URL = qxelvis.session.service.Session.SERVICE_BASE_URL;
    this._serviceUrl = qxelvis.session.service.Session.SERVICE_BASE_URL + "/ServiceNotation.py";
    this._serviceName = "ServiceNotation";
  },

  statics : {
    SERVICE_BASE_URL : "SERVICE_BASE_URL_not_defined"
  },

  /**
  * Method definitions.
  */
  members :   {
    _serviceUrl : "URL_NOT_SET",
    _serviceName : "SERVICE_NOT_SET",

    /**
    * asynchronous query
    * @param serviceMethod {String} method to call
    * @param args {Array} arguments
    * @return {Array} result
    */
    query : function(serviceMethod, args) {
      return new qxelvis.io.RpcService(this._serviceUrl, this._serviceName, serviceMethod, args);
    },

    /**
    * Recherche la liste des contextes notations et des types de notations associés
    * utilise la liste des usergroups en mode read
    */
    getContext : function() {
      let session = qxelvis.session.service.Session.getInstance();
      let rpc = new qx.io.remote.Rpc(this._serviceUrl, this._serviceName);
      rpc.addListener("failed", function(e) {
        this.debug("Failed to find context" + e.getData());
      }, this);
      rpc.addListener("completed", function(e) {
        let resp = e.getData();
        this.fireDataEvent("gotContext", resp["result"]);
      }, this);
      rpc.callAsyncListeners(
        true,
        "getContext",
        session.getSessionId(),
        session.getUserGroupsInfo().getReadGroupIds()
      );
    },

    /**
    * Save notations for given lots
    *
    * @param notationList {Object} list of prems.data.notation.Notaion associated
    * to Lot id.
    *
    * <pre>
    * {
    *   idLot1 : [
    *     notation1,
    *     notation2
    *   ],
    *   idLot2 : [
    *     notation3
    *   ]
    * }
    * </pre>
    *
    * @param campaign {Array} list of Experiement ids
    * @param type {String} "seed" or "tree"
    */
    saveNotationsForLots : function(notationList, campaign, type) {
      let outputData = {};
      for (let k in notationList) {
        outputData[k] = qx.util.Serializer.toNativeObject(notationList[k]);
      }
      let session = qxelvis.session.service.Session.getInstance();
      let rpc = new qx.io.remote.Rpc(this._serviceUrl, this._serviceName);
      rpc.setTimeout(60000);
      rpc.addListener("failed", function(e) {
        this.fireDataEvent("failed", e.getData());
        this.debug(e.getData());
        this.error("Failed to save data");
      }, this);
      rpc.addListener("completed", function(e) {
        this.fireDataEvent("savedListNotations", e.getData());
      }, this);
      rpc.callAsyncListeners(
        true,
        "saveNotationsForLots",
        session.getSessionId(),
        session.getUserGroupsInfo().getWriteGroupIds(),
        outputData,
        campaign,
        type
      );
    },

    /**
    * saveNotationsForVarieties
    * @param notationList {list} list of objects notation.js
    * @param campaign {list} list of id of experiement
    */
    saveNotationsForVarieties : function(notationList, campaign) {
      let outputData = {};
      for (let k in notationList) {
        outputData[k] = qx.util.Serializer.toNativeObject(notationList[k]);
      }
      let session = qxelvis.session.service.Session.getInstance();
      let rpc = new qx.io.remote.Rpc(this._serviceUrl, this._serviceName);
      rpc.addListener("failed", function(e) {
        this.fireDataEvent("failed", e.getData());
        this.debug(e.getData());
        this.error("Failed to save data");
      }, this);
      rpc.addListener("completed", function(e) {
        this.fireDataEvent("savedListNotations", e.getData());
      }, this);
      rpc.callAsyncListeners(
        true,
        "saveNotationsForVarieties",
        session.getSessionId(),
        session.getUserGroupsInfo().getWriteGroupIds(),
        outputData,
        campaign
      );
    },

    /**
    * saveNotationsForTaking
    * @param notationList {list} list of objects notation.js
    * @param campaign {list} list of id of experiement

    */
    saveNotationsForTaking: function(notationList, campaign) {
      let outputData = {};
      for (let k in notationList) {
        outputData[k] = qx.util.Serializer.toNativeObject(notationList[k]);
      }
      let session = qxelvis.session.service.Session.getInstance();
      let rpc = new qx.io.remote.Rpc(this._serviceUrl, this._serviceName);
      rpc.addListener("failed", function(e) {
        this.fireDataEvent("failed", e.getData());
        this.debug(e.getData());
        this.error("Failed to save data");
      }, this);
      rpc.addListener("completed", function(e) {
        this.fireDataEvent("savedListNotations", e.getData());
      }, this);
      rpc.callAsyncListeners(
        true,
        "saveNotationsForTaking",
        session.getSessionId(),
        session.getUserGroupsInfo().getWriteGroupIds(),
        outputData,
        campaign
      );
    },

    getTypeNotationByGroup: function() {
      let session = qxelvis.session.service.Session.getInstance();
      let rpc = new qx.io.remote.Rpc(this._serviceUrl, this._serviceName);
      rpc.addListener("failed", function(e) {
        this.debug("Failed to find context" + e.getData());
      }, this);
      rpc.addListener("completed", function(e) {
        this.fireDataEvent("gotTypeNotationByGroup", e.getData()["result"]);
      }, this);
      rpc.callAsyncListeners(
        true,
        "getTypeNotationByGroup",
        session.getSessionId(),
        session.getUserGroupsInfo().getWriteGroupIds()
      );
    }
  }
});
