/**
 * Experiment service
 *
 * @author Sylvain Gaillard
 * @date 5 juillet 2019
 */
qx.Class.define("prems.service.Experiment",
{
  type : "singleton",
  extend : qx.core.Object,

  events : {
    /**
     * Fired whe Experiments are fetched
     */
    "gotExperiments" : "qx.event.type.Data",

    /**
     * Fired when a request failed
     */
    "failed" : "qx.event.type.data"
  },

  /**
  * Constructor
  */
  construct : function() {
    this.base(arguments);
    this._serviceUrl = qxelvis.session.service.Session.SERVICE_BASE_URL + "/ServiceProject.py";
    this._serviceName = "ServiceProject";
  },

  statics : {
    SERVICE_BASE_URL : "SERVICE_BASE_URL_not_defined"
  },

  /**
  * Method definitions.
  */
  members :   {
    _serviceUrl : "URL_NOT_SET",
    _serviceName : "SERVICE_NOT_SET",

    /**
     * Fetch Experiments directly linked to a Project.
     *
     * @param projectId {Integer} The Project id from which Experiments must be fetched.
     * @fires gotExperiments where data is a list of Experiment objects
     */
    getExperimentsFromProjectId: function(projectId) {
      let session = qxelvis.session.service.Session.getInstance();
      let rpc = new qx.io.remote.Rpc(this._serviceUrl, this._serviceName);
      rpc.addListener("failed", function(e) {
        this.fireDataEvent("failed", e.getData());
      }, this);
      rpc.addListener("completed", function(e) {
        let resp = e.getData()["result"];
        // this.debug(qx.lang.Json.stringify(resp));
        let exp = [];
        for (let i in resp) {
          exp.push(prems.data.experiment.Experiment.fromNativeObject(resp[i]));
        }
        this.fireDataEvent("gotExperiments", exp);
      }, this);
      rpc.callAsyncListeners(
        true,
        "getExperimentsFromProjectId",
        session.getSessionId(),
        session.getUserGroupsInfo().getReadGroupIds(),
        projectId
      );
    }
  }
});
