/* ************************************************************************

  qooxdoo - the new era of web development
  Version 3.0.0
  http://qooxdoo.org

  Copyright: 2012 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
    See the LICENSE file in the project's top-level directory for details.

  Authors:
    * Aurélie Lelièvre
   
************************************************************************ */

/**
 * 
 */
qx.Class.define("prems.DataTypeValidator",
{

  statics :
  {
    /**
     * Check if a string is an integer.
     * @param data {String} Value to check.
     * @return {Boolean} Return value of the test.
     */
    isInteger : function(x) {
      return (/^\d+$/.test(x) === true);
      //return x % 1 === 0;
    },
    
      /**
     * Check if a string is an float.
     * @param data {String} Value to check.
     * @return {Boolean} Return value of the test.
     */
    isFloat : function(x) {
      return (/^\d+(\.\d+)$/.test(x) === true);
      //return !isNaN(x);
    },
    
     /**
     * Check if a string is a valid date.
     * @param data {String} Value to check.
     * @return {Boolean} Return value of the test.
     */
    isDate : function(dateString) {
      // First check for the pattern
      if (/^(\d{1,2})\/(\d{1,2})\/\d{4}$/.test(dateString) === false) {
 return false; 
}
      
      // Parse the date parts to integers
      var parts = dateString.split("/");
      var day = parseInt(parts[0], 10);
      var month = parseInt(parts[1], 10);
      var year = parseInt(parts[2], 10);

      // Check the ranges of month and year
      if (year < 1000 || year > 3000 || month == 0 || month > 12) {
 return false; 
}

      var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

      // Adjust for leap years
      if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)) {
 monthLength[1] = 29; 
}

      // Check the range of the day
      return day > 0 && day <= monthLength[month - 1];
    }
  }
});
