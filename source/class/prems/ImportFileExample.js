/**
 * File import example.
*/
qx.Class.define("prems.ImportFileExample",
{
  extend : qx.core.Object,
  events : {

  },
  properties : {

  },
  construct : function() {
    this.base(arguments);
    this.__service = prems.service.Service.getInstance();
  },
  members :
  {
    /**
           * Get import materiel file
           *@return {String} file
           */
    getFile : function() {
      var file = "";
      file += "Collection\t";
      file += "Nom:Type nom\t";
      file += "Date nom\t";
      file += "Obtenteur\t";
      file += "Éditeur\t";
      file += "Genre\t";
      file += "Espèce\t";
      file += "Remarque variété\t";
      file += "Numéro accession\t";
      file += "Nom accession\t";
      file += "Date introduction\t";
      file += "Fournisseur\t";
      file += "État sanitaire\t";
      file += "Mode introduction\t";
      file += "Numéro passeport phytosanitaire\t";
      file += "Lieu greffage\t";
      file += "Numéro lot\t";
      file += "Type lot\t";
      file += "Équilibre\t";
      file += "Date récolte\t";
      file += "Quantité\t";
      file += "Rang évaluation\t";
      file += "Lot père multiplication\t";
      file += "Conforme\t";
      file += "Multiplicateur\t";
      file += "Remarque multiplication\t";
      file += "Nombre porte-graines\t";
      file += "Quantité initiale multiplication\t";
      file += "Site\t";
      file += "Lieu:Type lieu";
      file += "\n";
      return file;
    },

    /**
           * Get import notation file.
           *@return {String} file
           */
    getNotationFile : function() {
      var file = "";
      file += "Numéro lot\t";
      file += "Notation:type notation\t";
      file += "Date notation\t";
      file += "Remarque notation\t";
      file += "Notateur";
      file += "\n";
      return file;
    }
  }
});
