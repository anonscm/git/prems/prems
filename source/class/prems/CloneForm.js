/**
 * Clone form
*/
qx.Class.define("prems.CloneForm",
{
  extend : qx.ui.core.Widget,

  /**
     * Creates a new instance of CloneForm.
     *@param clone {prems.Clone} clone object.
     *@param noVariete {Boolean} whether to show or not varieté informations.
    */
  construct : function(clone, noVariete) {
    this.base(arguments);

    //Layouts
    var container = new qx.ui.container.Composite(new qx.ui.layout.VBox(20));
    this._setLayout(new qx.ui.layout.VBox(20));
    this._add(container);

    //Service
    this.__service = prems.service.Service.getInstance();
    var session = qxelvis.session.service.Session.getInstance();
    var userGroups = session.getUserGroupsInfo().getReadGroupIds();

    //Form Clone
    var form = new qx.ui.form.Form();
    var boxCollections = new qx.ui.groupbox.GroupBox(this.tr("Collections"));
    boxCollections.setLayout(new qx.ui.layout.VBox(5));

    // Customize the table column model.  We want one that automatically

    // resizes columns.
    var custom = {
      tableColumnModel : function(obj) {
        return new qx.ui.table.columnmodel.Resize(obj);
      }
    };
    var tableModelCol = new qx.ui.table.model.Simple();
    tableModelCol.setColumnIds(["nom", "hash"]);
    tableModelCol.setColumnNamesByIndex(["Nom", "Hash"]);
    var tableCol = new qx.ui.table.Table(tableModelCol, custom);
    tableCol.setHeight(80);
    tableCol.setStatusBarVisible(false);
    tableCol.getTableColumnModel().setColumnVisible(1, false);
    tableModelCol.setColumnEditable(0, true);
    var tcmCol = tableCol.getTableColumnModel();
    var autoCompleteEditor = new prems.AutoCompleteCellEditor("collection");
    tcmCol.setCellEditorFactory(0, autoCompleteEditor);

    // Resize the table columns
    var resizeBehavior = tcmCol.getBehavior();
    resizeBehavior.set(0, {
      width : 400
    });
    boxCollections.add(tableCol);
    var layoutButtons1 = new qx.ui.container.Composite(new qx.ui.layout.HBox(10));


    //Bouton ajout clone
    var buttonAddCol = new qx.ui.form.Button(this.tr("+"));
    layoutButtons1.add(buttonAddCol);
    buttonAddCol.setAllowStretchX(false);
    buttonAddCol.setAlignX("right");
    buttonAddCol.addListener("execute", function() {
      clone.addCollection();
    }, this);
    var command = new qx.ui.command.Command("Delete");
    var buttonDeleteCol = new qx.ui.form.Button(this.tr("-"), null, command);
    layoutButtons1.add(buttonDeleteCol);
    buttonDeleteCol.setAllowStretchX(false);
    buttonDeleteCol.setAlignX("right");
    boxCollections.add(layoutButtons1);
    command.addListener("execute", function() {
      try {
        var line = tableCol.getSelectionModel().getSelectedRanges()[0]["maxIndex"];
        var hash = tableCol.getTableModel().getDataAsMapArray()[line]["hash"];
        var collection = qx.core.ObjectRegistry.fromHashCode(hash);
        collection.setToDelete(true);
        clone.fireDataEvent("changeCollections", null);
      } catch (e) {
      }
    }, this);
    this.__updateTableCol(clone, tableCol);
    clone.addListener("changeCollections", function() {
      this.__updateTableCol(clone, tableCol);
    }, this);
    tableModelCol.addListener("dataChanged", function(e) {
      this.__updateModelTableCol(clone, tableCol);
    }, this);
    var boxClone = new qx.ui.groupbox.GroupBox(this.tr("Accession"));
    boxClone.setLayout(new qx.ui.layout.VBox(5));

    //Variété
    var varieteTf = new prems.AutoCompleteBox();
    this.__service.getAutoCompleteModel("getAllNomVariete", [userGroups], "nom", varieteTf);
    form.add(varieteTf, this.tr("Variété"), null, "variete");
    clone.bind("nomVariete", varieteTf, "value");
    varieteTf.bind("value", clone, "nomVariete");
    varieteTf.setWidth(200);
    varieteTf.setRequired(true);
    if (noVariete == true) {
      varieteTf.setEnabled(false);
      varieteTf.exclude();
    }

    //Nom
    var nomTf = new prems.AutoCompleteBox();

    //this.__service.getAutoCompleteModel("getAllNomC", [], "espece", especeTf);
    form.add(nomTf, this.tr("Nom accession"), null, "nom");
    clone.bind("nom", nomTf, "value");
    nomTf.bind("value", clone, "nom");
    nomTf.setWidth(200);

    //Numéro
    var numeroTf = new prems.AutoCompleteBox();
    this.__service.getAutoCompleteModel("getAllNumeroClone", [userGroups], "numero", numeroTf);
    form.add(numeroTf, this.tr("Numéro"), null, "numero");
    clone.bind("numero", numeroTf, "value");
    numeroTf.bind("value", clone, "numero");
    numeroTf.setRequired(true);

    //Numero d'accession
    var numeroCheck = new qx.ui.form.CheckBox();
    form.add(numeroCheck, this.tr("Numéro automatique"), null, "numeroCheck");
    clone.bind("numeroAuto", numeroCheck, "value");
    numeroCheck.bind("value", clone, "numeroAuto");
    numeroCheck.addListener("changeValue", function() {
      numeroTf.setEnabled(!numeroCheck.getValue());
    }, this);
    if (clone.getId() !== null) {
      numeroCheck.exclude();
    }

    //Nom
    var dateIntroTf = new qx.ui.form.DateField();
    form.add(dateIntroTf, this.tr("Date introduction"), null, "dateIntro");
    clone.bind("dateIntro", dateIntroTf, "value");
    dateIntroTf.bind("value", clone, "dateIntro");

    //Nom
    var fournisseurTf = new prems.AutoCompleteBox();
    this.__service.getAutoCompleteModel("getAllPepinieriste", [], "nom", fournisseurTf);
    form.add(fournisseurTf, this.tr("Fournisseur"), null, fournisseurTf);
    clone.bind("fournisseur", fournisseurTf, "value");
    fournisseurTf.bind("value", clone, "fournisseur");
    var dateFormat = new qx.util.format.DateFormat("dd/MM/yyyy");
    dateIntroTf.setDateFormat(dateFormat);

    //Form Clone
    var formIntro = new qx.ui.form.Form();

    // create the second group box
    var boxIntroduction = new qx.ui.groupbox.CheckGroupBox(this.tr("Introduction"));
    boxIntroduction.setLayout(new qx.ui.layout.Grow());
    var introductionClone = clone.getIntroductionClone();
    if (introductionClone === null) {
      introductionClone = new prems.IntroductionClone();
    }

    //Etat sanitaire
    var etatSanitaireTf = new qx.ui.form.TextField();
    formIntro.add(etatSanitaireTf, this.tr("Etat sanitaire"), null, "etatSanitaire");
    introductionClone.bind("etatSanitaire", etatSanitaireTf, "value");
    etatSanitaireTf.bind("value", introductionClone, "etatSanitaire");

    //Etat sanitaire
    var modeIntroTf = new qx.ui.form.TextField();
    formIntro.add(modeIntroTf, this.tr("Mode introduction"), null, "modeIntroduction");
    introductionClone.bind("modeIntroduction", modeIntroTf, "value");
    modeIntroTf.bind("value", introductionClone, "modeIntroduction");

    //Etat sanitaire
    var numeroPasseportTf = new qx.ui.form.TextField();
    formIntro.add(numeroPasseportTf, this.tr("Passeport phytosanitaire"), null, "numeroPasseport");
    introductionClone.bind("numeroPasseport", numeroPasseportTf, "value");
    numeroPasseportTf.bind("value", introductionClone, "numeroPasseport");

    //Etat sanitaire
    var lieuGreffageTf = new qx.ui.form.TextField();
    formIntro.add(lieuGreffageTf, this.tr("Lieu greffage"), null, "lieuGreffage");
    introductionClone.bind("lieuGreffage", lieuGreffageTf, "value");
    lieuGreffageTf.bind("value", introductionClone, "lieuGreffage");
    var rendererIntro = new qx.ui.form.renderer.Double(formIntro);
    boxIntroduction.add(rendererIntro);
    boxIntroduction.addListener("changeValue", function(e) {
      if (e.getData() == true) {
        clone.setIntroductionClone(introductionClone);
        rendererIntro.show();
      }
      if (e.getData() == false) {
        clone.setIntroductionClone(null);
        formIntro.reset();
        rendererIntro.exclude();
      }
    }, this);
    var boxLots = new qx.ui.groupbox.GroupBox(this.tr("Lots"));
    boxLots.setLayout(new qx.ui.layout.VBox());
    var tableModel = new qx.ui.table.model.Simple();
    tableModel.setColumnIds(["id", "numero", "type", "hash"]);
    tableModel.setColumnNamesByIndex(["Id", "Numéro", "Type", "Hash"]);
    var tableLots = new qx.ui.table.Table(tableModel);
    tableLots.setHeight(80);
    tableLots.setStatusBarVisible(false);
    tableLots.getTableColumnModel().setColumnVisible(0, false);
    tableLots.getTableColumnModel().setColumnVisible(3, false);
    boxLots.add(tableLots);
    tableLots.addListener("cellDbltap", function(e) {
      var hash = tableLots.getTableModel().getRowDataAsMap(e.getRow())["hash"];
      var lot = qx.core.ObjectRegistry.fromHashCode(hash);
      var formLot = new prems.LotForm(lot, true);
      var win = new prems.MyWindow(this.tr("Nouveau lot"));
      win.setLayout(new qx.ui.layout.Grow());
      win.add(formLot);
      win.open();
      lot.addListenerOnce("saved", function() {
        clone.addLot(lot);
        win.close();
      }, this);
    }, this);

    //Boutons ajout/supression de lot
    var menu = new qx.ui.menu.Menu();
    var newArbre = new qx.ui.menu.Button("Arbre");
    var newGraines = new qx.ui.menu.Button("Graines");
    menu.setMinWidth(120);
    newArbre.addListener("execute", function() {
      var lot = new prems.Lot(null, "arbre");
      var formLot = new prems.LotForm(lot, true);
      var win = new prems.MyWindow(this.tr("Nouveau lot"));
      win.setLayout(new qx.ui.layout.Grow());
      win.add(formLot);
      win.open();
      lot.addListenerOnce("saved", function() {
        clone.addLot(lot);
        win.close();
      }, this);
    }, this);
    newGraines.addListener("execute", function() {
      var lot = new prems.Lot(null, "graines");
      var formLot = new prems.LotForm(lot, true);
      var win = new prems.MyWindow(this.tr("Nouveau lot"));
      win.setLayout(new qx.ui.layout.Grow());
      win.add(formLot);
      win.open();
      lot.adListenerOnce("saved", function() {
        clone.addLot(lot);
        win.close();
      }, this);
    }, this);
    menu.add(newArbre);
    menu.add(newGraines);
    var layoutButtons = new qx.ui.container.Composite(new qx.ui.layout.HBox(10));

    var buttonAddLot = new qx.ui.form.SplitButton(this.tr("+"), null, menu);
    // var buttonRemoveLot = new qx.ui.form.Button(this.tr("-"));
    layoutButtons.add(buttonAddLot);
    buttonAddLot.setAllowStretchX(false);
    buttonAddLot.setAlignX("right");
    // layoutButtons.add(buttonRemoveLot);
    // buttonRemoveLot.setAllowStretchX(false);
    // buttonRemoveLot.setAlignX("right");
    // buttonRemoveLot.addListener("execute", function(){
    //   //clone.removeLot(lot);
    //   var selection = tableLots.getSelectionModel().getSelectedRanges()[0]["maxIndex"]; //(maxIndex = minIndex)
    //   var hash = tableLots.getTableModel().getRowDataAsMap(selection)["hash"];
    //   var lot = qx.core.ObjectRegistry.fromHashCode(hash);
    //   clone.removeLot(lot);
    // },this);
    boxLots.add(layoutButtons);

    //Update de la liste des noms de la variété
    this.__updateTableLots(clone, tableLots);
    clone.addListener("changeLots", function() {
      this.__updateTableLots(clone, tableLots);
    }, this);

    //Bouton enregistrer
    var buttonSave = new qx.ui.form.Button(this.tr("Enregistrer"));
    buttonSave.setAllowStretchX(false);
    buttonSave.setAlignX("right");
    var popup = new qx.ui.popup.Popup(new qx.ui.layout.Canvas());
    popup.add(new qx.ui.basic.Atom(this.tr("Accession enregistrée")));
    buttonSave.addListener("execute", function() {
      this.__disableButtonSave(buttonSave);
      if (this.__validate(tableLots, varieteTf, numeroCheck, numeroTf, fournisseurTf)) {
        if (clone.getId() === null) {
          if (varieteTf.isEnabled()) {
            clone.save();
            clone.addListener("saved", function() {
              this.__enableButtonSave(buttonSave);
              popup.placeToWidget(buttonSave);
              popup.show();
            }, this);
          } else {
            clone.fireEvent("saved");
            this.__enableButtonSave(buttonSave);
            popup.placeToWidget(buttonSave);
            popup.show();
          }
        } else {
          clone.save();
          clone.addListener("saved", function() {
            this.__enableButtonSave(buttonSave);
            popup.placeToWidget(buttonSave);
            popup.show();
          }, this);
        }
      } else {
        this.__enableButtonSave(buttonSave);
      }
    }, this);
    boxClone.add(new qx.ui.form.renderer.Double(form));
    container.add(boxCollections);
    container.add(boxClone);
    container.add(boxIntroduction);
    container.add(boxLots);
    container.add(buttonSave);
    if (clone.getId() !== null) {
      boxLots.exclude();
      tableLots.exclude();
    }
  },
  members : {
    /**
    * Update table lots.
    * @param clone {prems.Clone} clone
    * @param table {table} table
    */
    __updateTableLots : function(clone, table) {
      var rows = [];
      clone.getLots().forEach(function(lot) {
        rows.push(lot.getRow());
        lot.addListener("saved", function() {
          this.__updateTableLots(clone, table);
        }, this);
      }, this);
      var tableModel = table.getTableModel();
      tableModel.setDataAsMapArray(rows);
    },

    /**
    * Update model table lots.
    * @param clone {prems.Clone} clone
    * @param table {table} table
    */
    __updateModelTableLots : function(clone, table) {
      var tableModel = table.getTableModel();
      var data = tableModel.getData();
      for (let r in data) {
        var lot = qx.core.ObjectRegistry.fromHashCode(data[r][3]);
        lot.setId(data[r][0]);
        lot.setType(data[r][1]);
        lot.setNumero(data[r][2]);
      }
    },

    /**
    * Update table collections.
    * @param clone {prems.Clone} clone
    * @param table {table} table
    */
    __updateTableCol : function(clone, table) {
      var rows = [];
      clone.getCollections().forEach(function(col) {
        if (!col.getToDelete()) {
          rows.push(col.getRow());
        }
      }, this);
      var tableModel = table.getTableModel();
      tableModel.setDataAsMapArray(rows);
    },

    /**
    * Update table collections.
    * @param clone {prems.Clone} clone
    * @param table {table} table
    */
    __updateModelTableCol : function(clone, table) {
      var tableModel = table.getTableModel();
      var data = tableModel.getData();
      for (let r in data) {
        var collection = qx.core.ObjectRegistry.fromHashCode(data[r][1]);
        collection.setNom(data[r][0]);
      }
    },

    /**
    * Validate form.
    * @param tableLots {qx.ui.table.Table} table lots.
    * @param varieteTf {qx.ui.form.TextField} variete textfield.
    * @param numeroCheck {qx.ui.form.CheckBox} numero checkBox.
    * @param numeroTf {qx.ui.form.TextField} numero textfield.
    * @param fournisseurTf {qx.ui.form.TextField} fournisseur textfield.
    * @return  {Boolean} wether the form is valid or not.
    */
    __validate : function(tableLots, varieteTf, numeroCheck, numeroTf, fournisseurTf) {
      var returnValue = true;
      tableLots.resetTextColor();
      varieteTf.setValid(true);
      numeroTf.setValid(true);
      if (!tableLots.isExcluded()) {
        if (tableLots.getTableModel().getDataAsMapArray().length == 0) {
          tableLots.setTextColor("#EE3233");
          returnValue = false;
        }
      }
      if (varieteTf.isEnabled()) {
        if (varieteTf.getValue() === null || varieteTf.length == 0) {
          returnValue = false;
          varieteTf.setValid(false);
          varieteTf.setInvalidMessage(this.tr("Variété non renseignée."));
        }
      }
      if (!numeroCheck.getValue()) {
        if (numeroTf.getValue() === null || numeroTf.getValue().length == 0) {
          returnValue = false;
          numeroTf.setValid(false);
          numeroTf.setInvalidMessage(this.tr("Numéro non renseigné."));
        }
      }
      if (fournisseurTf.validate(false, this.tr("Fournisseur non valide.")) == false) {
        returnValue = false;
      }
      return returnValue;
    },

    /**
    * Enable button save.
    * @param buttonSave {qx.ui.form.Button} save button.
    */
    __enableButtonSave : function(buttonSave) {
      buttonSave.setEnabled(true);
      buttonSave.setIcon(null);
      buttonSave.setLabel(this.tr("Enregistrer"));
    },

    /**
    * Disable button save.
    * @param buttonSave {qx.ui.form.Button} save button.
    */
    __disableButtonSave : function(buttonSave) {
      buttonSave.setEnabled(false);
      buttonSave.setIcon("prems/loading.gif");
      buttonSave.setLabel(this.tr("Enregistrement"));
    }
  }
});
