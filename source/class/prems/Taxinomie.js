/**
 * Taxinomie object.
 * Stores all the informations of the "Taxinomie" table.
*/
qx.Class.define("prems.Taxinomie",
{
  extend : qx.core.Object,
  events :
  {
    /**
     * Fired when the taxinomy is saved in the database.
     */
    "saved" : "qx.event.type.Data",

    /**
     * Fired when the taxinomy is retrieved from the database.
     */
    "getEnd" : "qx.event.type.Data"
  },
  properties :
  {
    /**
    * id
    */
    id :
    {
      init : null,
      nullable : true,
      check : "Integer",
      event : "changeId"
    },

    /**
     * genre
     */
    genre :
    {
      init : null,
      nullable : true,
      check : "String",
      event : "changeGenre"
    },

    /**
    * espece
    */
    espece :
    {
      init : null,
      nullable : true,
      check : "String",
      event : "changeEspece"
    }
  },

  /**
   * constructor
   * @param idTaxinomie {Integer} id of the taxinomy if it exists in the database.
   */
  construct : function(idTaxinomie) {
    this.base(arguments);

    //Sets the variete id
    if (idTaxinomie) {
      this.__idTaxinomie = idTaxinomie;
    }

    //Service

    //this.__service = prems.service.Service.getInstance();
  },
  members :
  {
    /**
       * Saves (update or create)
       */
    save : function() {
    },

    /**
       * Get
       */
    get : function() {
    },

    /**
       * Create
       */
    create : function() {
    },

    /**
       * Update
       */
    update : function() {
    }
  }
});
