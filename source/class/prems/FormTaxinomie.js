/**
* Type lieu form
*/
qx.Class.define("prems.FormTaxinomie",
{
  extend : qx.ui.core.Widget,
  events : {
    /**
    * Fired when the taxinomie is saved.
    */
    "saved" : "qx.event.type.Event"
  },
  construct : function(idTaxinomie) {
    this.base(arguments);

    //Layouts
    this.__container = new qx.ui.container.Composite(new qx.ui.layout.VBox(20));
    this._setLayout(new qx.ui.layout.Grow());
    this._add(this.__container);

    //Service
    this.__service = prems.service.Service.getInstance();
    var form = new qx.ui.form.Form();
    this.__genreTf = new qx.ui.form.TextField();
    this.__especeTf = new qx.ui.form.TextField();
    form.add(this.__genreTf, this.tr("Genre"), null, "genre");
    form.add(this.__especeTf, this.tr("Espece"), null, "espece");
    var saveButton = new qx.ui.form.Button(this.tr("Enregistrer"));
    form.addButton(saveButton);
    saveButton.addListener("execute", function() {
      if (this.__validate()) {
        this.__save();
      }
    }, this);
    this.__container.add(new qx.ui.form.renderer.Single(form));
  },
  members :
  {
    /**
    * Check if a field is empty.
    *@param str {String} string to check.
    *@return {Boolean} whether the string is empty or not.
    */
    isEmpty : function(str) {
      return (!str || str.length === 0);
    },

    /**
    * Validate.
    *@return {Boolean} wether the form is valid or not.
    */
    __validate : function() {
      var returnValue = true;
      this.__genreTf.setValid(true);
      this.__especeTf.setValid(true);
      if (this.isEmpty(this.__genreTf.getValue())) {
        this.__genreTf.setValid(false);
        returnValue = false;
      }
      if (this.isEmpty(this.__genreTf.getValue())) {
        this.__especeTf.setValid(false);
        returnValue = false;
      }
      return returnValue;
    },

    /**
    * Save.
    */
    __save : function() {
      let genre = this.__genreTf.getValue();
      let espece = this.__especeTf.getValue();
      let service = null;
      if (this.__idTaxinomie) {
        service = this.__service.query("updateTaxinomie", [genre, espece, this.__idTaxinomie]);
      } else {
        service = this.__service.query("createTaxinomie", [genre, espece]);
      }
      service.addListener("changeResponseModel", function() {
        if (service.noError()) {
          this.fireEvent("saved");
          this.__service.fireEvent("dbChange");
        } else {
          new prems.Dialog("Erreur"); // TOD To be replaced by event
        }
      }, this);
    },

    /**
    * Set id taxinomie.
    *@param idTaxinomie {Integer} id taxinomie.
    */
    setId : function(idTaxinomie) {
      this.__idTaxinomie = idTaxinomie;
      if (this.__idTaxinomie !== null) {
        var service = this.__service.query("getTaxinomieById", [this.__idTaxinomie]);
        service.addListener("changeResponseModel", function() {
          this.__genreTf.setValue(service.getResult()[0]["genre"]);
          this.__especeTf.setValue(service.getResult()[0]["espece"]);
        }, this);
      }
    }
  }
});
