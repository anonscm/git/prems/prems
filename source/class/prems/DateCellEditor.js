/**
 * Date cell editor
 */
qx.Class.define("prems.DateCellEditor",
{
  extend : qx.ui.table.celleditor.AbstractField,
  members :
  {
    /**
       * Create editor
       *@return {qx.ui.form.DateField} dateField.
       */
    _createEditor : function() {
      var dateField = new qx.ui.form.DateField();
      var dateFormat = new qx.util.format.DateFormat("dd/MM/yyyy");
      dateField.setDateFormat(dateFormat);
      return dateField;
    },

    /**
       * createCellEditor
       * @param cellInfo {prems.AutoCompleteBox} cell info.
       * @return {qx.ui.form.DateField} cell editor.
       */
    createCellEditor : function(cellInfo) {
      var cellEditor = this._createEditor();
      cellEditor.originalValue = cellInfo.value;
      if (cellInfo.value === null || cellInfo.value === undefined) {
        cellInfo.value = new Date();
      }
      cellEditor.setValue(cellInfo.value);
      cellEditor.addListener("appear", function() {
        //cellEditor.selectAllText();
      });
      return cellEditor;
    }
  }
});
