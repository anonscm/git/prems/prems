/**
* Introduction multiplication object.
* Stores all the informations of the "Multiplication" table.
*/
qx.Class.define("prems.Multiplication", {
  extend : qx.core.Object,
  events : {
    /**
    * Fired when the multiplication is saved in the database.
    */
    "saved" : "qx.event.type.Event",

    /**
    * Fired when the multiplication is retrieved from the database.
    */
    "getEnd" : "qx.event.type.Event"
  },
  properties : {
    /**
    * id
    */
    id : {
      init : null,
      nullable : true,
      check : "Integer",
      event : "changeId"
    },

    /**
    * lot ascendant
    */
    lotAscendant : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeLotAscendant"
    },

    /**
    * lot descendant
    */
    lotDescendant : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeLotDescendant"
    },

    /**
    * conforme
    */
    conforme : {
      init : null,
      nullable : true,
      check : "Boolean",
      event : "changeConforme"
    },

    /**
    * Multiplicateur
    */
    multiplicateur : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeMultiplicateur"
    },

    /**
    * Remarque
    */
    remarque : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeRemarque"
    },

    /**
    * Nombre porte graines
    */
    nbPorteGraines : {
      init : null,
      nullable : true,
      check : "Integer",
      event : "nbPorteGraines"
    },

    /**
    * Quantité
    */
    quantite : {
      init : null,
      nullable : true,

      //check : "Float",
      event : "changeQuantite"
    }
  },

  /**
  * constructor
  * @param idMultiplication {Integer} id of the multiplication if it exists in the database.
  */
  construct : function(idMultiplication) {
    this.base(arguments);

    //Service
    this.__service = prems.service.Service.getInstance();

    //Sets the variete id
    if (idMultiplication) {
      this.setId(idMultiplication);
      this.get();
    }
  },
  members : {
    /**
    * Saves (update or create)
    */
    save : function() {
      //Multiplicateur
      var serviceMultiplicateur = this.__service.query("getPepinieristeByNom", [this.getMultiplicateur()]);
      serviceMultiplicateur.addListener("changeResponseModel", function() {
        //Recupere l'id multiplicateur
        if (serviceMultiplicateur.getResult().length > 0) {
          var multiplicateur = serviceMultiplicateur.getResult()[0]["id_pepinieriste"];
        } else {
          multiplicateur = null;
        }

        //Recupere groupe lot ascendant
        var serviceAscendant = this.__service.query("getGroupeByLot", [this.getLotAscendant()]);
        serviceAscendant.addListener("changeResponseModel", function() {
          var groupA = serviceAscendant.getResult()[0]["id_group"];

          //Recupere groupe lot descendant
          var serviceDescendant = this.__service.query("getGroupeByLot", [this.getLotDescendant()]);
          serviceDescendant.addListener("changeResponseModel", function() {
            var groupD = serviceDescendant.getResult()[0]["id_group"];

            //Crée ou met à jour la table multiplication
            if (this.getId() === null) {
              this.create(groupA, groupD, multiplicateur);
            } else {
              this.update(groupA, groupD, multiplicateur);
            }
          }, this);
        }, this);
      }, this);
    },

    /**
    * Get
    */
    get : function() {
      var service = this.__service.query("getMultiplicationById", [this.getId()]);
      service.addListener("changeResponseModel", function() {
        this.setConforme(service.getResult()[0]["conforme"]);
        this.setRemarque(service.getResult()[0]["remarque"]);
        this.setNbPorteGraines(service.getResult()[0]["nb_porte_graines"]);
        this.setQuantite(service.getResult()[0]["quantite_initiale"]);
        var idGroupeAscendant = service.getResult()[0]["grp_ascendant"];
        var serviceMultiplicateur = this.__service.query("getPepinieristeById", [service.getResult()[0]["multiplicateur"]]);
        serviceMultiplicateur.addListener("changeResponseModel", function() {
          if (serviceMultiplicateur.getResult().length > 0) {
            this.setMultiplicateur(serviceMultiplicateur.getResult()[0]["nom"]);
          }
          var serviceGroupe = this.__service.query("getLotByGroupe", [idGroupeAscendant]);
          serviceGroupe.addListener("changeResponseModel", function() {
            this.setLotAscendant(serviceGroupe.getResult()[0]["numero_lot"]);
            this.fireEvent("getEnd");
          }, this);
        }, this);
      }, this);
    },

    /**
    * Create
    * @param groupA {} numero group ascendant
    * @param groupD {Integer} numero group descendant
    * @param multiplicateur {String} multiplicateur
    */
    create : function(groupA, groupD, multiplicateur) {
      var service = this.__service.query("createMultiplication", [groupA, groupD, this.getConforme(), multiplicateur, this.getRemarque(), this.getNbPorteGraines(), this.getQuantite()]);
      service.addListener("changeResponseModel", function() {
        this.setId(service.getResult()[0]["id"]);
        this.fireEvent("saved");
      }, this);
    },

    /**
    * Update
    * @param groupA {} numero group ascendant
    * @param groupD {Integer} numero group descendant
    * @param multiplicateur {String} multiplicateur
    */
    update : function(groupA, groupD, multiplicateur) {
      var service = this.__service.query("updateMultiplication", [this.getId(), groupA, groupD, this.getConforme(), multiplicateur, this.getRemarque(), this.getNbPorteGraines(), this.getQuantite()]);
      service.addListener("changeResponseModel", function() {
        this.fireEvent("saved");
      }, this);
    }
  }
});
