/**
* Variete form
* FIXME Never user -> to be removed
*/
qx.Class.define("prems.NomVarieteForm",
{
  extend : qx.ui.core.Widget,
  construct : function(nom) {
    this.base(arguments);

    //Layouts
    var container = new qx.ui.container.Composite(new qx.ui.layout.VBox(20));
    this._setLayout(new qx.ui.layout.Grow());
    this._add(container);

    //Service

    //this.__service = prems.service.Service.getInstance();
    var boxInfos = new qx.ui.groupbox.GroupBox(this.tr(""));
    boxInfos.setLayout(new qx.ui.layout.VBox());
    container.add(boxInfos);

    //Form
    var form = new qx.ui.form.Form();

    //Type
    var typeTf = new prems.AutoCompleteBox();

    //this.__service.getAutoCompleteModel("getAllGenre", [], "genre", this.__genre);
    form.add(typeTf, this.tr("Type nom"), null, "type");
    nom.bind("type", typeTf, "value");
    typeTf.bind("value", nom, "type");

    ///Nom
    var nomTf = new prems.AutoCompleteBox();

    //this.__service.getAutoCompleteModel("getAllNom", [], "espece", especeTf);
    form.add(nomTf, this.tr("Nom"), null, nomTf);
    nom.bind("nom", nomTf, "value");
    nomTf.bind("value", nom, "nom");

    //Date
    var dateTf = new qx.ui.form.DateField();
    form.add(dateTf, this.tr("Date"), null, "date");
    nom.bind("date", dateTf, "value");
    dateTf.bind("value", nom, "date");
    var formatDate = new qx.util.format.DateFormat("dd/MM/YYYY");
    dateTf.setDateFormat(formatDate);

    //Bouton enregistrer
    var buttonSave = new qx.ui.form.Button(this.tr("Enregistrer"));
    buttonSave.setAllowStretchX(false);
    buttonSave.setAlignX("right");
    form.addButton(buttonSave);
    buttonSave.addListener("execute", function() {
    }, this);
    boxInfos.add(new qx.ui.form.renderer.Single(form));
    container.add(buttonSave);
  },
  members : {

  }
});
