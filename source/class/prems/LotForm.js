/**
* Lot form
*/
qx.Class.define("prems.LotForm", {
  extend : qx.ui.core.Widget,

  /**
  * @param noClone {Boolean} whether to show the clone infos.
  * @param lot {prems.Lot} lot
  */
  construct : function(lot, noClone) {
    this.base(arguments);

    //Layouts
    var container = new qx.ui.container.Composite(new qx.ui.layout.VBox(10));
    this._setLayout(new qx.ui.layout.VBox(20));
    this._add(container);
    var session = qxelvis.session.service.Session.getInstance();
    var userGroups = session.getUserGroupsInfo().getReadGroupIds();
    var tabView = new qx.ui.tabview.TabView();
    tabView.setWidth(750);
    container.add(tabView);
    var pageLot = new qx.ui.tabview.Page(this.tr("Lot"));
    pageLot.setLayout(new qx.ui.layout.VBox(10));
    tabView.add(pageLot);
    var pageNotation = new qx.ui.tabview.Page(this.tr("Notations"));
    pageNotation.setLayout(new qx.ui.layout.VBox(10));
    tabView.add(pageNotation);

    var tableNotation = new prems.TableNotations(lot);
    pageNotation.add(tableNotation);
    tableNotation.exclude();

    //Service
    this.__service = prems.service.Service.getInstance();
    var boxInfos = new qx.ui.groupbox.GroupBox(this.tr("Informations"));
    boxInfos.setLayout(new qx.ui.layout.VBox());
    pageLot.add(boxInfos);

    //Form Lot
    var form = new qx.ui.form.Form();
    var formMulti = new qx.ui.form.Form();
    var formEmpl = new qx.ui.form.Form();

    //Accession
    var cloneTf = new prems.AutoCompleteBox();
    this.__service.getAutoCompleteModel("getAllNumeroClone", [userGroups], "numero", cloneTf);
    form.add(cloneTf, this.tr("Accession"), null, "clone");
    lot.bind("clone", cloneTf, "value");
    cloneTf.bind("value", lot, "clone");
    if (noClone == true) {
      cloneTf.setEnabled(false);
    }

    //Numéro
    var numeroTf = new qx.ui.form.TextField();
    form.add(numeroTf, this.tr("Numéro"), null, "numero");
    lot.bind("numero", numeroTf, "value");
    numeroTf.bind("value", lot, "numero");
    if (lot.getId() === null) {
      numeroTf.exclude();
    }
    lot.addListener("changeNumero", function() {
      numeroTf.show();
    }, this);
    var boxMultiplication = new qx.ui.groupbox.CheckGroupBox(this.tr("Multiplication"));
    boxMultiplication.setLayout(new qx.ui.layout.VBox());
    boxMultiplication.setValue(false);
    var multiplicateurTf = new prems.AutoCompleteBox();
    var lotPereTf = new prems.AutoCompleteBox();

    //Box mutplication
    if (lot.getType() == "graines") {
      //Infos

      //Equilibre
      var equilibreCb = new qx.ui.form.CheckBox();
      form.add(equilibreCb, this.tr("Équilibre"), null, "equilibre");
      lot.bind("equilibre", equilibreCb, "value");
      equilibreCb.bind("value", lot, "equilibre");

      //Date récolte
      var dateRecolteDf = new qx.ui.form.DateField();
      form.add(dateRecolteDf, this.tr("Date récolte"), null, "dateRecolte");
      lot.bind("dateRecolte", dateRecolteDf, "value");
      dateRecolteDf.bind("value", lot, "dateRecolte");
      var dateFormat = new qx.util.format.DateFormat("dd/MM/yyyy");
      dateRecolteDf.setDateFormat(dateFormat);

      //Rang évaluation
      var rangTf = new qx.ui.form.Spinner(0, 0, 100);
      form.add(rangTf, this.tr("Rang évaluation"), null, "rangEvaluation");
      lot.bind("rangEvaluation", rangTf, "value");
      rangTf.bind("value", lot, "rangEvaluation");
      var nf = new qx.util.format.NumberFormat();
      nf.setMinimumFractionDigits(2);
      nf.setMaximumFractionDigits(2);

      //Quantite
      var quantiteTf = new qx.ui.form.Spinner(0, 0, 100);
      form.add(quantiteTf, this.tr("Quantité"), null, "quantite");
      lot.bind("quantite", quantiteTf, "value");
      quantiteTf.bind("value", lot, "quantite");
      quantiteTf.setNumberFormat(nf);

      //Multiplication
      var multiplication = lot.getMultiplication();
      if (multiplication !== null) {
        boxMultiplication.setValue(true);
      } else {
        multiplication = new prems.Multiplication();
      }
      boxMultiplication.addListener("changeValue", function(e) {
        if (e.getData() == true) {
          lot.setMultiplication(multiplication);
          rendererMulti.show();
        }
        if (e.getData() == false) {
          lot.setMultiplication(null);
          rendererMulti.exclude();
        }
      }, this);
      pageLot.add(boxMultiplication);

      //Lot père
      this.__service.getAutoCompleteModel("getAllLot", [userGroups], "numero_lot", lotPereTf);
      formMulti.add(lotPereTf, this.tr("Lot père"), null, "lotPere");
      multiplication.bind("lotAscendant", lotPereTf, "value");
      lotPereTf.bind("value", multiplication, "lotAscendant");

      //Multiplicateur
      this.__service.getAutoCompleteModel("getAllPepinieriste", [], "nom", multiplicateurTf);
      formMulti.add(multiplicateurTf, this.tr("Multiplicateur"), null, "multiplicateur");
      multiplication.bind("multiplicateur", multiplicateurTf, "value");
      multiplicateurTf.bind("value", multiplication, "multiplicateur");

      //Quantité initiale
      var quantiteTf2 = new qx.ui.form.Spinner(0, 0, 1000);
      formMulti.add(quantiteTf2, this.tr("Quantité"), null, "quantite");
      multiplication.bind("quantite", quantiteTf2, "value");
      quantiteTf2.bind("value", multiplication, "quantite");
      quantiteTf2.setHeight(multiplicateurTf.getHeight());

      //Porte graines
      var nbPorteGrainesTf = new qx.ui.form.Spinner(0, 0, 1000);
      formMulti.add(nbPorteGrainesTf, this.tr("Nombre porte-graines"), null, "nbPorteGraines");
      multiplication.bind("nbPorteGraines", nbPorteGrainesTf, "value");
      nbPorteGrainesTf.bind("value", multiplication, "nbPorteGraines");

      //Conforme
      var conformeCb = new qx.ui.form.CheckBox();
      formMulti.add(conformeCb, this.tr("Conforme"), null, "conforme");
      multiplication.bind("conforme", conformeCb, "value");
      conformeCb.bind("value", multiplication, "conforme");

      //Blank
      var blank = new qx.ui.form.CheckBox();
      formMulti.add(blank, "");
      blank.exclude();

      //Remaruqe multiplication
      var remarqueTf = new qx.ui.form.TextArea();
      formMulti.add(remarqueTf, this.tr("Remarque"), null, "remarque");
      multiplication.bind("remarque", remarqueTf, "value");
      remarqueTf.bind("value", multiplication, "remarque");
      quantiteTf.setNumberFormat(nf);
      quantiteTf2.setNumberFormat(nf);
    }
    var emplacement = lot.getEmplacement();
    var boxEmplacement = new qx.ui.groupbox.GroupBox(this.tr("Emplacement"));
    boxEmplacement.setLayout(new qx.ui.layout.VBox(10));
    pageLot.add(boxEmplacement);

    //Site
    var siteTf = new prems.AutoCompleteBox();
    this.__service.getAutoCompleteModel("getAllSites", [], "nom_site", siteTf);
    formEmpl.add(siteTf, this.tr("Site"), null, "site");
    emplacement.bind("site", siteTf, "value");
    siteTf.bind("value", emplacement, "site");
    boxEmplacement.add(new qx.ui.form.renderer.Double(formEmpl));

    // Customize the table column model.  We want one that automatically

    // resizes columns.
    var custom = {
      tableColumnModel : function(obj) {
        return new qx.ui.table.columnmodel.Resize(obj);
      }
    };
    var tableModel = new qx.ui.table.model.Simple();
    tableModel.setColumnIds(["id", "type", "valeur", "hash"]);
    tableModel.setColumnNamesByIndex(["Id", "Type", "Valeur", "Hash"]);
    var tableLieux = new qx.ui.table.Table(tableModel, custom);
    tableLieux.setHeight(120);
    tableLieux.setStatusBarVisible(false);
    tableLieux.getTableColumnModel().setColumnVisible(0, false);
    tableLieux.getTableColumnModel().setColumnVisible(3, false);
    tableModel.setColumnEditable(1, true);
    tableModel.setColumnEditable(2, true);
    var tcm = tableLieux.getTableColumnModel();
    var autoCompleteEditor = new prems.AutoCompleteCellEditor("typeLieu");
    tcm.setCellEditorFactory(1, autoCompleteEditor);
    boxEmplacement.add(tableLieux);

    // Resize the table columns
    var resizeBehavior = tcm.getBehavior();
    resizeBehavior.set(0, {
      width : 40
    });
    var layoutButtons = new qx.ui.container.Composite(new qx.ui.layout.HBox(10));


    //Bouton ajout lieu
    var buttonAddLieu = new qx.ui.form.Button(this.tr("+"));
    layoutButtons.add(buttonAddLieu);
    buttonAddLieu.setAllowStretchX(false);
    buttonAddLieu.setAlignX("right");
    var command = new qx.ui.command.Command("Delete");
    var buttonRemoveLieu = new qx.ui.form.Button(this.tr("-"), null, command);
    command.addListener("execute", function() {
      try {
        var line = tableLieux.getSelectionModel().getSelectedRanges()[0]["maxIndex"];
        var hash = tableLieux.getTableModel().getDataAsMapArray()[line]["hash"];
        var lieu = qx.core.ObjectRegistry.fromHashCode(hash);
        lieu.setToDelete(true);
        emplacement.fireDataEvent("changeLieux", null);
      } catch (e) {
      }
    }, this);
    layoutButtons.add(buttonRemoveLieu);
    buttonRemoveLieu.setAllowStretchX(false);
    buttonRemoveLieu.setAlignX("right");
    buttonAddLieu.addListener("execute", function() {
      emplacement.addLieu(new prems.Lieu());
    }, this);
    boxEmplacement.add(layoutButtons);

    //Update de la liste des lieux
    this.__updateTableLieux(emplacement, tableLieux);
    emplacement.addListener("changeLieux", function() {
      this.__updateTableLieux(emplacement, tableLieux);
    }, this);
    tableModel.addListener("dataChanged", function(e) {
      this.__updateModelTableLieux(emplacement, tableLieux);
    }, this);
    var popup = new qx.ui.popup.Popup(new qx.ui.layout.Canvas());
    popup.add(new qx.ui.basic.Atom(this.tr("Lot enregistré")));

    //Bouton enregistrer
    var buttonSave = new qx.ui.form.Button(this.tr("Enregistrer"));
    buttonSave.setAllowStretchX(false);
    buttonSave.setAlignX("right");
    buttonSave.addListener("execute", function() {
      this.__disableButtonSave(buttonSave);
      if (this.__validate(cloneTf, siteTf, lot.getMultiplication(), lotPereTf, multiplicateurTf)) {
        if (!cloneTf.isEnabled()) {
          lot.fireEvent("saved");
          popup.placeToWidget(buttonSave);
          popup.show();
        } else {
          lot.save();
          lot.addListenerOnce("saved", function() {
            this.__enableButtonSave(buttonSave);
            popup.placeToWidget(buttonSave);
            popup.show();
          }, this);
        }
      } else {
        this.__enableButtonSave(buttonSave);
      }
    }, this);
    lot.addListener("saved", function() {
      buttonSave.setEnabled(true);
      buttonSave.setLabel(this.tr("Enregistrer"));
    }, this);
    boxInfos.add(new qx.ui.form.renderer.Double(form));
    var rendererMulti = new qx.ui.form.renderer.Double(formMulti);
    boxMultiplication.add(rendererMulti);
    container.add(buttonSave);

    //regle la taille de la fenetre en fonction de l'onglet selectionné
    tabView.addListener("changeSelection", function(e) {
      var selectedPage = tabView.getSelection()[0];
      var title = selectedPage.getLabel().getMessageId();
      if (title == "Lot") {
        tableNotation.exclude();
      } else {
        tableNotation.show();
      }
    }, this);
    this.__service.addListener("dbChange", function() {
      this.__update(cloneTf, siteTf, multiplicateurTf, lotPereTf);
    }, this);
  },
  members :
  {
    /**
    * update table lieux
    * @param emplacement {prems.Emplacement} emplacement
    * @param table {qx.ui.table.Table} table
    */
    __updateTableLieux : function(emplacement, table) {
      var rows = [];
      emplacement.getLieux().forEach(function(lieu) {
        if (lieu.getToDelete() == false) {
          rows.push(lieu.getRow());
        }
      }, this);
      var tableModel = table.getTableModel();
      tableModel.setDataAsMapArray(rows);
    },

    /**
    * update model table lieux
    * @param emplacement {prems.Emplacement} emplacement
    * @param table {qx.ui.table.Table} table
    */
    __updateModelTableLieux : function(emplacement, table) {
      var tableModel = table.getTableModel();
      var data = tableModel.getData();
      for (var r in data) {
        var lieu = qx.core.ObjectRegistry.fromHashCode(data[r][3]);
        lieu.setType(data[r][1]);
        lieu.setValeur(data[r][2]);
      }
    },

    /**
    * update table lieux
    * @param cloneTf {qx.ui.form.TextField} clone textfield
    * @param siteTf {qx.ui.form.TextField} site textfield
    * @param multiplication {prems.Multiplication} multiplication
    * @param lotPereTf {qx.ui.form.TextField} lot pere textfield
    * @param multiplicateurTf {qx.ui.form.TextField} multiplicateur textfield
    * @return {Boolean} whether the form is valid or not.
    */
    __validate : function(cloneTf, siteTf, multiplication, lotPereTf, multiplicateurTf) {
      var siteMsg = this.tr("Ce site n'existe pas dans la base de données.");
      var cloneMsg = this.tr("Cette accession n'existe pas dans la base de données.");
      var lotMsg = this.tr("Cet lot n'existe pas dans la base de données.");
      var contactMsg = this.tr("Ce contact n'existe pas dans la base de données.");
      if (!cloneTf.isEnabled()) {
        if (multiplication === null) {
          return (siteTf.validate(false, siteMsg));
        }
        return (siteTf.validate(false, siteMsg) && cloneTf.validate(true, cloneMsg) && lotPereTf.validate(true, lotMsg) && multiplicateurTf.validate(true, contactMsg));
      }
      if (multiplication === null) {
        return (siteTf.validate(false, siteMsg) && cloneTf.validate(true, cloneMsg));
      }
      return (siteTf.validate(false, siteMsg) && cloneTf.validate(true, cloneMsg) && lotPereTf.validate(true, lotMsg) && multiplicateurTf.validate(false, contactMsg));
    },

    /**
    * update
    * @param cloneTf {qx.ui.form.TextField} clone textfield
    * @param siteTf {qx.ui.form.TextField} site textfield
    * @param multiplicateurTf {qx.ui.form.TextField} multiplicateur textfield
    * @param lotPereTf {qx.ui.form.TextField} lot pere textfield
    */
    __update : function(cloneTf, siteTf, multiplicateurTf, lotPereTf) {
      var session = qxelvis.session.service.Session.getInstance();
      var userGroups = session.getUserGroupsInfo().getReadGroupIds();
      this.__service.getAutoCompleteModel("getAllNumeroClone", [userGroups], "numero", cloneTf);
      this.__service.getAutoCompleteModel("getAllSites", [], "nom_site", siteTf);
      this.__service.getAutoCompleteModel("getAllPepinieriste", [], "nom", multiplicateurTf);
      this.__service.getAutoCompleteModel("getAllLot", [userGroups], "numero_lot", lotPereTf);
    },

    /**
    * enable button save
    * @param buttonSave {qx.ui.form.Button} save button
    */
    __enableButtonSave : function(buttonSave) {
      buttonSave.setEnabled(true);
      buttonSave.setIcon(null);
      buttonSave.setLabel(this.tr("Enregistrer"));
    },

    /**
    * disable button save
    * @param buttonSave {qx.ui.form.Button} save button
    */
    __disableButtonSave : function(buttonSave) {
      buttonSave.setEnabled(false);
      buttonSave.setIcon("prems/loading.gif");
      buttonSave.setLabel(this.tr("Enregistrement"));
    }
  }
});
