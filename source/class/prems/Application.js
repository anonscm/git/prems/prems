/* ************************************************************************
   Copyright: INRA 2016
   License: CeCILL
   Authors: Aurélie Lelièvre
            Sylvain Gaillard

************************************************************************ */

/**
 * "prems" application
 *
 * @asset(prems/*)
 */
qx.Class.define("prems.Application",
{
  extend : qx.application.Standalone,

  /*
  *****************************************************************************
     MEMBERS
  *****************************************************************************
  */
  members : {
    main : function() {
      this.base(arguments);

      // Enable logging in debug variant
      if (qx.core.Environment.get("qx.debug")) {
        // support native logging capabilities, e.g. Firebug for Firefox
        qx.log.appender.Native;

        // support additional cross-browser console. Press F7 to toggle visibility
        qx.log.appender.Console;
      }

      /*
      ---------------------------------
      Global configuration and services
      ---------------------------------
      */

      // Load application Config
      prems.Config.getInstance();

      //instanciation du singleton session and services
      var session = qxelvis.session.service.Session.getInstance();
      //Fix date problem
      // TODO is this realy needed or can we pass date in string format?
      qx.io.remote.Rpc.CONVERT_DATES = true;

      /*
      -------------------
      Draw main interface
      -------------------
      */

      // Application header
      var version = qx.core.Environment.get("qx.libraryInfoMap")["prems"]["version"];
      var header = new prems.ui.AppHeader("The Plant Ressouce Manager System", version);
      this.getRoot().add(header, {top : 0, left : 0, right : 0});

      // Main space of the app
      var mainSpace = new qx.ui.container.Composite(new qx.ui.layout.HBox());

      // Test left paner
      var leftPanel = new qxelvis.ui.CollapsiblePanel();
      leftPanel.setHandleSide("right");
      leftPanel.collapse();
      mainSpace.add(leftPanel);

      // Desktop to display windows
      var windowManager = new qx.ui.window.Manager();
      var desktop = new qx.ui.window.Desktop(windowManager);
      desktop.set(
      {
        decorator : "main",
        backgroundColor : "background-pane"
      });
      mainSpace.add(desktop, {flex: 1});
      var rightPanel = new qxelvis.ui.CollapsiblePanel();
      mainSpace.add(rightPanel);
      this.getRoot().add(mainSpace, { top : 94, bottom : 24, right : 0, left : 0 });

      // Activity bar to display open windows
      var toolBar = new qx.ui.menubar.MenuBar();
      toolBar.setHeight(24);
      this.getRoot().add(toolBar, { bottom : 0, right : 0, left : 0 });
      //localStorage.setItem('toolBarHash', toolBar.toHashCode());

      // Top menu bar
      var menuBar = new prems.ui.Menu(desktop, toolBar);
      this.getRoot().add(menuBar, { left : 0, top : 70, right : 0 });

      /*
      -----------------
      Application parts
      -----------------
      */

      var loginWin = new qx.ui.window.Window(this.tr("Login"));
      var loginForm = new qxelvis.ui.LoginForm();
      loginWin.setLayout(new qx.ui.layout.Grow());
      loginWin.add(loginForm);
      loginWin.setModal(true);

      var groupWidget = new qxelvis.ui.groups.GroupList();
      groupWidget.setMinWidth(200);

      var grpTaxa = new qx.ui.groupbox.GroupBox(this.tr("Taxa"));
      grpTaxa.setLayout(new qx.ui.layout.Grow());
      rightPanel.add(grpTaxa);

      var grpGroups = new qx.ui.groupbox.GroupBox(this.tr("Groups"));
      grpGroups.setLayout(new qx.ui.layout.Grow());
      grpGroups.add(groupWidget);
      rightPanel.add(grpGroups);

      /*
      ---------------
      Action bindings
      ---------------
      */

      // Session and login
      menuBar.addListener("wantLogin", function() {
        loginWin.center();
        loginWin.show();
      }, this);
      menuBar.addListener("wantUserAccount", function() {
        if (rightPanel.collapsed()) {
          rightPanel.expend();
        } else {
          rightPanel.collapse();
        }
      }, this);
      session.addListener("gotUserInfo", function() {
        if (!session.userLoggedIn()) {
          loginWin.center();
          loginWin.show();
        }
      }, this);
      session.addListener("authenticationSucced", function() {
        loginForm.reset();
      }, this);
      session.addListener("authenticationFailed", function() {
        loginForm.setValid(false);
      }, this);
      loginForm.addListener("validate", function(e) {
        var lg = e.getData()["username"];
        var pw = e.getData()["password"];
        session.userLogin(lg, pw);
      }, this);
      loginForm.addListener("reset", function() {
        loginWin.close();
      }, this);

      // Main menu
      menuBar.addListener("newVariety", function() {
        var variete = new prems.Variete(null);
        var formVariete = new prems.VarieteForm(variete);
        var win = this.newWindow(this.tr("Nouvelle variété"), formVariete, desktop, toolBar);
        variete.addListener("saved", function() {
          win.close();
        }, this);
      }, this);
      menuBar.addListener("newAccession", function() {
        var clone = new prems.Clone(null);
        var formClone = new prems.CloneForm(clone);
        var win = this.newWindow(this.tr("Nouvelle accession"), formClone, desktop, toolBar);
        clone.addListener("saved", function() {
          win.close();
        }, this);
      }, this);
      menuBar.addListener("newTreeLot", function() {
        var lot = new prems.Lot(null, "arbre");
        var win = this.newWindow(this.tr("Nouveau lot"), new prems.LotForm(lot, "arbre"), desktop, toolBar);
      }, this);
      menuBar.addListener("newSeedLot", function() {
        var lot = new prems.ui.plant.CreateLot("graine");
        var win = this.newWindow(this.tr("Nouveau lot de graine"), lot, desktop, toolBar);
        lot.addListener("saveDraft", function(e) {
          var store = qx.bom.Storage.getLocal();
          store.setItem("drafts", e.getData());
        }, this);
        lot.loadDraft();
      }, this);
      menuBar.addListener("newLotNotation", function() {
        var notation = new prems.Notation();
        var formNota = new prems.NotationForm(notation);
        var win = this.newWindow(this.tr("Nouvelle notation"), formNota, desktop, toolBar);
        notation.addListener("saved", function() {
          win.close();
        }, this);
      }, this);
      menuBar.addListener("importMaterial", function() {
        var win = this.newWindow(this.tr("Import de matériel végétal"), new prems.InsertMateriel(), desktop, toolBar);
      }, this);
      menuBar.addListener("importNotations", function() {
        var win = this.newWindow(this.tr("Import de notations"), new prems.InsertNotations(), desktop, toolBar);
      }, this);
      menuBar.addListener("searchMaterial", function() {
        var searchW = new prems.ui.GlobalSearch();
        var win = this.newWindow(this.tr("Recherche"), searchW, desktop, toolBar);
        searchW.addListener("wantsHelp", function() {
          let help = new prems.Help();
          let win = this.newWindow(this.tr("Aide"), help, desktop, toolBar);
        }, this);
        searchW.addListener("displayInformations", function(e) {
          var tree = new prems.TreeVariete();
          tree.setCultivated(e.getData()["cultivated"]);
          tree.setGenus(e.getData()["genus"]);
          tree.setSpecie(e.getData()["specie"]);
          tree.setIdVariete(Number(e.getData()["variete"]));
          tree.setInitialFocus(e.getData()["ligneTable"]);
          var win = this.newWindow(this.tr("Informations "), tree, desktop, toolBar);
        }, this);
      }, this);
      menuBar.addListener("viewSummary", function() {
        var win = this.newWindow(this.tr("Bilan"), new prems.Stats(), desktop, toolBar);
        win.setWidth(300);
        win.setHeight(200);
      }, this);
      menuBar.addListener("exportDescriptors", function() {
        var win = this.newWindow(this.tr("Export descripteurs"), new prems.ExportDescripteurs(), desktop, toolBar);
      }, this);
      menuBar.addListener("viewContacts", function() {
        var win = this.newWindow(this.tr("Contacts"), new prems.PepinieristeDescripteur(), desktop, toolBar);
      }, this);
      menuBar.addListener("viewNameTypes", function() {
        var win = this.newWindow(this.tr("Types noms"), new prems.TypeNomDescripteur(), desktop, toolBar);
      }, this);
      menuBar.addListener("viewPlaceTypes", function() {
        var win = this.newWindow(this.tr("Types lieu"), new prems.TypeLieuDescripteur(), desktop, toolBar);
      }, this);
      menuBar.addListener("viewTaxonomy", function() {
        var win = this.newWindow(this.tr("Taxinomie"), new prems.TaxinomieDescripteur(), desktop, toolBar);
      }, this);
      menuBar.addListener("viewCollections", function() {
        var win = this.newWindow(this.tr("Collections"), new prems.CollectionDescripteur(), desktop, toolBar);
      }, this);
      menuBar.addListener("viewNotationTypes", function() {
        var win = this.newWindow(this.tr("Type notation"), new prems.TypeNotationDescripteur(), desktop, toolBar);
      }, this);
      menuBar.addListener("viewSites", function() {
        var win = this.newWindow(this.tr("Sites"), new prems.SiteDescripteur(), desktop, toolBar);
      }, this);
    },

    /**
     * Creates a new window.
     *
     * @param winTitle {String} title of the window.
     * @param object {qx.ui.core.Widget} Object to display in the window.
     * @param desktop {qx.ui.window.Desktop} The desktop where the window is attached.
     * @param dock {qx.ui.menubar.MenuBar} The dock where the window is attached.
     * @return {prems.MyWindow} window
     */
    newWindow : function(winTitle, object, desktop, dock) {
      var win = new prems.MyWindow(winTitle, null, dock);
      desktop.add(win);
      win.setLayout(new qx.ui.layout.Grow());
      win.add(object);
      win.center();
      win.open();
      return win;
    }
  }
});
