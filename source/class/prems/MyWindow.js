/**
   * My window
   */
qx.Class.define("prems.MyWindow",
{
  extend : qx.ui.window.Window,

  /**
   * Create a new prem.MyWindow
   *
   * That window can have a dock button if the param dock is set to a qx.ui.menubar.MenuBar
   *
   * @param caption {String} The caption text
   * @param icon {String} The URL of the caption icon
   * @param dock {qx.ui.menubar.MenuBar} The dock where the window should be attached
   */
  construct : function(caption, icon, dock) {
    this.base(arguments, caption, icon);
    if (dock !== null) {
      //Bouton
      var winButton = new qx.ui.menubar.Button(caption, icon);

      dock.add(winButton);

      //Menu contextuel
      var menu = new qx.ui.menu.Menu();
      var closeButton = new qx.ui.menu.Button(this.tr("Close"));
      menu.add(closeButton);
      winButton.setContextMenu(menu);

      //Close button listener
      closeButton.addListener("execute", function() {
        this.close();
        winButton.destroy();
      }, this);

      //Minimize or open
      winButton.addListener("execute", function() {
        if (this.getVisibility() == "visible") {
          this.minimize();
        } else {
          this.open();
        }
      }, this);

      //close
      this.addListener("close", function() {
        winButton.destroy();
      }, this);
    }
  }
});
