/**
 * Clone object.
 * Stores all the informations of the "Clone" table.
*/
qx.Class.define("prems.Clone",
{
  extend : qx.core.Object,
  events : {
    /**
    * Fired when the introduction is saved in the database.
    */
    "saved" : "qx.event.type.Event",

    /**
    * Fired when the introduction is retrieved from the database.
    */
    "getEnd" : "qx.event.type.Data"
  },
  properties : {
    /**
    * id
    */
    id : {
      init : null,
      nullable : true,
      check : "Integer",
      event : "changeId"
    },
    /**
    * Numéro
    */
    numero : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeNumero"
    },
    /**
    * Numéro auto
    */
    numeroAuto : {
      init : null,
      nullable : true,
      check : "Boolean",
      event : "changeNumeroAuto"
    },
    /**
    * Nom
    */
    nom : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeNom"
    },
    /**
    * nom variete
    */
    nomVariete : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeNomVariete"
    },
    /**
    * id variete
    */
    idVariete : {
      init : null,
      nullable : true,
      check : "Integer",
      event : "changeIdVariete"
    },
    /**
    * Remarque
    */
    remarque : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeRemarque"
    },
    /**
    * Date introduction
    */
    dateIntro : {
      init : null,
      nullable : true,
      check : "Date",
      event : "changeDateIntro"
    },
    /**
    * Fournisseur
    */
    fournisseur : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeFournisseur"
    },
    /**
    * Fournisseur id
    */
    idFournisseur : {
      init : null,
      nullable : true,
      check : "Integer",
      event : "changeIdFournisseur"
    },
    /**
    * Introduction
    */
    introductionClone : {
      init : null,
      nullable : true,
      check : "prems.IntroductionClone",
      event : "changeIntroductionClone"
    },
    /**
    * Introduction id
    */
    introductionCloneId : {
      init : null,
      nullable : true,
      check : "Integer",
      event : "changeIntroductionCloneId"
    },
    /**
    * Lots
    */
    lots : {
      init : new qx.data.Array(),
      check : "Array",
      event : "changeLots"
    },
    /**
    * Collections
    */
    collections : {
      init : new qx.data.Array(),
      check : "Array",
      event : "changeCollections"
    },
    /**
    * Variété object
    */
    variete : {
      init : new prems.Variete(),
      event : "changeVariete"
    }
  },

  /**
  * constructor
  * @param idClone {Integer} id of the introduction if it exists in the database.
  */
  construct : function(idClone) {
    this.base(arguments);
    //Service
    this.__service = prems.service.Service.getInstance();
    this.setLots(new qx.data.Array());
    this.setCollections(new qx.data.Array());

    //Sets the variete id
    if (idClone) {
      this.setId(idClone);
      this.get();
    }

    //    this.addListener("saved", function(){
    //      new prems.Dialog("Accession enregistrée");
    //      this.__service.fireEvent("dbChange");
    //    },this);
  },
  members : {
    /**
    * Saves (update or create)
    */
    save : function() {
      if (this.getIdVariete() === null) {
        var session = qxelvis.session.service.Session.getInstance();
        var userGroups = session.getUserGroupsInfo().getReadGroupIds();
        var serviceVariete = this.__service.query("getVarieteByNom", [this.getNomVariete(), userGroups]);
        serviceVariete.addListener("changeResponseModel", function() {
          if (serviceVariete.getResult().length > 0) {
            this.setIdVariete(serviceVariete.getResult()[0]["id_var"]);
            this.saveIntroductionClone();
          } else {
            var variete = new prems.Variete();
            var nomVariete = new prems.NomVariete();
            nomVariete.setNom(this.getNomVariete());
            variete.addNom(nomVariete);
            variete.save();
            variete.addListener("saved", function() {
              this.setIdVariete(variete.getId());
              this.saveIntroductionClone();
            }, this);
          }
        }, this);
      } else {
        this.saveIntroductionClone();
      }
    },

    /**
    * Get from the database.
    */
    get : function() {
      var serviceClone = this.__service.query("getCloneById", [this.getId()]);
      serviceClone.addListener("changeResponseModel", function() {
        this.setNumero(serviceClone.getResult()[0]["numero"]);
        this.setIdVariete(serviceClone.getResult()[0]["variete"]);
        this.setRemarque(serviceClone.getResult()[0]["remarque"]);
        this.setDateIntro(serviceClone.getResult()[0]["date_intro"]);
        this.setNom(serviceClone.getResult()[0]["nom"]);
        var idFournisseur = serviceClone.getResult()[0]["fournisseur"];
        var serviceFournisseur = this.__service.query("getPepinieristeById", [idFournisseur]);
        serviceFournisseur.addListener("changeResponseModel", function() {
          if (serviceFournisseur.getResult().length > 0) {
            this.setFournisseur(serviceFournisseur.getResult()[0]["nom"]);
          }

          //Introduction clone
          var idIntroduction = null;
          try {
            idIntroduction = serviceClone.getResult()[0]["introduction_clone"];
          } catch (e) {
            this.debug("Failed to get introduction id");
          }
          this.getIntroductionCloneInfos(idIntroduction);
        }, this);
      }, this);
    },

    /**
    * Get from the introduction clone table.
    * @param idIntroduction {Integer} id introduction
    */
    getIntroductionCloneInfos : function(idIntroduction) {
      if (idIntroduction !== null) {
        var introductionClone = new prems.IntroductionClone(idIntroduction);
        introductionClone.addListener("getEnd", function() {
          this.setIntroductionClone(new prems.IntroductionClone(idIntroduction));
          this.getCollectionsInfos();
        }, this);
      } else {
        this.getCollectionsInfos();
      }
    },

    /**
    * Get from the collection table.
    */
    getCollectionsInfos : function() {
      var collections = new qx.data.Array();
      var service = this.__service.query("getCollectionsByClone", [this.getId()]);
      service.addListener("changeResponseModel", function() {
        for (let i in service.getResult()) {
          var collection = new prems.Collection();
          collection.setNom(service.getResult()[i]["nom"]);
          collections.push(collection);
        }
        this.setCollections(collections);
        this.getVarieteObj();
      }, this);
    },

    /**
    * Get from the lot table.
    */
    getLotsInfos : function() {
      var lots = new qx.data.Array();
      var service = this.__service.query("getLotByClone", [this.getId()]);
      service.addListener("changeResponseModel", function() {
        if (service.getResult().length == 0) {
          this.fireDataEvent("getEnd", this);
        } else {
          var cpt = 0;
          for (let i in service.getResult()) {
            var lot = new prems.Lot(service.getResult()[i]["id"], service.getResult()[i]["type"]);
            lots.push(lot);
            lot.addListener("getEnd", function() {
              cpt++;
              if (cpt == service.getResult().length) {
                this.setLots(lots);
                this.fireDataEvent("getEnd", this);
              }
            }, this);
          }
        }
      }, this);
    },

    /**
    * Saves the introduction clone table.
    */
    saveIntroductionClone : function() {
      if (this.getIntroductionClone() === null) {
        this.saveClone();
      } else {
        this.getIntroductionClone().save();
        this.getIntroductionClone().addListener("saved", function() {
          this.setIntroductionCloneId(this.getIntroductionClone().getId());
          this.saveClone();
        }, this);
      }
    },

    /**
    * Save collections.
    */
    saveCollections : function() {
      this.getCollections().forEach(function(collection) {
        collection.save(this.getId());
      }, this);
      this.saveLots();
    },

    /**
    * Save lots.
    */
    saveLots : function() {
      if (this.getLots().getLength() > 0) {
        var cptLot = 0;
        this.getLots().forEach(function(lot) {
          lot.setClone(this.getNumero());
          lot.save();
          lot.addListener("saved", function() {
            cptLot++;
            if (cptLot == this.getLots().length) {
              this.fireEvent("saved");
            }
          }, this);
        }, this);
      } else {
        this.fireEvent("saved");
      }
    },

    /**
    * Save clone.
    */
    saveClone : function() {
      var serviceFournisseur = this.__service.query("getPepinieristeByNom", [this.getFournisseur()]);
      serviceFournisseur.addListener("changeResponseModel", function() {
        try {
          this.setIdFournisseur(serviceFournisseur.getResult()[0]["id_pepinieriste"]);
        } catch (e) {
          this.debug("Failed to set fournisseur id");
        }
        if (this.getId() === null) {
          this.getNumeroClone();
        } else {
          this.update();
        }
      }, this);
    },

    /**
    * Get numero automatic.
    */
    getNumeroClone : function() {
      if (this.getNumeroAuto()) {
        var variete = new prems.Variete(this.getIdVariete());
        variete.addListener("getEnd", function() {
          var serviceNumero = this.__service.query("getNextValueCpt", [variete.getGenre()]);
          serviceNumero.addListener("changeResponseModel", function() {
            var cpt = serviceNumero.getResult()[0]["cpt"];
            var character = serviceNumero.getResult()[0]["char"];
            var numero = character + cpt;
            this.setNumero(numero);
            this.create();
          }, this);
        }, this);
      } else {
        this.create();
      }
    },

    /**
    * Create
    */
    create : function() {
      var serviceClone = this.__service.query("createClone", [this.getNumero(), this.getIdVariete(), this.getRemarque(), this.getIntroductionCloneId(), this.getNom(), this.getIdFournisseur(), this.getDateIntro()]);
      serviceClone.addListener("changeResponseModel", function() {
        this.setId(serviceClone.getResult()[0]["id_clone"]);
        this.saveCollections();
      }, this);
    },

    /**
    * Update
    */
    update : function() {
      var serviceClone = this.__service.query("updateClone", [this.getId(), this.getNumero(), this.getIdVariete(), this.getRemarque(), this.getIntroductionCloneId(), this.getNom(), this.getIdFournisseur(), this.getDateIntro()]);
      serviceClone.addListener("changeResponseModel", function() {
        this.saveCollections();
      }, this);
    },

    /**
    * get table row
    * @return {Object} row to show in tables.
    */
    getRow : function() {
      return {
        "id" : this.getId(),
        "numero" : this.getNumero(),
        "nom" : this.getNom(),
        "hash" : this.toHashCode()
      };
    },

    /**
    *
    * get Variete object
    */
    getVarieteObj : function() {
      var variete = new prems.Variete(this.getIdVariete());
      this.setVariete(variete);
      variete.addListener("getEnd", function() {
        this.fireDataEvent("getEnd", this);
      }, this);
    },

    /**
    * Add a lot.
    * @param lot {prems.Lot} lot
    */
    addLot : function(lot) {
      var lots = this.getLots();
      if (lots.indexOf(lot) < 0) {
        lots.push(lot);
      }
      this.setLots(lots);
      this.fireDataEvent("changeLots");
    },

    /**
    * Remove a lot.
    * @param lot {prems.Lot} lot
    */
    removeLot : function(lot) {
      var lots = this.getLots();
      lots.remove(lot);
      this.setLots(lots);
      this.fireDataEvent("changeLots");
    },

    /**
    * Add collection.
    */
    addCollection : function() {
      var collections = this.getCollections();
      collections.push(new prems.Collection());
      this.setCollections(collections);
      this.fireDataEvent("changeCollections");
    },

    /**
    * to CSV.
    *@return {Object} data to csv.
    */
    toCsv : function() {
      var data = this.getVariete().toCsv();
      data["id accession"] = this.getId();
      data["Numéro accession"] = this.getNumero();
      data["Nom accession"] = this.getNom();
      var format = new qx.util.format.DateFormat("dd/MM/yyyy");
      if (this.getDateIntro() !== null) {
        data["Date introduction"] = format.format(this.getDateIntro());
      } else {
        data["Date introduction"] = null;
      }
      data["Fournisseur"] = this.getFournisseur();
      data["Collection"] = "";
      this.getCollections().forEach(function(collection) {
        data["Collection"] += collection.getNom() + ";";
      }, this);
      if (this.getIntroductionClone() !== null) {
        data["État sanitaire"] = this.getIntroductionClone().getEtatSanitaire();
        data["Mode introduction"] = this.getIntroductionClone().getModeIntroduction();
        data["Numéro passeport phytosanitaire"] = this.getIntroductionClone().getNumeroPasseport();
        data["Lieu greffage"] = this.getIntroductionClone().getLieuGreffage();
      }
      return data;
    }
  }
});
