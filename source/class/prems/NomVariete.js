/**
* NomVariete object.
* Stores all the informations of the "Nom variete" table.
*/
qx.Class.define("prems.NomVariete", {
  extend : qx.core.Object,
  events : {
    /**
    * Fired when the name is saved in the database.
    */
    "saved" : "qx.event.type.Event",

    /**
    * Fired when the name is retrieved from the database.
    */
    "getEnd" : "qx.event.type.Event"
  },
  properties : {
    /**
    * id
    */
    id : {
      init : null,
      nullable : true,
      check : "Integer",
      event : "changeId"
    },

    /**
    * idVariete
    */
    idVariete : {
      init : null,
      nullable : true,
      check : "Integer",
      event : "changeIdVariete"
    },

    /**
    * nom
    */
    nom : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeNom"
    },

    /**
    * date
    */
    date : {
      init : null,
      nullable : true,
      check : "Date",
      event : "changeDate"
    },

    /**
    * type
    */
    type : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeType"
    },

    /**
    * toDelete
    */
    toDelete : {
      init : false,
      check : "Boolean",
      event : "changeToDelete"
    }
  },

  /**
  * constructor
  * @param idTypeNom {Integer} id of the typeNom if it exists in the database.
  */
  construct : function(idTypeNom) {
    this.base(arguments);

    //Service
    this.__service = prems.service.Service.getInstance();

    //Sets the variete id
    if (idTypeNom) {
      this.setId(idTypeNom);
      this.get();
    }
  },
  members :
  {
    /**
    * Saves (update or create)
    */
    save : function() {
      if (this.getToDelete()) {
        this.remove();
      } else {
        var serviceTypeNom = this.__service.query("getTypeNom", [this.getType()]);
        serviceTypeNom.addListener("changeResponseModel", function() {
          try {
            var idTypeNom = serviceTypeNom.getResult()[0]["id_type_nom"];
          } catch (e) {
            idTypeNom = null;
          }
          if (this.getId() === null) {
            this.create(idTypeNom);
          } else {
            this.update(idTypeNom);
          }
        }, this);
      }
    },

    /**
    * Get
    */
    get : function() {
      var service = this.__service.query("getNomVarieteById", [this.getId()]);
      service.addListener("changeResponseModel", function() {
        this.setId(service.getResult()[0]["id_nom_var"]);
        this.setDate(service.getResult()[0]["date_var"]);
        this.setNom(service.getResult()[0]["nom"]);
        var idTypeNom = service.getResult()[0]["type"];
        this.getTypeInfos(idTypeNom);
      }, this);
    },

    /**
    * Get type infos
    *@param idTypeNom {Integer} id type nom
    */
    getTypeInfos : function(idTypeNom) {
      var serviceType = this.__service.query("getTypeNomById", [idTypeNom]);
      serviceType.addListener("changeResponseModel", function() {
        try {
          this.setType(serviceType.getResult()[0]["valeur"]);
        } catch (e) {
        }
        this.fireEvent("getEnd");
      }, this);
    },

    /**
    * Create
    *@param idTypeNom {Integer} id type nom
    */
    create : function(idTypeNom) {
      var service = this.__service.query("createNomVariete", [this.getIdVariete(), this.getNom(), this.getDate(), idTypeNom]);
      service.addListener("changeResponseModel", function() {
        this.fireEvent("saved");
      }, this);
    },

    /**
    * Update
    *@param idTypeNom {Integer} id type nom
    */
    update : function(idTypeNom) {
      var service = this.__service.query("updateNomVariete", [this.getId(), this.getIdVariete(), this.getNom(), this.getDate(), idTypeNom]);
      service.addListener("changeResponseModel", function() {
        this.fireEvent("saved");
      }, this);
    },

    /**
    * Remove
    */
    remove : function() {
      var service = this.__service.query("deleteNomVariete", [this.getId()]);
      service.addListener("changeResponseModel", function() {
        this.fireEvent("saved");
      }, this);
    },

    /**
    * Get row
    *@return {Object} objct to show in the tables.
    */
    getRow : function() {
      return {
        "id" : this.getId(),
        "type" : this.getType(),
        "nom" : this.getNom(),
        "date" : this.getDate(),
        "hash" : this.toHashCode()
      };
    }
  }
});
