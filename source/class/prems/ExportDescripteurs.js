/**
* Export descripteurs
*/
qx.Class.define("prems.ExportDescripteurs",
{
  extend : qx.ui.core.Widget,
  events :
  {

  },
  construct : function(type) {
    this.base(arguments);
    this.__type = type;
    //this.getData();

    //Service
    this.__service = prems.service.Service.getInstance();

    //Containers
    this.__container = new qx.ui.container.Composite(new qx.ui.layout.VBox(5));
    this._setLayout(new qx.ui.layout.Grow());
    this._add(this.__container);


    var r1 = new qx.ui.form.RadioButton("Collections");
    var r2 = new qx.ui.form.RadioButton("Contacts");
    var r3 = new qx.ui.form.RadioButton("Sites");
    var r4 = new qx.ui.form.RadioButton("Taxinomie");
    var r5 = new qx.ui.form.RadioButton("Type lieux");
    var r6 = new qx.ui.form.RadioButton("Type noms");
    var r7 = new qx.ui.form.RadioButton("Type notations");

    // Add them to the container
    this.__container.add(r1);
    this.__container.add(r2);
    this.__container.add(r3);
    this.__container.add(r4);
    this.__container.add(r5);
    this.__container.add(r6);
    this.__container.add(r7);

    // Add all radio buttons to the manager
    var manager = new qx.ui.form.RadioGroup(r1, r2, r3, r4, r5, r6, r7);

    var exportButton = new qx.ui.form.Button(this.tr("Export"));
    this.__container.add(exportButton);

    exportButton.addListener("execute", function() {
      this.__type=manager.getSelection()[0].getLabel();
      this.getData();
    }, this);
  },
  members :
  {
    getData : function() {
      let service = null;
      switch (this.__type) {
        case "Collections":
        service = this.__service.query("getAllCollection", []);
        break;
        case "Sites":
        service = this.__service.query("getAllSites", []);
        break;
        case "Contacts":
        service = this.__service.query("getAllPepinieristePays", []);
        break;
        case "Taxinomie":
        service = this.__service.query("getAllTaxinomie", []);
        break;
        case "Type lieux":
        service = this.__service.qeury("getAllTypeLieu", []);
        break;
        case "Type noms":
        service = this.__service.query("getAllTypeNom", []);
        break;
        case "Type notations":
        service = this.__service.query("getAllTypeNotation", []);
        break;
        default:
        break;
      }

      service.addListener("changeResponseModel", function() {
        this.__data = [];
        switch (this.__type) {
          case "Collections":
          this.__data.push(["collection"]);
          for (let i in service.getResult()) {
            this.__data.push([service.getResult()[i]["nom"]]);
          }
          break;
          case "Contacts":
          this.__data.push(["Nom", "Adresse1", "Adresse2", "Ville", "Etat", "Code postal", "Pays", "Email", "Téléphone", "Fax"]);
          var result = service.getResult();
          for (let i in service.getResult()) {
            this.__data.push([result[i]["nom"], result[i]["adresse1"], result[i]["adresse2"], result[i]["ville"], result[i]["etat"], result[i]["code postal"], result[i]["pays"], result[i]["email"], result[i]["telephone"], result[i]["fax"]]);
          }
          break;
          case "Sites":
          this.__data.push(["site"]);
          for (let i in service.getResult()) {
            this.__data.push([service.getResult()[i]["nom_site"]]);
          }
          break;
          case "Taxinomie":
          this.__data.push(["Genre", "Espèce"]);
          for (let i in service.getResult()) {
            this.__data.push([service.getResult()[i]["genre"], service.getResult()[i]["espece"]]);
          }
          break;
          case "Type lieux":
          this.__data.push(["Type lieux"]);
          for (let i in service.getResult()) {
            this.__data.push([service.getResult()[i]["type"]]);
          }
          break;
          case "Type noms":
          this.__data.push(["Type noms"]);
          for (let i in service.getResult()) {
            this.__data.push([service.getResult()[i]["valeur"]]);
          }
          break;
          case "Type notations":
          this.__data.push(["Type notations"]);
          for (let i in service.getResult()) {
            this.__data.push([service.getResult()[i]["nom"]]);
          }
          break;
          default:
          break;
        }
        this.__writeFile();
      }, this);
    },

    /**
    * Write file.
    */
    __writeFile : function() {
      let delimiter = "\t";
      let file = "";
      for (let line in this.__data) {
        for (let col in this.__data[line]) {
          file += this.__data[line][col];
          file += delimiter;
        }
        file += "\n";
      }

      file = file.replace(new RegExp("null", "g"), "");
      file = file.replace(new RegExp("undefined", "g"), "");
      let fileWriter = new qxfileio.FileWriter();
      fileWriter.saveTextAsFile(file, "DescripteursPlantDb.csv");
    }
  }
});
