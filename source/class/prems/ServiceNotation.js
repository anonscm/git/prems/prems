/**
 * Service
 */
qx.Class.define("prems.ServiceNotation",
{
  type : "singleton",
  extend : qx.core.Object,


  /**
  * Constructor
  */
  construct : function() {
    this.base(arguments);
    prems.ServiceNotation.SERVICE_BASE_URL = qxelvis.session.service.Session.SERVICE_BASE_URL;
    this._serviceUrl = prems.service.Service.SERVICE_BASE_URL + "/ServiceNotation.py";
    this._serviceName = "ServiceNotation";
  },

  statics : {
    SERVICE_BASE_URL : "SERVICE_BASE_URL_not_defined"
  },

  /**
  * Method definitions.
  */
  members :
  {
    _serviceUrl : "URL_NOT_SET",
    _serviceName : "SERVICE_NOT_SET",

    /**
    * asynchronous query
    *@param serviceMethod {String} method to call
    *@param args {Array} arguments
    *@return {Array} result
    */
    query : function(serviceMethod, args) {
      return new qxelvis.io.RpcService(this._serviceUrl, this._serviceName, serviceMethod, args);
    }
  }
});
